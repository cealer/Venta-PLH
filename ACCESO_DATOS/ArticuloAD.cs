using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class ArticuloAD
    {
        public static bool Agregar(Articulo ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_INSERTAR_Articulo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdEmpresa", ent._Empresa.Id);
                cmd.Parameters.AddWithValue("@IdSucursal", ent.Sucursal.Id);
                cmd.Parameters.AddWithValue("@IdUsuario", ent.Usuario.Id);
                cmd.Parameters.AddWithValue("@IdFamilia", ent.Familia.Id);
                cmd.Parameters.AddWithValue("@IdUnidad", ent.Unidad.Id);
                cmd.Parameters.AddWithValue("@IdMoneda", ent.moneda.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cmd.Parameters.AddWithValue("@Stock", ent.Stock);
                cmd.Parameters.AddWithValue("@StockInicial", ent.StockInicial);
                cmd.Parameters.AddWithValue("@Precio", ent.Precio);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(Articulo ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_Modificar_Articulo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@IdEmpresa", ent._Empresa.Id);
                cmd.Parameters.AddWithValue("@IdSucursal", ent.Sucursal.Id);
                cmd.Parameters.AddWithValue("@IdUsuario", ent.Usuario.Id);
                cmd.Parameters.AddWithValue("@IdFamilia", ent.Familia.Id);
                cmd.Parameters.AddWithValue("@IdUnidad", ent.Unidad.Id);
                cmd.Parameters.AddWithValue("@IdMoneda", ent.moneda.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cmd.Parameters.AddWithValue("@Stock", ent.Stock);
                cmd.Parameters.AddWithValue("@StockInicial", ent.StockInicial);
                cmd.Parameters.AddWithValue("@Precio", ent.Precio);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Articulo where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE Articulo SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static Articulo CrearEntidad(IDataReader lector)
        {
            var aux = new Articulo();
            aux.Id = lector.GetString(0);
            aux._Empresa = new Empresa();
            aux._Empresa.Id = lector.GetString(1);
            aux.Sucursal = new Sucursal();
            aux.Sucursal.Id = lector.GetString(2);
            aux.Usuario = new Usuario();
            aux.Usuario.Id = lector.GetString(3);
            aux.Familia = new Familia();
            aux.Familia.Id = lector.GetString(4);
            aux.Unidad = new Unidad();
            aux.Unidad.Id = lector.GetString(5);
            aux.moneda = new Moneda();
            aux.moneda.Id = lector.GetString(6);
            aux.Descripcion = lector.GetString(7);
            aux.Stock = lector.GetInt32(8);
            aux.StockInicial = lector.GetInt32(9);
            aux.Precio = lector.GetDecimal(10);
            aux.Estado = lector.GetString(11);
            aux.Unidad.Descripcion = lector.GetString(12);
            aux.moneda.Descripcion = lector.GetString(13);
            aux.Familia.Descripcion = lector.GetString(14);
            return aux;
        }

        public static List<Articulo> Buscar(Articulo ent)
        {
            var lista = new List<Articulo>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_BUSCAR_Articulo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@ArticuloIdEmpresa", ent._Empresa.Id);
                cmd.Parameters.AddWithValue("@ArticuloIdSucursal", ent.Sucursal.Id);
                cmd.Parameters.AddWithValue("@ArticuloDescripcion", ent.Descripcion);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<Articulo> ObtenerUltimos(string Empresa, string Sucursal)
        {
            var lista = new List<Articulo>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_TOP10_Articulo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@Empresa", Empresa);
                cmd.Parameters.AddWithValue("@Sucursal", Sucursal);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }
    }
}
