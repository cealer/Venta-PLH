﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCESO_DATOS
{
    public class ConsultasAD
    {
        public static DataTable RealizarConsulta(string consulta)
        {
            var ds = new DataSet();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var adapter = new SqlDataAdapter(consulta, cn);
                adapter.Fill(ds);
            }
            return ds.Tables[0];
        }



    }
}