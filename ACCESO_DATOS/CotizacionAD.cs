using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class CotizacionAD
    {
        public static bool Agregar(Cotizacion ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_INSERTAR_Cotizacion", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cmd.Parameters.AddWithValue("@IdEmpresa", ent.Empresa.Id);
                cmd.Parameters.AddWithValue("@IdSucursal", ent.Sucursal.Id);
                cmd.Parameters.AddWithValue("@IdUsuario", ent.Usuario.Id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(Cotizacion ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_Modificar_Cotizacion", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static string MaxCotizacion()
        {
            var res = "";
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_MAX_Cotizacion", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    res = r.GetString(0);
                }
                cn.Close();
            }
            return res;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Cotizacion where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE Cotizacion SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static Cotizacion CrearEntidad(IDataReader lector)
        {
            var aux = new Cotizacion();
            aux.Id = lector.GetString(0);
            aux.Descripcion = lector.GetString(1);
            aux.Empresa.Id = lector.GetString(2);
            aux.Sucursal.Id = lector.GetString(3);
            aux.Usuario.Id = lector.GetString(4);
            aux.Fecha = lector.GetDateTime(5);
            aux.Estado = lector.GetString(6);
            return aux;
        }

        public static List<Cotizacion> Buscar(Cotizacion ent, DateTime fec1, DateTime fec2)
        {
            var lista = new List<Cotizacion>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_BUSCAR_Cotizacion", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@CotizacionDescripcion", ent.Descripcion);
                cmd.Parameters.AddWithValue("@fec1", fec1);
                cmd.Parameters.AddWithValue("@fec2", fec2);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        //public static List<Cotizacion> ObtenerUltimos()
        //{
        //    var lista = new List<Cotizacion>();
        //    using (var cn = Conexion.ConexionDBSqlServer())
        //    {
        //        SqlCommand cmd = new SqlCommand("SPU_TOP10_Cotizacion", cn);
        //        cn.Open();
        //        var r = cmd.ExecuteReader();
        //        while (r.Read())
        //        {
        //            lista.Add(CrearEntidad(r));
        //        }
        //        cn.Close();
        //    }
        //    return lista;
        //}
    }
}
