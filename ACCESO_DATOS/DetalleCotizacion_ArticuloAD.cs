using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class DetalleCotizacion_ArticuloAD
    {
        public static bool Agregar(DetalleCotizacion_Articulo ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_INSERTAR_DetalleCotizacion_Articulo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdCotizacion", ent.Cotizacion.Id);
                cmd.Parameters.AddWithValue("@IdArticulo", ent.Articulo.Id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static DetalleCotizacion_Articulo CrearEntidad(IDataReader lector)
        {
            var aux = new DetalleCotizacion_Articulo();
            aux.Id = lector.GetString(0);
            aux.Cotizacion = new Cotizacion();
            aux.Cotizacion.Id = lector.GetString(1);
            aux.Articulo = new Articulo();
            aux.Articulo.Id = lector.GetString(2);
            aux.Estado = lector.GetString(3);
            return aux;
        }
    }
}
