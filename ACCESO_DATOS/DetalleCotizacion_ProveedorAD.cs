using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class DetalleCotizacion_ProveedorAD
    {
        public static bool Agregar(DetalleCotizacion_Proveedor ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_INSERTAR_DetalleCotizacion_Proveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdCotizacion", ent.Cotizacion.Id);
                cmd.Parameters.AddWithValue("@IdProveedor", ent.Proveedor.Id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(DetalleCotizacion_Proveedor ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_Modificar_DetalleCotizacion_Proveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from DetalleCotizacion_Proveedor where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE DetalleCotizacion_Proveedor SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static DetalleCotizacion_Proveedor CrearEntidad(IDataReader lector)
        {
            var aux = new DetalleCotizacion_Proveedor();
            aux.Id = lector.GetString(0);
            aux.Cotizacion = new Cotizacion();
            aux.Cotizacion.Id = lector.GetString(1);
            aux.Proveedor = new Proveedor();
            aux.Proveedor.Id = lector.GetString(2);
            aux.Estado = lector.GetString(3);
            return aux;
        }

        public static List<DetalleCotizacion_Proveedor> Buscar(DetalleCotizacion_Proveedor ent)
        {
            var lista = new List<DetalleCotizacion_Proveedor>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_BUSCAR_DetalleCotizacion_Proveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<DetalleCotizacion_Proveedor> ObtenerUltimos()
        {
            var lista = new List<DetalleCotizacion_Proveedor>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_TOP10_DetalleCotizacion_Proveedor", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }
    }
}
