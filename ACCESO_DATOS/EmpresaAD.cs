using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class EmpresaAd
    {
        public static bool Agregar(Empresa ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_INSERTAR_Empresa", cn) {CommandType = CommandType.StoredProcedure};
                cmd.Parameters.AddWithValue("@Ruc", ent.Ruc);
                cmd.Parameters.AddWithValue("@RazonSocial", ent.RazonSocial);
                cmd.Parameters.AddWithValue("@NombreAbreviado", ent.NombreAbreviado);
                cmd.Parameters.AddWithValue("@Direccion", ent.Direccion);
                cmd.Parameters.AddWithValue("@Telefono", ent.Telefono);
                cmd.Parameters.AddWithValue("@Fax", ent.Fax);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(Empresa ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_Modificar_Empresa", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@Ruc", ent.Ruc);
                cmd.Parameters.AddWithValue("@RazonSocial", ent.RazonSocial);
                cmd.Parameters.AddWithValue("@NombreAbreviado", ent.NombreAbreviado);
                cmd.Parameters.AddWithValue("@Direccion", ent.Direccion);
                cmd.Parameters.AddWithValue("@Telefono", ent.Telefono);
                cmd.Parameters.AddWithValue("@Fax", ent.Fax);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Empresa where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE Empresa SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static Empresa CrearEntidad(IDataReader lector)
        {
            var aux = new Empresa();
            aux.Id = lector.GetString(0);
            aux.Ruc = lector.GetString(1);
            aux.RazonSocial = lector.GetString(2);
            aux.NombreAbreviado = lector.GetString(3);
            aux.Direccion = lector.GetString(4);
            aux.Estado = lector.GetString(5);
            aux.Telefono = lector.GetString(6);
            aux.Fax = lector.GetString(7);
            return aux;
        }

        public static List<Empresa> Buscar(Empresa ent)
        {
            var lista = new List<Empresa>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_Empresa", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@EmpresaRuc", ent.Ruc);
                cmd.Parameters.AddWithValue("@EmpresaRazonSocial", ent.RazonSocial);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<Empresa> ObtenerUltimos()
        {
            var lista = new List<Empresa>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_TOP10_Empresa", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }
    }
}
