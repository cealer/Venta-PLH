using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class MedioProveedorAD
    {
        public static bool Agregar(MedioProveedor ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_INSERTAR_MedioProveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdTipoMedio", ent.TipoMedio.Id);
                cmd.Parameters.AddWithValue("@IdProveedor", ent.Proveedor.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(MedioProveedor ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_Modificar_MedioProveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@IdTipoMedio", ent.TipoMedio.Id);
                cmd.Parameters.AddWithValue("@IdProveedor", ent.Proveedor.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from MedioProveedor where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE MedioProveedor SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static MedioProveedor CrearEntidad(IDataReader lector)
        {
            var aux = new MedioProveedor();
            aux.Id = lector.GetString(0);
            aux.TipoMedio = new TipoMedio();
            aux.TipoMedio.Id = lector.GetString(1);
            aux.Proveedor = new Proveedor();
            aux.Proveedor.Id = lector.GetString(2);
            aux.TipoMedio.Descripcion = lector.GetString(3);
            aux.Descripcion = lector.GetString(4);
            aux.Estado = lector.GetString(5);
            return aux;
        }

        public static List<MedioProveedor> Buscar(MedioProveedor ent)
        {
            var lista = new List<MedioProveedor>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_MedioProveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdProveedor", ent.Proveedor.Id);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        //public static List<MedioProveedor> ObtenerUltimos()
        //{
        //    var lista = new List<MedioProveedor>();
        //    using (var cn = Conexion.ConexionDBSqlServer())
        //    {
        //        SqlCommand cmd = new SqlCommand("SPU_TOP10_MedioProveedor", cn);
        //        cn.Open();
        //        var r = cmd.ExecuteReader();
        //        while (r.Read())
        //        {
        //            lista.Add(CrearEntidad(r));
        //        }
        //        cn.Close();
        //    }
        //    return lista;
        //}
    }
}
