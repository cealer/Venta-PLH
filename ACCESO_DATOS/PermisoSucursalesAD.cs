﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCESO_DATOS
{
    public class PermisoSucursalesAD
    {
        public static PermisoSucursales CrearEntidad(IDataReader lector)
        {
            var aux = new PermisoSucursales();
            aux.Id = lector.GetString(0);
            aux.usuario = new Usuario();
            aux.usuario.Id = lector.GetString(1);
            aux.sucursal = new Sucursal();
            aux.sucursal.Id = lector.GetString(2);
            aux.empresa = new Empresa();
            aux.empresa.Id = lector.GetString(3);
            aux.sucursal.Descripcion = lector.GetString(4);
            aux.empresa.NombreAbreviado = lector.GetString(5);
            aux.Estado = lector.GetString(6);
            return aux;
        }

        public static List<PermisoSucursales> Buscar(PermisoSucursales ent)
        {
            var lista = new List<PermisoSucursales>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_PermisoSucursales", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PermisoSucursalesIdUsuario", ent.usuario.Id);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<Empresa> BuscarSucursal_IdEmpresa(string IdEmpresa)
        {
            var lista = new List<Empresa>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_Sucursales_Por_EmpresaId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(EmpresaAd.CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }
    }
}
