using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class PreferenciasAD
    {
        public static bool Agregar(Preferencias ent)
        {
        var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
            SqlCommand cmd = new SqlCommand("SPU_INSERTAR_Preferencias",cn);
            cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        r = cmd.ExecuteNonQuery();
        cn.Close();
        }
        return r > 0;
      }

        public static bool Modificar(Preferencias ent)
        {
        var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
            SqlCommand cmd = new SqlCommand("SPU_Modificar_Preferencias",cn);
            cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        r = cmd.ExecuteNonQuery();
        cn.Close();
        }
        return r > 0;
        }

        public static bool Existe(string id)
        {
        var r = 0;
        using (var cn = Conexion.ConexionDBSqlServer())
         {
         var sql = "select count(Id) from Preferencias where Id = @Id"; 
          var cmd = new SqlCommand(sql, cn);
          cmd.Parameters.AddWithValue("@Id", id);
          cn.Open();
          r = (int)cmd.ExecuteScalar();
          cn.Close();
         }
         return r > 0;
         }

        public static bool Eliminar(string id)
        {
        var r = 0;
        using (var cn = Conexion.ConexionDBSqlServer()){
        var sql = "UPDATE Preferencias SET Estado='0' WHERE Id =@Id";
        var cmd = new SqlCommand(sql, cn);
        cmd.Parameters.AddWithValue("@Id", id);
        cn.Open();
        r = cmd.ExecuteNonQuery();
        cn.Close();
        }
        return r > 0;
        }


        public static Preferencias CrearEntidad(IDataReader lector)
        {
        var aux = new Preferencias();
        aux.Id = lector.GetString(0);
        aux.IdUsuario = lector.GetString(1);
        aux.IdSucursal = lector.GetString(2);
        aux.TituloTile1 = lector.GetString(3);
        aux.TituloTile2 = lector.GetString(4);
        aux.IdTile = lector.GetString(5);
        return aux;
        }

         public static List<Preferencias> Buscar(Preferencias ent)
        {
        var lista = new List<Preferencias>();
        using (var cn = Conexion.ConexionDBSqlServer())
        {
                    SqlCommand cmd = new SqlCommand("SPU_BUSCAR_Preferencias", cn); 
            cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        var r = cmd.ExecuteReader();
        while (r.Read())
        {
        lista.Add(CrearEntidad(r));
        }
       cn.Close();
        }
        return lista;
        }

        public static List<Preferencias> ObtenerUltimos()
        {
        var lista = new List<Preferencias>();
        using (var cn = Conexion.ConexionDBSqlServer())
        {
                    SqlCommand cmd = new SqlCommand("SPU_TOP10_Preferencias", cn); 
        cn.Open();
        var r = cmd.ExecuteReader();
        while (r.Read())
        {
        lista.Add(CrearEntidad(r));
        }
       cn.Close();
        }
        return lista;
        }
    }
}
