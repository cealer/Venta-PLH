using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class ProveedorAD
    {
        public static bool Agregar(Proveedor ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_INSERTAR_Proveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdEmpresa", ent.Empresa.Id);
                cmd.Parameters.AddWithValue("@RUC", ent.RUC);
                cmd.Parameters.AddWithValue("@RazonSocial", ent.RazonSocial);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(Proveedor ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_Modificar_Proveedor", cn);
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@IdEmpresa", ent.Empresa.Id);
                cmd.Parameters.AddWithValue("@RUC", ent.RUC);
                cmd.Parameters.AddWithValue("@RazonSocial", ent.RazonSocial);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Proveedor where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE Proveedor SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static Proveedor CrearEntidad(IDataReader lector)
        {
            var aux = new Proveedor();
            aux.Id = lector.GetString(0);
            aux.Empresa = new Empresa();
            aux.Empresa.Id = lector.GetString(1);
            aux.RUC = lector.GetString(2);
            aux.RazonSocial = lector.GetString(3);
            aux.Estado = lector.GetString(4);
            return aux;
        }

        public static List<Proveedor> Buscar(Proveedor ent)
        {
            var lista = new List<Proveedor>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_Proveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@ProveedorRUC", ent.RUC);
                cmd.Parameters.AddWithValue("@ProveedorRazonSocial", ent.RazonSocial);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<Proveedor> ObtenerUltimos()
        {
            var lista = new List<Proveedor>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_TOP10_Proveedor", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static string MaxProveedor()
        {
            string res = "";
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_MAX_Proveedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    res = r.GetString(0);
                }
                cn.Close();
            }
            return res;
        }

    }
}
