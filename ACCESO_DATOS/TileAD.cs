using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class TileAD
    {
        public static bool Agregar(Tile ent)
        {
        var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
            SqlCommand cmd = new SqlCommand("SPU_INSERTAR_Tile",cn);
            cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        r = cmd.ExecuteNonQuery();
        cn.Close();
        }
        return r > 0;
      }

        public static bool Modificar(Tile ent)
        {
        var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
            SqlCommand cmd = new SqlCommand("SPU_Modificar_Tile",cn);
            cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        r = cmd.ExecuteNonQuery();
        cn.Close();
        }
        return r > 0;
        }

        public static bool Existe(string id)
        {
        var r = 0;
        using (var cn = Conexion.ConexionDBSqlServer())
         {
         var sql = "select count(Id) from Tile where Id = @Id"; 
          var cmd = new SqlCommand(sql, cn);
          cmd.Parameters.AddWithValue("@Id", id);
          cn.Open();
          r = (int)cmd.ExecuteScalar();
          cn.Close();
         }
         return r > 0;
         }

        public static bool Eliminar(string id)
        {
        var r = 0;
        using (var cn = Conexion.ConexionDBSqlServer()){
        var sql = "UPDATE Tile SET Estado='0' WHERE Id =@Id";
        var cmd = new SqlCommand(sql, cn);
        cmd.Parameters.AddWithValue("@Id", id);
        cn.Open();
        r = cmd.ExecuteNonQuery();
        cn.Close();
        }
        return r > 0;
        }


        public static Tile CrearEntidad(IDataReader lector)
        {
        var aux = new Tile();
        aux.Id = lector.GetString(0);
        aux.Tile = lector.GetString(1);
        aux.Img = lector.GetString(2);
        aux.ColorLetra = lector.GetString(3);
        aux.ColorFondo = lector.GetString(4);
        aux.Formulario = lector.GetString(5);
        aux.Estado = lector.GetString(6);
        return aux;
        }

         public static List<Tile> Buscar(Tile ent)
        {
        var lista = new List<Tile>();
        using (var cn = Conexion.ConexionDBSqlServer())
        {
                    SqlCommand cmd = new SqlCommand("SPU_BUSCAR_Tile", cn); 
            cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        var r = cmd.ExecuteReader();
        while (r.Read())
        {
        lista.Add(CrearEntidad(r));
        }
       cn.Close();
        }
        return lista;
        }

        public static List<Tile> ObtenerUltimos()
        {
        var lista = new List<Tile>();
        using (var cn = Conexion.ConexionDBSqlServer())
        {
                    SqlCommand cmd = new SqlCommand("SPU_TOP10_Tile", cn); 
        cn.Open();
        var r = cmd.ExecuteReader();
        while (r.Read())
        {
        lista.Add(CrearEntidad(r));
        }
       cn.Close();
        }
        return lista;
        }
    }
}
