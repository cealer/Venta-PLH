using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;

namespace ACCESO_DATOS
{
    public class TipoMedioAD
    {
        public static bool Agregar(TipoMedio ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_INSERTAR_TipoMedio", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(TipoMedio ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_Modificar_TipoMedio", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from TipoMedio where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE TipoMedio SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static TipoMedio CrearEntidad(IDataReader lector)
        {
            var aux = new TipoMedio();
            aux.Id = lector.GetString(0);
            aux.Descripcion = lector.GetString(1);
            aux.Estado = lector.GetString(2);
            return aux;
        }

        public static List<TipoMedio> Buscar(TipoMedio ent)
        {
            var lista = new List<TipoMedio>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_TipoMedio", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@TipoMedioDescripcion", ent.Descripcion);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<TipoMedio> ObtenerUltimos()
        {
            var lista = new List<TipoMedio>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_TOP10_TipoMedio", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }
    }
}
