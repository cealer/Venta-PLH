using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class UnidadAD
    {
        public static bool Agregar(Unidad ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_INSERTAR_Unidad", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cmd.Parameters.AddWithValue("@NombreCorto", ent.NombreCorto);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(Unidad ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_Modificar_Unidad", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", ent.Id);
                cmd.Parameters.AddWithValue("@Descripcion", ent.Descripcion);
                cmd.Parameters.AddWithValue("@NombreCorto", ent.NombreCorto);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Unidad where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE Unidad SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static Unidad CrearEntidad(IDataReader lector)
        {
            var aux = new Unidad();
            aux.Id = lector.GetString(0);
            aux.Descripcion = lector.GetString(1);
            aux.NombreCorto = lector.GetString(2);
            aux.Estado = lector.GetString(3);
            return aux;
        }

        public static List<Unidad> Buscar(Unidad ent)
        {
            var lista = new List<Unidad>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_BUSCAR_Unidad", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.Parameters.AddWithValue("@UnidadDescripcion", ent.Descripcion);
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<Unidad> ObtenerUltimos()
        {
            var lista = new List<Unidad>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                SqlCommand cmd = new SqlCommand("SPU_TOP10_Unidad", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }
    }
}
