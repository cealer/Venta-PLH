using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using ACCESO_DATOS;
using System.Data;
using System.Data.SqlClient;


namespace ACCESO_DATOS
{
    public class UsuarioAD
    {
        public static bool Agregar(Usuario ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_INSERTAR_Usuario", cn) { CommandType = CommandType.StoredProcedure };
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Modificar(Usuario ent)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_Modificar_Usuario", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Existe(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Usuario where Id = @Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

        public static bool Eliminar(string id)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "UPDATE Usuario SET Estado='0' WHERE Id =@Id";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                r = cmd.ExecuteNonQuery();
                cn.Close();
            }
            return r > 0;
        }


        public static Usuario CrearEntidad(IDataReader lector)
        {
            var aux = new Usuario();
            aux.Id = lector.GetString(0);
            aux.persona = new Persona();
            aux.persona.Id = lector.GetString(1);
            aux.tipoUsuario = new TipoUsuario();
            aux.tipoUsuario.Id = lector.GetString(2);
            aux.Empresa = new Empresa();
            aux.Empresa.Id = lector.GetString(3);
            aux.Sucursal = new Sucursal();
            aux.Sucursal.Id = lector.GetString(4);
            aux.Empresa.NombreAbreviado = lector.GetString(5);
            aux.Sucursal.Descripcion = lector.GetString(6);
            aux._Usuario = lector.GetString(7);
            aux.tipoUsuario.Descripcion = lector.GetString(8);
            aux.Estado = lector.GetString(9);
            return aux;
        }

        public static List<Usuario> Buscar(Usuario ent)
        {
            var lista = new List<Usuario>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_BUSCAR_Usuario", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UsuarioUsuario", ent._Usuario);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static List<Usuario> ObtenerUltimos()
        {
            var lista = new List<Usuario>();
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var cmd = new SqlCommand("SPU_TOP10_Usuario", cn);
                cn.Open();
                var r = cmd.ExecuteReader();
                while (r.Read())
                {
                    lista.Add(CrearEntidad(r));
                }
                cn.Close();
            }
            return lista;
        }

        public static bool Autenticar(string us, string pass)
        {
            var r = 0;
            using (var cn = Conexion.ConexionDBSqlServer())
            {
                var sql = "select count(Id) from Usuario where Usuario = @us and Pass =@pass";
                var cmd = new SqlCommand(sql, cn);
                cmd.Parameters.AddWithValue("@us", us);
                cmd.Parameters.AddWithValue("@pass", pass);
                cn.Open();
                r = (int)cmd.ExecuteScalar();
                cn.Close();
            }
            return r > 0;
        }

    }
}
