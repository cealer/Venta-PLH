using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class ArticuloBOL
    {

        public static bool Registro(Articulo ent)
        {
            bool r;
            if (ArticuloAD.Existe(ent.Id))
            {
                r = true;
                ArticuloAD.Modificar(ent);
            }
            else
            {
                r = false;
                ArticuloAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            ArticuloAD.Eliminar(id);
        }

        public static List<Articulo> BuscarUltimos(string Empresa, string Sucursal)
        {
            return ArticuloAD.ObtenerUltimos(Empresa, Sucursal);
        }

        public static List<Articulo> Buscar(Articulo ent)
        {
            return ArticuloAD.Buscar(ent);
        }
    }
}
