﻿using ACCESO_DATOS;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class ConsultasBOL
    {
        public static DataTable Buscar(string consulta)
        {
            return ConsultasAD.RealizarConsulta(consulta);
        }
    }
}