using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class CotizacionBOL
    {

        public static bool Registro(Cotizacion ent)
        {
            bool r;
            if (CotizacionAD.Existe(ent.Id))
            {
                r = true;
                CotizacionAD.Modificar(ent);
            }
            else
            {
                r = false;
                CotizacionAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            CotizacionAD.Eliminar(id);
        }

        //public static List<Cotizacion> BuscarUltimos()
        //{
        //    return CotizacionAD.ObtenerUltimos();
        //}

        public static List<Cotizacion> Buscar(Cotizacion ent, DateTime fec1, DateTime fec2)
        {
            return CotizacionAD.Buscar(ent, fec1, fec2);
        }

        public static string Max()
        {
            return CotizacionAD.MaxCotizacion();
        }
    }
}
