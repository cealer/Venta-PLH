using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class EmpresaBOL
    {

        public static bool Registro(Empresa ent)
        {
            bool r;
            if (EmpresaAd.Existe(ent.Id))
            {
                r = true;
                EmpresaAd.Modificar(ent);
            }
            else
            {
                r = false;
                EmpresaAd.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            EmpresaAd.Eliminar(id);
        }

        public static List<Empresa> BuscarUltimos()
        {
            return EmpresaAd.ObtenerUltimos();
        }

        public static List<Empresa> Buscar(Empresa ent)
        {
            return EmpresaAd.Buscar(ent);
        }
    }
}
