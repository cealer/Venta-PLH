using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class FamiliaBOL
    {

        public static bool Registro(Familia ent)
        {
            bool r;
            if (FamiliaAD.Existe(ent.Id))
            {
                r = true;
                FamiliaAD.Modificar(ent);
            }
            else
            {
                r = false;
                FamiliaAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            FamiliaAD.Eliminar(id);
        }

        public static List<Familia> BuscarUltimos()
        {
            return FamiliaAD.ObtenerUltimos();
        }

        public static List<Familia> Buscar(Familia ent)
        {
            return FamiliaAD.Buscar(ent);
        }
    }
}
