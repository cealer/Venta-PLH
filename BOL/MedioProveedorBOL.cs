using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class MedioProveedorBOL
    {

        public static bool Registro(MedioProveedor ent)
        {
            bool r;
            if (MedioProveedorAD.Existe(ent.Id))
            {
                r = true;
                MedioProveedorAD.Modificar(ent);
            }
            else
            {
                r = false;
                MedioProveedorAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            MedioProveedorAD.Eliminar(id);
        }

        // public static List<MedioProveedor> BuscarUltimos()
        //{
        //        return MedioProveedorAD.ObtenerUltimos();
        //}

        public static List<MedioProveedor> Buscar(MedioProveedor ent)
        {
            return MedioProveedorAD.Buscar(ent);
        }
    }
}
