using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class MonedaBOL
    {

        public static bool Registro(Moneda ent)
        {

            bool r;

            if (MonedaAD.Existe(ent.Id))
            {
                r = true;
                MonedaAD.Modificar(ent);
            }
            else
            {
                r = false;
                MonedaAD.Agregar(ent);
            }
            return r;
        }

        public static void Eliminar(string id)
        {
            MonedaAD.Eliminar(id);
        }

        public static List<Moneda> BuscarUltimos()
        {
            return MonedaAD.ObtenerUltimos();
        }

        public static List<Moneda> Buscar(Moneda ent)
        {
            return MonedaAD.Buscar(ent);
        }
    }
}
