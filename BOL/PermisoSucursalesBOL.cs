﻿using ACCESO_DATOS;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class PermisoSucursalesBOL
    {
        public static List<PermisoSucursales> Buscar(PermisoSucursales ent)
        {
            return PermisoSucursalesAD.Buscar(ent);
        }

        public static List<Empresa> Buscar(string ent)
        {
            return PermisoSucursalesAD.BuscarSucursal_IdEmpresa(ent);
        }
    }
}