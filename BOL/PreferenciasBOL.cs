using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class PreferenciasBOL
    {

        public static bool Registro(Preferencias ent)
        {
        bool r;
                if (PreferenciasAD.Existe(ent.Id))
                {
        r = true;
                PreferenciasAD.Modificar(ent);
                 }
                else
                {
        r = false;
               PreferenciasAD.Agregar(ent);
                 }
        return r;
        }
        public static void Eliminar(string id)
        {
                PreferenciasAD.Eliminar(id);
        }

         public static List<Preferencias> BuscarUltimos()
        {
                return PreferenciasAD.ObtenerUltimos();
        }

         public static List<Preferencias> Buscar(Preferencias ent)
        {
                return PreferenciasAD.Buscar(ent);
        }
    }
}
