using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class ProveedorBOL
    {

        public static bool Registro(Proveedor ent)
        {
            bool r;
            if (ProveedorAD.Existe(ent.Id))
            {
                r = true;
                ProveedorAD.Modificar(ent);
            }
            else
            {
                r = false;
                ProveedorAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            ProveedorAD.Eliminar(id);
        }

        public static List<Proveedor> BuscarUltimos()
        {
            return ProveedorAD.ObtenerUltimos();
        }

        public static List<Proveedor> Buscar(Proveedor ent)
        {
            return ProveedorAD.Buscar(ent);
        }

        public static string MaxProveedor()
        {
            return ProveedorAD.MaxProveedor();
        }
    }
}