using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class SucursalBOL
    {
        public static bool Registro(Sucursal ent)
        {
            bool r;
            if (SucursalAD.Existe(ent.Id))
            {
                r = true;
                SucursalAD.Modificar(ent);
            }
            else
            {
                r = false;
                SucursalAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            SucursalAD.Eliminar(id);
        }

        public static List<Sucursal> BuscarUltimos()
        {
            return SucursalAD.ObtenerUltimos();
        }

        public static List<Sucursal> Buscar(Sucursal ent)
        {
            return SucursalAD.Buscar(ent);
        }
    }
}