using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class TileBOL
    {

        public static bool Registro(Tile ent)
        {
        bool r;
                if (TileAD.Existe(ent.Id))
                {
        r = true;
                TileAD.Modificar(ent);
                 }
                else
                {
        r = false;
               TileAD.Agregar(ent);
                 }
        return r;
        }
        public static void Eliminar(string id)
        {
                TileAD.Eliminar(id);
        }

         public static List<Tile> BuscarUltimos()
        {
                return TileAD.ObtenerUltimos();
        }

         public static List<Tile> Buscar(Tile ent)
        {
                return TileAD.Buscar(ent);
        }
    }
}
