using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class TipoMedioBOL
    {

        public static bool Registro(TipoMedio ent)
        {
            bool r;
            if (TipoMedioAD.Existe(ent.Id))
            {
                r = true;
                TipoMedioAD.Modificar(ent);
            }
            else
            {
                r = false;
                TipoMedioAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            TipoMedioAD.Eliminar(id);
        }

        public static List<TipoMedio> BuscarUltimos()
        {
            return TipoMedioAD.ObtenerUltimos();
        }

        public static List<TipoMedio> Buscar(TipoMedio ent)
        {
            return TipoMedioAD.Buscar(ent);
        }
    }
}
