using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class TipoPersonaBOL
    {

        public static bool Registro(TipoPersona ent)
        {
        bool r;
                if (TipoPersonaAD.Existe(ent.Id))
                {
        r = true;
                TipoPersonaAD.Modificar(ent);
                 }
                else
                {
        r = false;
               TipoPersonaAD.Agregar(ent);
                 }
        return r;
        }
        public static void Eliminar(string id)
        {
                TipoPersonaAD.Eliminar(id);
        }

         public static List<TipoPersona> BuscarUltimos()
        {
                return TipoPersonaAD.ObtenerUltimos();
        }

         public static List<TipoPersona> Buscar(TipoPersona ent)
        {
                return TipoPersonaAD.Buscar(ent);
        }
    }
}
