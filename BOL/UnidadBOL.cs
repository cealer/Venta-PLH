using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class UnidadBOL
    {

        public static bool Registro(Unidad ent)
        {
        bool r;
                if (UnidadAD.Existe(ent.Id))
                {
        r = true;
                UnidadAD.Modificar(ent);
                 }
                else
                {
        r = false;
               UnidadAD.Agregar(ent);
                 }
        return r;
        }
        public static void Eliminar(string id)
        {
                UnidadAD.Eliminar(id);
        }

         public static List<Unidad> BuscarUltimos()
        {
                return UnidadAD.ObtenerUltimos();
        }

         public static List<Unidad> Buscar(Unidad ent)
        {
                return UnidadAD.Buscar(ent);
        }
    }
}
