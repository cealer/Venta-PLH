using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO_DATOS;
using Entidades;

namespace BOL
{
    public class UsuarioBOL
    {

        public static bool Registro(Usuario ent)
        {
            bool r;
            if (UsuarioAD.Existe(ent.Id))
            {
                r = true;
                UsuarioAD.Modificar(ent);
            }
            else
            {
                r = false;
                UsuarioAD.Agregar(ent);
            }
            return r;
        }
        public static void Eliminar(string id)
        {
            UsuarioAD.Eliminar(id);
        }

        public static List<Usuario> BuscarUltimos()
        {
            return UsuarioAD.ObtenerUltimos();
        }

        public static List<Usuario> Buscar(Usuario ent)
        {
            return UsuarioAD.Buscar(ent);
        }

        public static bool Autenticar(string usu, string pass)
        {
            return UsuarioAD.Autenticar(usu, pass);
        }



    }
}
