﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Articulo
    {

        public string Id { get; set; }
        public string Descripcion { get; set; }
        public Empresa _Empresa { get; set; }
        public Sucursal Sucursal { get; set; }
        public Familia Familia { get; set; }
        public Usuario Usuario { get; set; }
        public Moneda moneda { get; set; }
        public Unidad Unidad { get; set; }
        public int StockInicial { get; set; }
        public int Stock { get; set; }
        public decimal Precio { get; set; }
        public string Estado { get; set; }

        public Articulo()
        {

        }

        public override string ToString()
        {
            try
            {
                return Descripcion;
            }
            catch (Exception)
            {

                return "";
            }
        }
    }
}