﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Cotizacion
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public Empresa Empresa { get; set; }
        public Sucursal Sucursal { get; set; }
        public Usuario Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public string Estado { get; set; }

        public Cotizacion()
        {

        }

        public override string ToString()
        {
            try
            {
                return Descripcion;
            }
            catch (Exception)
            {
                return "";
            }
        }

    }
}