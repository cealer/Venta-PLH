﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class DetalleCotizacion_Articulo
    {
        public string Id { get; set; }
        public Articulo Articulo { get; set; }
        public Cotizacion Cotizacion { get; set; }
        public string Estado { get; set; }

        public DetalleCotizacion_Articulo()
        {

        }
    }
}