﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class DetalleCotizacion_Proveedor
    {
        public string Id { get; set; }
        public Cotizacion Cotizacion { get; set; }
        public Proveedor Proveedor { get; set; }
        public string Estado { get; set; }

        public DetalleCotizacion_Proveedor()
        {

        }
    }
}