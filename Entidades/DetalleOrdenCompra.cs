﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class DetalleOrdenCompra
    {
        public string Id { get; set; }
        public OrdenCompra OrdenCompra { get; set; }
        public Articulo Articulo { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Importe { get; set; }
        public string Estado { get; set; }



    }

}