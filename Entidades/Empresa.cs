﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Empresa
    {
        public string Id { get; set; }
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string NombreAbreviado { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Fax { get; set; }
        public string Estado { get; set; }

        public Empresa()
        {

        }

        public Empresa(string id = "", string ruc = "", string razonSocial = "", string nombreAbreviado = "", string direccion = "", string telefono = "", string fax = "", string estado = "")
        {
            Id = id;
            Ruc = ruc;
            RazonSocial = razonSocial;
            NombreAbreviado = nombreAbreviado;
            Direccion = direccion;
            Telefono = telefono;
            Fax = fax;
            Estado = estado;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(NombreAbreviado.Length > 0 ? (NombreAbreviado) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }

        }

    }
}