﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Familia
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }

        public Familia()
        {

        }

        public Familia(string id, string des, string estado)
        {
            Id = id;
            Descripcion = des;
            Estado = estado;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(Descripcion.Length > 0 ? (Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }

        }
    }
}