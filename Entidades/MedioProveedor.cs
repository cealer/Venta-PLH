﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class MedioProveedor
    {
        public string Id { get; set; }
        public TipoMedio TipoMedio { get; set; }
        public Proveedor Proveedor { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }


        public MedioProveedor()
        {

        }

        public MedioProveedor(string id, TipoMedio tipoMedio, Proveedor proveedor, string descripcion, string estado)
        {
            Id = id;
            TipoMedio = tipoMedio;
            Proveedor = proveedor;
            Descripcion = descripcion;
            Estado = estado;
        }



    }
}