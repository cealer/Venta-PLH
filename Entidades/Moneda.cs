﻿using System;
using System.Text;

namespace Entidades
{
    public class Moneda
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Simbolo { get; set; }
        public string Estado { get; set; }

        public Moneda()
        {

        }

        public Moneda(string id, string des, string estado)
        {
            Id = id;
            Descripcion = des;
            Estado = estado;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(Descripcion.Length > 0 ? (Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }

        }
    }
}