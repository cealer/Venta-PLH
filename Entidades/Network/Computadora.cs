﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Network
{
    public class Computadora
    {
        public string Nombre { get; set; }
        public string IPv4 { get; set; }
        public string MAC { get; set; }

        public Computadora()
        {

        }

        public Computadora(string nombre, string pv4, string mAC)
        {
            Nombre = nombre;
            IPv4 = pv4;
            MAC = mAC;
        }
    }
}
