﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Network
{
    public class Red
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string MAC { get; set; }
        public string Velocidad { get; set; }

        public Red()
        {

        }

        public Red(string nombre, string mAC, string velocidad, string descripcion)
        {
            Nombre = nombre;
            Descripcion = descripcion;
            MAC = mAC;
            Velocidad = velocidad;
        }

    }
}
