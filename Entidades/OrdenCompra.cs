﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class OrdenCompra
    {
        public string Id { get; set; }
        public Empresa Empresa { get; set; }
        public Sucursal Sucursal { get; set; }
        public Usuario Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public Moneda Moneda { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public decimal Igv { get; set; }
        public string Estado { get; set; }

        //public Articulo Articulo { get; set; }

        public OrdenCompra()
        {

        }
    }
}