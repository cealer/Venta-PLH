﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class PermisoSucursales
    {

        public string Id { get; set; }
        public Usuario usuario { get; set; }
        public Sucursal sucursal { get; set; }
        public Empresa empresa { get; set; }
        public string Estado { get; set; }

        public PermisoSucursales()
        {

        }

        public PermisoSucursales(string id = "", Usuario us = null, Empresa em = null, Sucursal su = null, string estado = "")
        {
            Id = id;
            usuario = usuario;
            sucursal = su;
            Estado = estado;
            empresa = em;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(sucursal._Empresa.NombreAbreviado.Length > 0 ? (sucursal._Empresa.NombreAbreviado) : "");
            sb.Append(sucursal.Descripcion.Length > 0 ? (" - " + sucursal.Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "Error al cargar datos";
            }
        }
    }
}