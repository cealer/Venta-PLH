﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Persona
    {
        public string Id { get; set; }
        public TipoPersona TipoPersona { get; set; }
        public string Nombres { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string DNI { get; set; }
        public DateTime? FecNac { get; set; }
        public string Sexo { get; set; }
        public string Estado { get; set; }

        public Persona()
        {

        }

        public Persona(string id = "", TipoPersona per = null, string nombres = "", string apellidoP = "", string apellidoM = "", string dNI = "", DateTime? fecNac = null, string sexo = "", string estado = "")
        {
            Id = id;
            TipoPersona = per;
            Nombres = nombres;
            ApellidoM = apellidoM;
            ApellidoP = apellidoP;
            DNI = dNI;
            FecNac = fecNac;
            Sexo = sexo;
            Estado = estado;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(DNI.Length > 0 ? (DNI) : "");
            sb.Append(ApellidoP.Length > 0 ? (" - " + ApellidoP) : "");
            sb.Append(ApellidoM.Length > 0 ? (" - " + ApellidoM) : "");
            sb.Append(Nombres.Length > 0 ? (" - " + Nombres) : "");
            sb.Append(TipoPersona.Descripcion.Length > 0 ? (" - " + TipoPersona.Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "Error al cargar datos";
            }
        }
    }
}