﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Proveedor
    {
        public string Id { get; set; }
        public Empresa Empresa { get; set; }
        public string RUC { get; set; }
        public string RazonSocial { get; set; }
        public string Estado { get; set; }

        public Proveedor()
        {

        }

        public Proveedor(string id, Empresa empresa, string rUC, string razonSocial, string estado)
        {
            Id = id;
            Empresa = empresa;
            RUC = rUC;
            RazonSocial = razonSocial;
            Estado = estado;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();

            sb.Append(RazonSocial != null ? (RazonSocial) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
