﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Sucursal
    {
        public string Id { get; set; }
        public Empresa _Empresa { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }

        public Sucursal()
        {

        }

        public Sucursal(string id, Empresa empresa, string descripcion, string direccion, string estado)
        {
            Id = id; _Empresa = empresa;
            Descripcion = descripcion;
            Direccion = direccion;
            Estado = estado;
        }


        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(Descripcion.Length > 0 ? (Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }

        }

    }
}
