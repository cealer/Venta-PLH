﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class TipoPersona
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }

        public TipoPersona(string id, string des, string est)
        {
            Id = id;
            Descripcion = des;
            Estado = est;
        }

        public TipoPersona()
        {

        }
    }
}
