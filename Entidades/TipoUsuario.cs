﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class TipoUsuario
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }

        public TipoUsuario()
        {

        }

        public TipoUsuario(string id, string des)
        {
            Id = id;
            Descripcion = des;
        }

        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(Descripcion.Length > 0 ? (Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "Error al cargar datos";
            }
        }

    }
}
