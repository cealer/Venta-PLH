﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Unidad
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string NombreCorto { get; set; }
        public string Estado { get; set; }

        public Unidad()
        {

        }

        public Unidad(string id, string des, string nom, string estado)
        {
            Id = id;
            Descripcion = des;
            NombreCorto = nom;
            Estado = estado;
        }
        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(Descripcion.Length > 0 ? (Descripcion) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }

        }

    }
}
