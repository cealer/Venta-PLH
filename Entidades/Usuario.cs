﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Usuario
    {

        public string Id { get; set; }
        public Persona persona { get; set; }
        public TipoUsuario tipoUsuario { get; set; }
        public Sucursal Sucursal { get; set; }
        public Empresa Empresa { get; set; }
        public string _Usuario { get; set; }
        public string Pass { get; set; }
        public string Estado { get; set; }

        public Usuario()
        {

        }

        public Usuario(string id = "", TipoUsuario _tipoUsuario = null, Persona per = null, Sucursal sucursal = null, Empresa empresa = null, string usuario = "", string pass = "", string estado = "")
        {
            Id = id;
            tipoUsuario = _tipoUsuario;
            persona = per;
            Sucursal = sucursal;
            Empresa = empresa;
            _Usuario = usuario;
            Pass = pass;
            Estado = estado;
        }


        public string VerDatosCadena()
        {
            var sb = new StringBuilder();
            sb.Append(_Usuario.Length > 0 ? (_Usuario) : "");
            return sb.ToString();
        }

        public override string ToString()
        {
            try
            {
                return VerDatosCadena();
            }
            catch (Exception)
            {
                return "";
            }

        }

    }
}