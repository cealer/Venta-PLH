﻿namespace Presentacion.Formularios.Buscar
{
    partial class FrmBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            this.btnBuscar = new MetroFramework.Controls.MetroButton();
            this.tbxBusqueda1 = new MetroFramework.Controls.MetroTextBox();
            this.lblBuscar1 = new MetroFramework.Controls.MetroLabel();
            this.tbxBusqueda2 = new MetroFramework.Controls.MetroTextBox();
            this.lblBuscar2 = new MetroFramework.Controls.MetroLabel();
            this.tbxBusqueda3 = new MetroFramework.Controls.MetroTextBox();
            this.lblBuscar3 = new MetroFramework.Controls.MetroLabel();
            this.tbxBusqueda4 = new MetroFramework.Controls.MetroTextBox();
            this.lblBuscar4 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(52, 131);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(964, 324);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 43;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            this.dgvLista.DoubleClick += new System.EventHandler(this.dgvLista_DoubleClick);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(953, 69);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(91, 37);
            this.btnBuscar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscar.TabIndex = 52;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscar.UseSelectable = true;
            this.btnBuscar.UseStyleColors = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // tbxBusqueda1
            // 
            // 
            // 
            // 
            this.tbxBusqueda1.CustomButton.Image = null;
            this.tbxBusqueda1.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxBusqueda1.CustomButton.Name = "";
            this.tbxBusqueda1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBusqueda1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBusqueda1.CustomButton.TabIndex = 1;
            this.tbxBusqueda1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBusqueda1.CustomButton.UseSelectable = true;
            this.tbxBusqueda1.CustomButton.Visible = false;
            this.tbxBusqueda1.Lines = new string[0];
            this.tbxBusqueda1.Location = new System.Drawing.Point(112, 76);
            this.tbxBusqueda1.MaxLength = 11;
            this.tbxBusqueda1.Name = "tbxBusqueda1";
            this.tbxBusqueda1.PasswordChar = '\0';
            this.tbxBusqueda1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBusqueda1.SelectedText = "";
            this.tbxBusqueda1.SelectionLength = 0;
            this.tbxBusqueda1.SelectionStart = 0;
            this.tbxBusqueda1.Size = new System.Drawing.Size(118, 23);
            this.tbxBusqueda1.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBusqueda1.TabIndex = 44;
            this.tbxBusqueda1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBusqueda1.UseSelectable = true;
            this.tbxBusqueda1.Visible = false;
            this.tbxBusqueda1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBusqueda1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblBuscar1
            // 
            this.lblBuscar1.AutoSize = true;
            this.lblBuscar1.Location = new System.Drawing.Point(23, 76);
            this.lblBuscar1.Name = "lblBuscar1";
            this.lblBuscar1.Size = new System.Drawing.Size(66, 19);
            this.lblBuscar1.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar1.TabIndex = 45;
            this.lblBuscar1.Text = "Busqueda";
            this.lblBuscar1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar1.UseStyleColors = true;
            this.lblBuscar1.Visible = false;
            // 
            // tbxBusqueda2
            // 
            // 
            // 
            // 
            this.tbxBusqueda2.CustomButton.Image = null;
            this.tbxBusqueda2.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxBusqueda2.CustomButton.Name = "";
            this.tbxBusqueda2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBusqueda2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBusqueda2.CustomButton.TabIndex = 1;
            this.tbxBusqueda2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBusqueda2.CustomButton.UseSelectable = true;
            this.tbxBusqueda2.CustomButton.Visible = false;
            this.tbxBusqueda2.Lines = new string[0];
            this.tbxBusqueda2.Location = new System.Drawing.Point(342, 76);
            this.tbxBusqueda2.MaxLength = 11;
            this.tbxBusqueda2.Name = "tbxBusqueda2";
            this.tbxBusqueda2.PasswordChar = '\0';
            this.tbxBusqueda2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBusqueda2.SelectedText = "";
            this.tbxBusqueda2.SelectionLength = 0;
            this.tbxBusqueda2.SelectionStart = 0;
            this.tbxBusqueda2.Size = new System.Drawing.Size(118, 23);
            this.tbxBusqueda2.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBusqueda2.TabIndex = 46;
            this.tbxBusqueda2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBusqueda2.UseSelectable = true;
            this.tbxBusqueda2.Visible = false;
            this.tbxBusqueda2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBusqueda2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblBuscar2
            // 
            this.lblBuscar2.AutoSize = true;
            this.lblBuscar2.Location = new System.Drawing.Point(253, 76);
            this.lblBuscar2.Name = "lblBuscar2";
            this.lblBuscar2.Size = new System.Drawing.Size(66, 19);
            this.lblBuscar2.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar2.TabIndex = 47;
            this.lblBuscar2.Text = "Busqueda";
            this.lblBuscar2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar2.UseStyleColors = true;
            this.lblBuscar2.Visible = false;
            // 
            // tbxBusqueda3
            // 
            // 
            // 
            // 
            this.tbxBusqueda3.CustomButton.Image = null;
            this.tbxBusqueda3.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxBusqueda3.CustomButton.Name = "";
            this.tbxBusqueda3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBusqueda3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBusqueda3.CustomButton.TabIndex = 1;
            this.tbxBusqueda3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBusqueda3.CustomButton.UseSelectable = true;
            this.tbxBusqueda3.CustomButton.Visible = false;
            this.tbxBusqueda3.Lines = new string[0];
            this.tbxBusqueda3.Location = new System.Drawing.Point(565, 76);
            this.tbxBusqueda3.MaxLength = 11;
            this.tbxBusqueda3.Name = "tbxBusqueda3";
            this.tbxBusqueda3.PasswordChar = '\0';
            this.tbxBusqueda3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBusqueda3.SelectedText = "";
            this.tbxBusqueda3.SelectionLength = 0;
            this.tbxBusqueda3.SelectionStart = 0;
            this.tbxBusqueda3.Size = new System.Drawing.Size(118, 23);
            this.tbxBusqueda3.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBusqueda3.TabIndex = 48;
            this.tbxBusqueda3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBusqueda3.UseSelectable = true;
            this.tbxBusqueda3.Visible = false;
            this.tbxBusqueda3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBusqueda3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblBuscar3
            // 
            this.lblBuscar3.AutoSize = true;
            this.lblBuscar3.Location = new System.Drawing.Point(476, 76);
            this.lblBuscar3.Name = "lblBuscar3";
            this.lblBuscar3.Size = new System.Drawing.Size(66, 19);
            this.lblBuscar3.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar3.TabIndex = 49;
            this.lblBuscar3.Text = "Busqueda";
            this.lblBuscar3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar3.UseStyleColors = true;
            this.lblBuscar3.Visible = false;
            // 
            // tbxBusqueda4
            // 
            // 
            // 
            // 
            this.tbxBusqueda4.CustomButton.Image = null;
            this.tbxBusqueda4.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxBusqueda4.CustomButton.Name = "";
            this.tbxBusqueda4.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBusqueda4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBusqueda4.CustomButton.TabIndex = 1;
            this.tbxBusqueda4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBusqueda4.CustomButton.UseSelectable = true;
            this.tbxBusqueda4.CustomButton.Visible = false;
            this.tbxBusqueda4.Lines = new string[0];
            this.tbxBusqueda4.Location = new System.Drawing.Point(796, 76);
            this.tbxBusqueda4.MaxLength = 11;
            this.tbxBusqueda4.Name = "tbxBusqueda4";
            this.tbxBusqueda4.PasswordChar = '\0';
            this.tbxBusqueda4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBusqueda4.SelectedText = "";
            this.tbxBusqueda4.SelectionLength = 0;
            this.tbxBusqueda4.SelectionStart = 0;
            this.tbxBusqueda4.Size = new System.Drawing.Size(118, 23);
            this.tbxBusqueda4.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBusqueda4.TabIndex = 50;
            this.tbxBusqueda4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBusqueda4.UseSelectable = true;
            this.tbxBusqueda4.Visible = false;
            this.tbxBusqueda4.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBusqueda4.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblBuscar4
            // 
            this.lblBuscar4.AutoSize = true;
            this.lblBuscar4.Location = new System.Drawing.Point(707, 76);
            this.lblBuscar4.Name = "lblBuscar4";
            this.lblBuscar4.Size = new System.Drawing.Size(66, 19);
            this.lblBuscar4.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar4.TabIndex = 51;
            this.lblBuscar4.Text = "Busqueda";
            this.lblBuscar4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar4.UseStyleColors = true;
            this.lblBuscar4.Visible = false;
            // 
            // FrmBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(1067, 478);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.lblBuscar4);
            this.Controls.Add(this.tbxBusqueda4);
            this.Controls.Add(this.lblBuscar3);
            this.Controls.Add(this.tbxBusqueda3);
            this.Controls.Add(this.lblBuscar2);
            this.Controls.Add(this.tbxBusqueda2);
            this.Controls.Add(this.lblBuscar1);
            this.Controls.Add(this.tbxBusqueda1);
            this.Controls.Add(this.dgvLista);
            this.Name = "FrmBuscar";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Buscar";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmBuscar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid dgvLista;
        private MetroFramework.Controls.MetroButton btnBuscar;
        private MetroFramework.Controls.MetroTextBox tbxBusqueda1;
        private MetroFramework.Controls.MetroLabel lblBuscar1;
        private MetroFramework.Controls.MetroTextBox tbxBusqueda2;
        private MetroFramework.Controls.MetroLabel lblBuscar2;
        private MetroFramework.Controls.MetroTextBox tbxBusqueda3;
        private MetroFramework.Controls.MetroLabel lblBuscar3;
        private MetroFramework.Controls.MetroTextBox tbxBusqueda4;
        private MetroFramework.Controls.MetroLabel lblBuscar4;
    }
}