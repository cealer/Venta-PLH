﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using System.Reflection;
using System.Collections;

namespace Presentacion.Formularios.Buscar
{
    public partial class FrmBuscar : MetroForm
    {
        public string Tabla { get; set; }

        public string[] Resultados { get; } = new string[2];

        public FrmBuscar()
        {
            InitializeComponent();
        }

        private void Listar()
        {
            //Cargar la librería
            var assembly = Assembly.LoadFrom("BOL.dll");
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));
            //Establecer el nombre de la clase de la librería que se usara
            string nombreclase = $"BOL.{Tabla}BOL";
            //Obtener dicha clase
            var clase = assembly.GetType(nombreclase);
            //Instanciar la clase
            var obj = Activator.CreateInstance(clase);
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            //Crear objetos que contengan los resultados
            var mParam = new object[] { };
            //Invocar el método que se quiere llamar y obtener lo que devuelva
            var lista = (IList)clase.InvokeMember("BuscarUltimos", BindingFlags.InvokeMethod, null, obj, mParam);
            if (lista == null) throw new ArgumentNullException(nameof(lista));
            //Llenar el datagridview con los resultados obtenidos
            dgvLista.DataSource = lista;
        }

        private void FrmBuscar_Load(object sender, EventArgs e)
        {
            Text += $" {Tabla}";
            Listar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            if (dgvLista.CurrentRow != null && dgvLista.CurrentRow.Index <= -1) return;
            var obj = (object)dgvLista.CurrentRow?.DataBoundItem;
            Resultados[0] = dgvLista.CurrentRow?.Cells["Id"].Value.ToString();
            Resultados[1] = obj?.ToString();
            this.Close();
        }
    }
}