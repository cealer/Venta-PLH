﻿namespace Presentacion.Formularios.Buscar
{
    partial class FrmBuscarArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnBuscar = new MetroFramework.Controls.MetroButton();
            this.lblBuscar2 = new MetroFramework.Controls.MetroLabel();
            this.tbxDes = new MetroFramework.Controls.MetroTextBox();
            this.lblBuscar1 = new MetroFramework.Controls.MetroLabel();
            this.tbxRuc = new MetroFramework.Controls.MetroTextBox();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(882, 63);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(91, 37);
            this.btnBuscar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscar.TabIndex = 68;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscar.UseSelectable = true;
            this.btnBuscar.UseStyleColors = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // lblBuscar2
            // 
            this.lblBuscar2.AutoSize = true;
            this.lblBuscar2.Location = new System.Drawing.Point(9, 77);
            this.lblBuscar2.Name = "lblBuscar2";
            this.lblBuscar2.Size = new System.Drawing.Size(83, 19);
            this.lblBuscar2.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar2.TabIndex = 67;
            this.lblBuscar2.Text = "Descripción: ";
            this.lblBuscar2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar2.UseStyleColors = true;
            // 
            // tbxDes
            // 
            // 
            // 
            // 
            this.tbxDes.CustomButton.Image = null;
            this.tbxDes.CustomButton.Location = new System.Drawing.Point(299, 1);
            this.tbxDes.CustomButton.Name = "";
            this.tbxDes.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxDes.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxDes.CustomButton.TabIndex = 1;
            this.tbxDes.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxDes.CustomButton.UseSelectable = true;
            this.tbxDes.CustomButton.Visible = false;
            this.tbxDes.Lines = new string[0];
            this.tbxDes.Location = new System.Drawing.Point(98, 77);
            this.tbxDes.MaxLength = 11;
            this.tbxDes.Name = "tbxDes";
            this.tbxDes.PasswordChar = '\0';
            this.tbxDes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxDes.SelectedText = "";
            this.tbxDes.SelectionLength = 0;
            this.tbxDes.SelectionStart = 0;
            this.tbxDes.Size = new System.Drawing.Size(321, 23);
            this.tbxDes.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxDes.TabIndex = 66;
            this.tbxDes.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxDes.UseSelectable = true;
            this.tbxDes.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxDes.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblBuscar1
            // 
            this.lblBuscar1.AutoSize = true;
            this.lblBuscar1.Location = new System.Drawing.Point(479, 77);
            this.lblBuscar1.Name = "lblBuscar1";
            this.lblBuscar1.Size = new System.Drawing.Size(57, 19);
            this.lblBuscar1.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar1.TabIndex = 65;
            this.lblBuscar1.Text = "Familia: ";
            this.lblBuscar1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar1.UseStyleColors = true;
            // 
            // tbxRuc
            // 
            // 
            // 
            // 
            this.tbxRuc.CustomButton.Image = null;
            this.tbxRuc.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxRuc.CustomButton.Name = "";
            this.tbxRuc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxRuc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxRuc.CustomButton.TabIndex = 1;
            this.tbxRuc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxRuc.CustomButton.UseSelectable = true;
            this.tbxRuc.CustomButton.Visible = false;
            this.tbxRuc.Lines = new string[0];
            this.tbxRuc.Location = new System.Drawing.Point(568, 77);
            this.tbxRuc.MaxLength = 11;
            this.tbxRuc.Name = "tbxRuc";
            this.tbxRuc.PasswordChar = '\0';
            this.tbxRuc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxRuc.SelectedText = "";
            this.tbxRuc.SelectionLength = 0;
            this.tbxRuc.SelectionStart = 0;
            this.tbxRuc.Size = new System.Drawing.Size(118, 23);
            this.tbxRuc.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxRuc.TabIndex = 64;
            this.tbxRuc.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxRuc.UseSelectable = true;
            this.tbxRuc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxRuc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(9, 132);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(964, 324);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 63;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            this.dgvLista.SelectionChanged += new System.EventHandler(this.dgvLista_SelectionChanged);
            this.dgvLista.DoubleClick += new System.EventHandler(this.dgvLista_DoubleClick);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(1030, 5);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(15, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 70;
            this.metroLabel1.Text = "x";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // FrmBuscarArticulo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 501);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.lblBuscar2);
            this.Controls.Add(this.tbxDes);
            this.Controls.Add(this.lblBuscar1);
            this.Controls.Add(this.tbxRuc);
            this.Controls.Add(this.dgvLista);
            this.MaximizeBox = false;
            this.Name = "FrmBuscarArticulo";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Buscar Artículo";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmBuscarArticulo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnBuscar;
        private MetroFramework.Controls.MetroLabel lblBuscar2;
        private MetroFramework.Controls.MetroTextBox tbxDes;
        private MetroFramework.Controls.MetroLabel lblBuscar1;
        private MetroFramework.Controls.MetroTextBox tbxRuc;
        private MetroFramework.Controls.MetroGrid dgvLista;
        private MetroFramework.Controls.MetroLabel metroLabel1;
    }
}