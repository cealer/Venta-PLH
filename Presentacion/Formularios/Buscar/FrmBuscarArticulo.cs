﻿using BOL;
using Entidades;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Buscar
{
    public partial class FrmBuscarArticulo : MetroForm
    {
        internal Articulo ent;

        public string IdEmpresa { get; internal set; }
        public string IdSucursal { get; internal set; }
        public string IdUsuario { get; internal set; }
        public string IdMoneda { get; internal set; }

        public FrmBuscarArticulo()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Articulo();
            aux._Empresa = new Entidades.Empresa();
            aux.Sucursal = new Sucursal();
            aux.Usuario = new Usuario();
            aux._Empresa.Id = IdEmpresa;
            aux.Sucursal.Id = IdSucursal;
            aux.Usuario.Id = IdUsuario;
            aux.moneda = new Moneda();
            aux.moneda.Id = IdMoneda;
            aux.Descripcion = tbxDes.Text;

            dgvLista.DataSource = ArticuloBOL.Buscar(aux).Where(x => x.moneda.Id.Equals(IdMoneda)).ToList();
        }

        private void OcultarCampos()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["_Empresa"].Visible = false;
            dgvLista.Columns["Sucursal"].Visible = false;
            dgvLista.Columns["Usuario"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        //private void Ultimos()
        //{
        //    var lista = ArticuloBOL.BuscarUltimos(IdEmpresa, IdSucursal);
        //    dgvLista.DataSource = lista.ToList();
        //    OcultarCampos();
        //}

        private void FrmBuscarArticulo_Load(object sender, EventArgs e)
        {
            //Ultimos();
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLista.SelectedRows.Count > -1)
                {
                    ent = (Articulo)dgvLista.CurrentRow.DataBoundItem;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al seleccionar");
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {
            ent = null;
            this.Dispose();
        }
    }
}