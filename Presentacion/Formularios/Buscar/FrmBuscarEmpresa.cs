﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;

namespace Presentacion.Formularios.Buscar
{
    public partial class FrmBuscarEmpresa : MetroForm
    {
        public string[] resultados = new string[2];

        public FrmBuscarEmpresa()
        {
            InitializeComponent();
        }

        void Listar()
        {
            dgvLista.DataSource = EmpresaBOL.BuscarUltimos();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Empresa();
            aux.RazonSocial = tbxRazonSocial.Text;
            aux.Ruc = tbxRuc.Text;
            dgvLista.DataSource = EmpresaBOL.Buscar(aux);
        }

        private void FrmBuscarEmpresa_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            if (dgvLista.CurrentRow.Index > -1)
            {
                resultados[0] = dgvLista.CurrentRow.Cells["Id"].Value.ToString();
                resultados[1] = dgvLista.CurrentRow.Cells["RazonSocial"].Value.ToString();
                this.Close();
            }
        }
    }
}