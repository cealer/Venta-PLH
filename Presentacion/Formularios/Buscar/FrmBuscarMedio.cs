﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using Entidades;
using MetroFramework.Forms;

namespace Presentacion.Formularios.Buscar
{
    public partial class FrmBuscarMedio : MetroForm
    {
        public TipoMedio obj = new TipoMedio();

        void Listar()
        {
            dgvLista.DataSource = TipoMedioBOL.Buscar(new TipoMedio("", "", ""));
        }

        public FrmBuscarMedio()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dgvLista.DataSource = TipoMedioBOL.Buscar(new TipoMedio("", tbxDes.Text, ""));
        }

        private void FrmBuscarMedio_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            if (dgvLista.SelectedRows.Count == 1)
            {
                obj = (TipoMedio)dgvLista.CurrentRow.DataBoundItem;
                this.Close();
            }
        }
    }
}
