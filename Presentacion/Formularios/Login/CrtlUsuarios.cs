﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Presentacion.Formularios.Login
{
    public partial class CrtlUsuarios : MetroUserControl
    {
        public CrtlUsuarios()
        {
            InitializeComponent();
        }

        private void pbxIcono_Click(object sender, EventArgs e)
        {
            var currentForm = Form.ActiveForm;
            currentForm.Hide();
            var form = new FrmLoginUsuario();
            form.Usuario = lblUsuario.Text;
            form.ShowDialog();
        }
    }
}
