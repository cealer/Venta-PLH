﻿namespace Presentacion.Formularios.Login
{
    partial class FrmCargaUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.crtlUsuarios2 = new Presentacion.Formularios.Login.CrtlUsuarios();
            this.crtlUsuarios1 = new Presentacion.Formularios.Login.CrtlUsuarios();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.crtlUsuarios2);
            this.metroPanel1.Controls.Add(this.crtlUsuarios1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 60);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1000, 415);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // crtlUsuarios2
            // 
            this.crtlUsuarios2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.crtlUsuarios2.Location = new System.Drawing.Point(535, 107);
            this.crtlUsuarios2.Name = "crtlUsuarios2";
            this.crtlUsuarios2.Size = new System.Drawing.Size(265, 218);
            this.crtlUsuarios2.TabIndex = 3;
            this.crtlUsuarios2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.crtlUsuarios2.UseSelectable = true;
            // 
            // crtlUsuarios1
            // 
            this.crtlUsuarios1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.crtlUsuarios1.Location = new System.Drawing.Point(168, 107);
            this.crtlUsuarios1.Name = "crtlUsuarios1";
            this.crtlUsuarios1.Size = new System.Drawing.Size(269, 213);
            this.crtlUsuarios1.TabIndex = 2;
            this.crtlUsuarios1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.crtlUsuarios1.UseSelectable = true;
            // 
            // FrmCargaUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 495);
            this.Controls.Add(this.metroPanel1);
            this.Name = "FrmCargaUsuario";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "USUARIOS ";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCargaUsuario_FormClosed);
            this.Load += new System.EventHandler(this.FrmCargaUsuario_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private CrtlUsuarios crtlUsuarios2;
        private CrtlUsuarios crtlUsuarios1;
    }
}