﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Login
{
    public partial class FrmCargaUsuario : MetroForm
    {
        public FrmCargaUsuario()
        {
            InitializeComponent();
        }

        public string Usuario { get; set; }

        private void FrmCargaUsuario_Load(object sender, EventArgs e)
        {
            // crtlUsuarios1.pbxIcono = "";
            crtlUsuarios1.lblUsuario.Text = "CESAR";
            crtlUsuarios2.lblUsuario.Text = "ALAN";
        }

        private void FrmCargaUsuario_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}