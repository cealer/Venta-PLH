﻿namespace Presentacion
{
    partial class FrmLoginUsuario
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUsuario = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbxPass = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tbxUsuario = new MetroFramework.Controls.MetroTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Black;
            this.lblUsuario.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblUsuario.Location = new System.Drawing.Point(392, 164);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(65, 19);
            this.lblUsuario.Style = MetroFramework.MetroColorStyle.Green;
            this.lblUsuario.TabIndex = 1;
            this.lblUsuario.Text = "USUARIO";
            this.lblUsuario.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblUsuario.UseStyleColors = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(392, 126);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(117, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "INICIO DE SESIÓN";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // tbxPass
            // 
            // 
            // 
            // 
            this.tbxPass.CustomButton.Image = null;
            this.tbxPass.CustomButton.Location = new System.Drawing.Point(250, 1);
            this.tbxPass.CustomButton.Name = "";
            this.tbxPass.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxPass.CustomButton.TabIndex = 1;
            this.tbxPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxPass.CustomButton.UseSelectable = true;
            this.tbxPass.CustomButton.Visible = false;
            this.tbxPass.Lines = new string[0];
            this.tbxPass.Location = new System.Drawing.Point(392, 248);
            this.tbxPass.MaxLength = 32767;
            this.tbxPass.Name = "tbxPass";
            this.tbxPass.PasswordChar = '●';
            this.tbxPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPass.SelectedText = "";
            this.tbxPass.SelectionLength = 0;
            this.tbxPass.SelectionStart = 0;
            this.tbxPass.Size = new System.Drawing.Size(272, 23);
            this.tbxPass.TabIndex = 1;
            this.tbxPass.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxPass.UseSelectable = true;
            this.tbxPass.UseSystemPasswordChar = true;
            this.tbxPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbxPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxPass_KeyDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Presentacion.Properties.Resources.Invitado;
            this.pictureBox1.Location = new System.Drawing.Point(182, 126);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(178, 175);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Black;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.metroLabel1.Location = new System.Drawing.Point(392, 226);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(78, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 6;
            this.metroLabel1.Text = "Contraseña:";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // tbxUsuario
            // 
            // 
            // 
            // 
            this.tbxUsuario.CustomButton.Image = null;
            this.tbxUsuario.CustomButton.Location = new System.Drawing.Point(250, 1);
            this.tbxUsuario.CustomButton.Name = "";
            this.tbxUsuario.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxUsuario.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxUsuario.CustomButton.TabIndex = 1;
            this.tbxUsuario.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxUsuario.CustomButton.UseSelectable = true;
            this.tbxUsuario.CustomButton.Visible = false;
            this.tbxUsuario.Lines = new string[0];
            this.tbxUsuario.Location = new System.Drawing.Point(392, 188);
            this.tbxUsuario.MaxLength = 32767;
            this.tbxUsuario.Name = "tbxUsuario";
            this.tbxUsuario.PasswordChar = '\0';
            this.tbxUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxUsuario.SelectedText = "";
            this.tbxUsuario.SelectionLength = 0;
            this.tbxUsuario.SelectionStart = 0;
            this.tbxUsuario.Size = new System.Drawing.Size(272, 23);
            this.tbxUsuario.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxUsuario.TabIndex = 0;
            this.tbxUsuario.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxUsuario.UseSelectable = true;
            this.tbxUsuario.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxUsuario.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // FrmLoginUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 429);
            this.ControlBox = false;
            this.Controls.Add(this.tbxUsuario);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.tbxPass);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmLoginUsuario";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmLoginUsuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel lblUsuario;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox tbxPass;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox tbxUsuario;
    }
}

