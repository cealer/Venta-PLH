﻿using BOL;
using MetroFramework;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmLoginUsuario : MetroForm
    {
        public string Usuario { get; set; }

        public FrmLoginUsuario()
        {
            InitializeComponent();
        }

        private void SetUsuario()
        {
            Usuario = tbxUsuario.Text;
        }

        private void lblCambioUsuario_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
            var aux = new Formularios.Login.FrmCargaUsuario();
            aux.Show();
        }

        private void FrmLoginUsuario_Load(object sender, EventArgs e)
        {
            new Utilidades.Apariencia.Control_PictureBox().FormaCircular(pictureBox1);
        }

        private void tbxPass_KeyDown(object sender, KeyEventArgs e)
        {
            //try
            //{
                var ent = new Entidades.Usuario();
                ent._Usuario = tbxUsuario.Text;
                ent.Pass = tbxPass.Text;

                if (e.KeyCode == Keys.Enter)
                {
                    if (UsuarioBOL.Autenticar(ent._Usuario, ent.Pass))
                    {
                        var respuesta = UsuarioBOL.Buscar(ent).FirstOrDefault();

                        SetUsuario();
                        MetroMessageBox.Show(this, $"Bienvenido {Usuario}", "Incio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                        this.Hide();
                        var aux = new Formularios.Menu_Principal.FrmPrincipal();
                        //setUsuario();
                        aux.usuario = respuesta;
                        aux.ShowDialog();

                    }
                    else
                    {
                        MetroMessageBox.Show(this, $"Datos incorrectos.", "Incio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Error");
            //}

        }
    }
}