﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using BOL;
using MetroFramework;

namespace Presentacion.Formularios.Logistica
{
    public partial class FrmCotizacion : MetroForm
    {
        public Articulo articulo { get; set; }
        public Cotizacion ent { get; set; }
        //public Articulo articulo { get; set; }

        public string Empresa { get; internal set; }
        public string Sucursal { get; internal set; }
        public string Usuario { get; internal set; }
        public string Moneda { get; set; }

        List<Articulo> ListaArt = new List<Articulo>();
        List<OrdenCompra> ListaOC = new List<OrdenCompra>();


        public FrmCotizacion()
        {
            InitializeComponent();
        }

        private void Habilitar()
        {
            btnAdd.Enabled = true;
            btnQuitar.Enabled = true;
            btnCancelar.Enabled = true;
            btnGuardar.Enabled = true;

        }


        private void CargarArticulos()
        {
            dgvLista.DataSource = ListaArt.ToList();
        }

        private void DataGridFormato()
        {

            dgvLista.Columns[0].Visible = false;
            dgvLista.Columns[2].Visible = false;
            dgvLista.Columns[3].Visible = false;
            dgvLista.Columns[5].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        //private void Ultimos()
        //{
        //    List<OrdenCompra> lista = new List<OrdenCompra>();
        //    dgvListaOC.DataSource = lista.ToList();
        //}

        private void FrmCompraOrden_Load(object sender, EventArgs e)
        {
            //Ultimos();
            dgvLista.DataSource = ListaArt.ToList();
            DataGridFormato();
            CargarMoneda();
        }

        private void CargarMoneda()
        {
            cboMoneda.DataSource = MonedaBOL.Buscar(new Moneda("", "", ""));
            cboMoneda.DisplayMember = "Descripcion";
            cboMoneda.ValueMember = "Id";
        }

        private void FrmCompraOrden_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
            DataGridFormato();
            ent = new Cotizacion();
            btnNuevo.Enabled = false;
            Habilitar();
            ent.Id = "";
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            Buscar.FrmBuscarArticulo aux = new Buscar.FrmBuscarArticulo();
            aux.IdEmpresa = Empresa;
            aux.IdSucursal = Sucursal;
            aux.IdUsuario = Usuario;
            aux.IdMoneda = Moneda;
            aux.ShowDialog();

            if (aux.ent != null)
            {
                var repetido = ListaArt.Find(x => x.Id.Equals(aux.ent.Id));

                if (repetido == null)
                {
                    ListaArt.Add(aux.ent);
                    CargarArticulos();
                }
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            ListaArt.Remove(articulo);
            CargarArticulos();
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            articulo = (Articulo)dgvLista.CurrentRow.DataBoundItem;
        }

        private void btnEstablecer_Click(object sender, EventArgs e)
        {
            Moneda = cboMoneda.SelectedValue.ToString();
            btnEstablecer.Enabled = false;
            cboMoneda.Enabled = false;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ent.Descripcion = tbxDescripcion.Text;
                ent.Empresa = new Entidades.Empresa();
                ent.Sucursal = new Entidades.Sucursal();
                ent.Usuario = new Entidades.Usuario();
                ent.Empresa.Id = Empresa;
                ent.Sucursal.Id = Sucursal;
                ent.Usuario.Id = Usuario;

                if (string.IsNullOrWhiteSpace(ent.Descripcion) == true)
                {
                    MessageBox.Show("No puede estar vacío.", "");
                }
                else
                {
                    var r = CotizacionBOL.Registro(ent);

                    if (r == false)
                    {
                        var Max = CotizacionBOL.Max();
                        foreach (var item in ListaArt)
                        {
                            var detalle = new DetalleCotizacion_Articulo();
                            detalle.Cotizacion = new Cotizacion();
                            detalle.Cotizacion.Id = Max;
                            detalle.Articulo = new Articulo();
                            detalle.Articulo.Id = item.Id;
                            DetalleCotizacion_ArticuloBOL.Registro(detalle);
                        }
                        //Limpiar();
                        //DesHabilitar();
                        //Ultimos();
                        MetroMessageBox.Show(this, $"Guardado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }
                    else
                    {
                        //Ultimos();
                        //Limpiar();
                        //DesHabilitar();
                        MetroMessageBox.Show(this, $"Modifidado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }

                }
            }

            catch (Exception ex)
            {
                //    MessageBox.Show(ex.Message);
            }
        }
    }
}