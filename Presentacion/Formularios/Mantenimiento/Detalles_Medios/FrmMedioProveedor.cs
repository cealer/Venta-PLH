﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using Entidades;
using MetroFramework;
using MetroFramework.Forms;

namespace Presentacion.Formularios.Mantenimiento.Detalles_Medios
{
    public partial class FrmMedioProveedor : MetroForm
    {
        public TipoMedio tipo = new TipoMedio();
        public Proveedor proveedor = new Proveedor();

        MedioProveedor medioProveedor = new MedioProveedor();

        void Listar()
        {
            var aux = new MedioProveedor();
            aux.TipoMedio = tipo;
            aux.Proveedor = proveedor;
            var lista = MedioProveedorBOL.Buscar(aux).ToList();
            dgvLista.DataSource = lista;
            FormatoDataGridView();
        }

        void FormatoDataGridView()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["Proveedor"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        private void Limpiar()
        {
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
        }


        public FrmMedioProveedor()
        {
            InitializeComponent();
        }

        private void btnMedios_Click(object sender, EventArgs e)
        {
            var aux = new Buscar.FrmBuscarMedio();
            aux.ShowDialog();
            tipo = aux.obj;
            lblMedio.Text = tipo.Descripcion;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                var aux = new MedioProveedor();
                aux.Id = medioProveedor.Id;
                aux.Proveedor = proveedor;
                aux.TipoMedio = tipo;
                aux.Descripcion = tbxDescripcion.Text;
                var r = MedioProveedorBOL.Registro(aux);
                if (r == false)
                {
                    MetroMessageBox.Show(this, $"Medio registrado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }
                else if (r == true)
                {
                    MetroMessageBox.Show(this, $"Medio editado correctamente.", "Edición", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }
                Limpiar();
                Listar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error al guardar");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmMedioProveedor_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (r.Equals(DialogResult.Yes))
                {
                    MedioProveedorBOL.Eliminar(medioProveedor.Id);
                    Listar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No se pudo Eliminar");
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            tbxDescripcion.Text = medioProveedor.Descripcion;
            lblMedio.Text = medioProveedor.TipoMedio.Descripcion;
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvLista.SelectedRows.Count == 1)
            {
                medioProveedor = (MedioProveedor)dgvLista.CurrentRow.DataBoundItem;
                tipo = medioProveedor.TipoMedio;
            }
        }

        private void dgvLista_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvLista.ClearSelection();
        }
    }
}