﻿namespace Presentacion.Formularios.Mantenimiento
{
    partial class FrmArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnMoneda = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.tbxMoneda = new MetroFramework.Controls.MetroTextBox();
            this.tbxCantidad = new MetroFramework.Controls.MetroTextBox();
            this.tbxPrecio = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnBuscarFamilia = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.tbxDescripcion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbxFamilia = new MetroFramework.Controls.MetroTextBox();
            this.btnCancelar = new MetroFramework.Controls.MetroButton();
            this.btnEliminar = new MetroFramework.Controls.MetroButton();
            this.btnEditar = new MetroFramework.Controls.MetroButton();
            this.btnNuevo = new MetroFramework.Controls.MetroButton();
            this.btnGuardar = new MetroFramework.Controls.MetroButton();
            this.btnUnidad = new MetroFramework.Controls.MetroButton();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.tbxUnidad = new MetroFramework.Controls.MetroTextBox();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.lblBuscar2 = new MetroFramework.Controls.MetroLabel();
            this.tbxBuscarDescripcion = new MetroFramework.Controls.MetroTextBox();
            this.btnBuscar = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMoneda
            // 
            this.btnMoneda.Location = new System.Drawing.Point(248, 144);
            this.btnMoneda.Name = "btnMoneda";
            this.btnMoneda.Size = new System.Drawing.Size(27, 23);
            this.btnMoneda.Style = MetroFramework.MetroColorStyle.Green;
            this.btnMoneda.TabIndex = 4;
            this.btnMoneda.Text = "...";
            this.btnMoneda.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnMoneda.UseSelectable = true;
            this.btnMoneda.UseStyleColors = true;
            this.btnMoneda.Click += new System.EventHandler(this.btnMoneda_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 144);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(65, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 65;
            this.metroLabel3.Text = "Moneda: ";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            // 
            // tbxMoneda
            // 
            // 
            // 
            // 
            this.tbxMoneda.CustomButton.Image = null;
            this.tbxMoneda.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxMoneda.CustomButton.Name = "";
            this.tbxMoneda.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxMoneda.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxMoneda.CustomButton.TabIndex = 1;
            this.tbxMoneda.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxMoneda.CustomButton.UseSelectable = true;
            this.tbxMoneda.CustomButton.Visible = false;
            this.tbxMoneda.Enabled = false;
            this.tbxMoneda.Lines = new string[0];
            this.tbxMoneda.Location = new System.Drawing.Point(111, 144);
            this.tbxMoneda.MaxLength = 250;
            this.tbxMoneda.Name = "tbxMoneda";
            this.tbxMoneda.PasswordChar = '\0';
            this.tbxMoneda.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxMoneda.SelectedText = "";
            this.tbxMoneda.SelectionLength = 0;
            this.tbxMoneda.SelectionStart = 0;
            this.tbxMoneda.Size = new System.Drawing.Size(118, 23);
            this.tbxMoneda.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxMoneda.TabIndex = 64;
            this.tbxMoneda.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxMoneda.UseSelectable = true;
            this.tbxMoneda.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxMoneda.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbxCantidad
            // 
            // 
            // 
            // 
            this.tbxCantidad.CustomButton.Image = null;
            this.tbxCantidad.CustomButton.Location = new System.Drawing.Point(42, 1);
            this.tbxCantidad.CustomButton.Name = "";
            this.tbxCantidad.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxCantidad.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxCantidad.CustomButton.TabIndex = 1;
            this.tbxCantidad.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxCantidad.CustomButton.UseSelectable = true;
            this.tbxCantidad.CustomButton.Visible = false;
            this.tbxCantidad.Enabled = false;
            this.tbxCantidad.Lines = new string[0];
            this.tbxCantidad.Location = new System.Drawing.Point(781, 74);
            this.tbxCantidad.MaxLength = 8;
            this.tbxCantidad.Name = "tbxCantidad";
            this.tbxCantidad.PasswordChar = '\0';
            this.tbxCantidad.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxCantidad.SelectedText = "";
            this.tbxCantidad.SelectionLength = 0;
            this.tbxCantidad.SelectionStart = 0;
            this.tbxCantidad.Size = new System.Drawing.Size(64, 23);
            this.tbxCantidad.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxCantidad.TabIndex = 3;
            this.tbxCantidad.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxCantidad.UseSelectable = true;
            this.tbxCantidad.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxCantidad.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbxPrecio
            // 
            // 
            // 
            // 
            this.tbxPrecio.CustomButton.Image = null;
            this.tbxPrecio.CustomButton.Location = new System.Drawing.Point(42, 1);
            this.tbxPrecio.CustomButton.Name = "";
            this.tbxPrecio.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxPrecio.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxPrecio.CustomButton.TabIndex = 1;
            this.tbxPrecio.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxPrecio.CustomButton.UseSelectable = true;
            this.tbxPrecio.CustomButton.Visible = false;
            this.tbxPrecio.Enabled = false;
            this.tbxPrecio.Lines = new string[0];
            this.tbxPrecio.Location = new System.Drawing.Point(610, 71);
            this.tbxPrecio.MaxLength = 8;
            this.tbxPrecio.Name = "tbxPrecio";
            this.tbxPrecio.PasswordChar = '\0';
            this.tbxPrecio.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPrecio.SelectedText = "";
            this.tbxPrecio.SelectionLength = 0;
            this.tbxPrecio.SelectionStart = 0;
            this.tbxPrecio.Size = new System.Drawing.Size(64, 23);
            this.tbxPrecio.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxPrecio.TabIndex = 2;
            this.tbxPrecio.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxPrecio.UseSelectable = true;
            this.tbxPrecio.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxPrecio.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(551, 71);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(53, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 61;
            this.metroLabel4.Text = "Precio: ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(706, 74);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(47, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 60;
            this.metroLabel1.Text = "Stock: ";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // btnBuscarFamilia
            // 
            this.btnBuscarFamilia.Location = new System.Drawing.Point(503, 71);
            this.btnBuscarFamilia.Name = "btnBuscarFamilia";
            this.btnBuscarFamilia.Size = new System.Drawing.Size(27, 23);
            this.btnBuscarFamilia.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscarFamilia.TabIndex = 1;
            this.btnBuscarFamilia.Text = "...";
            this.btnBuscarFamilia.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscarFamilia.UseSelectable = true;
            this.btnBuscarFamilia.UseStyleColors = true;
            this.btnBuscarFamilia.Click += new System.EventHandler(this.btnBuscarFamilia_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(23, 71);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(83, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel5.TabIndex = 58;
            this.metroLabel5.Text = "Descripción: ";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseStyleColors = true;
            // 
            // tbxDescripcion
            // 
            // 
            // 
            // 
            this.tbxDescripcion.CustomButton.Image = null;
            this.tbxDescripcion.CustomButton.Location = new System.Drawing.Point(96, 1);
            this.tbxDescripcion.CustomButton.Name = "";
            this.tbxDescripcion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxDescripcion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxDescripcion.CustomButton.TabIndex = 1;
            this.tbxDescripcion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxDescripcion.CustomButton.UseSelectable = true;
            this.tbxDescripcion.CustomButton.Visible = false;
            this.tbxDescripcion.Enabled = false;
            this.tbxDescripcion.Lines = new string[0];
            this.tbxDescripcion.Location = new System.Drawing.Point(112, 71);
            this.tbxDescripcion.MaxLength = 250;
            this.tbxDescripcion.Name = "tbxDescripcion";
            this.tbxDescripcion.PasswordChar = '\0';
            this.tbxDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxDescripcion.SelectedText = "";
            this.tbxDescripcion.SelectionLength = 0;
            this.tbxDescripcion.SelectionStart = 0;
            this.tbxDescripcion.Size = new System.Drawing.Size(118, 23);
            this.tbxDescripcion.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxDescripcion.TabIndex = 0;
            this.tbxDescripcion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxDescripcion.UseSelectable = true;
            this.tbxDescripcion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxDescripcion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(306, 71);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(57, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 56;
            this.metroLabel2.Text = "Familia: ";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // tbxFamilia
            // 
            // 
            // 
            // 
            this.tbxFamilia.CustomButton.Image = null;
            this.tbxFamilia.CustomButton.Location = new System.Drawing.Point(82, 1);
            this.tbxFamilia.CustomButton.Name = "";
            this.tbxFamilia.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxFamilia.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxFamilia.CustomButton.TabIndex = 1;
            this.tbxFamilia.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxFamilia.CustomButton.UseSelectable = true;
            this.tbxFamilia.CustomButton.Visible = false;
            this.tbxFamilia.Enabled = false;
            this.tbxFamilia.Lines = new string[0];
            this.tbxFamilia.Location = new System.Drawing.Point(384, 74);
            this.tbxFamilia.MaxLength = 250;
            this.tbxFamilia.Name = "tbxFamilia";
            this.tbxFamilia.PasswordChar = '\0';
            this.tbxFamilia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxFamilia.SelectedText = "";
            this.tbxFamilia.SelectionLength = 0;
            this.tbxFamilia.SelectionStart = 0;
            this.tbxFamilia.Size = new System.Drawing.Size(104, 23);
            this.tbxFamilia.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxFamilia.TabIndex = 55;
            this.tbxFamilia.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxFamilia.UseSelectable = true;
            this.tbxFamilia.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxFamilia.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(869, 294);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 37);
            this.btnCancelar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancelar.UseSelectable = true;
            this.btnCancelar.UseStyleColors = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(869, 239);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(91, 37);
            this.btnEliminar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEliminar.TabIndex = 8;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEliminar.UseSelectable = true;
            this.btnEliminar.UseStyleColors = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(869, 183);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(91, 37);
            this.btnEditar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEditar.TabIndex = 7;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEditar.UseSelectable = true;
            this.btnEditar.UseStyleColors = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(869, 74);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(91, 37);
            this.btnNuevo.Style = MetroFramework.MetroColorStyle.Green;
            this.btnNuevo.TabIndex = 0;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnNuevo.UseSelectable = true;
            this.btnNuevo.UseStyleColors = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(869, 130);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(91, 37);
            this.btnGuardar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnGuardar.TabIndex = 6;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnGuardar.UseSelectable = true;
            this.btnGuardar.UseStyleColors = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnUnidad
            // 
            this.btnUnidad.Location = new System.Drawing.Point(503, 144);
            this.btnUnidad.Name = "btnUnidad";
            this.btnUnidad.Size = new System.Drawing.Size(27, 23);
            this.btnUnidad.Style = MetroFramework.MetroColorStyle.Green;
            this.btnUnidad.TabIndex = 5;
            this.btnUnidad.Text = "...";
            this.btnUnidad.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnUnidad.UseSelectable = true;
            this.btnUnidad.UseStyleColors = true;
            this.btnUnidad.Click += new System.EventHandler(this.btnUnidad_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(306, 144);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(58, 19);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel7.TabIndex = 77;
            this.metroLabel7.Text = "Unidad: ";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel7.UseStyleColors = true;
            // 
            // tbxUnidad
            // 
            // 
            // 
            // 
            this.tbxUnidad.CustomButton.Image = null;
            this.tbxUnidad.CustomButton.Location = new System.Drawing.Point(82, 1);
            this.tbxUnidad.CustomButton.Name = "";
            this.tbxUnidad.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxUnidad.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxUnidad.CustomButton.TabIndex = 1;
            this.tbxUnidad.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxUnidad.CustomButton.UseSelectable = true;
            this.tbxUnidad.CustomButton.Visible = false;
            this.tbxUnidad.Enabled = false;
            this.tbxUnidad.Lines = new string[0];
            this.tbxUnidad.Location = new System.Drawing.Point(384, 144);
            this.tbxUnidad.MaxLength = 250;
            this.tbxUnidad.Name = "tbxUnidad";
            this.tbxUnidad.PasswordChar = '\0';
            this.tbxUnidad.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxUnidad.SelectedText = "";
            this.tbxUnidad.SelectionLength = 0;
            this.tbxUnidad.SelectionStart = 0;
            this.tbxUnidad.Size = new System.Drawing.Size(104, 23);
            this.tbxUnidad.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxUnidad.TabIndex = 76;
            this.tbxUnidad.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxUnidad.UseSelectable = true;
            this.tbxUnidad.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxUnidad.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(112, 441);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(687, 264);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 78;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.BackColor = System.Drawing.Color.SeaGreen;
            this.metroLabel9.Location = new System.Drawing.Point(23, 343);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(941, 10);
            this.metroLabel9.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel9.TabIndex = 79;
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel9.UseCustomBackColor = true;
            // 
            // lblBuscar2
            // 
            this.lblBuscar2.AutoSize = true;
            this.lblBuscar2.Location = new System.Drawing.Point(22, 386);
            this.lblBuscar2.Name = "lblBuscar2";
            this.lblBuscar2.Size = new System.Drawing.Size(83, 19);
            this.lblBuscar2.Style = MetroFramework.MetroColorStyle.Green;
            this.lblBuscar2.TabIndex = 81;
            this.lblBuscar2.Text = "Descripción: ";
            this.lblBuscar2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblBuscar2.UseStyleColors = true;
            // 
            // tbxBuscarDescripcion
            // 
            // 
            // 
            // 
            this.tbxBuscarDescripcion.CustomButton.Image = null;
            this.tbxBuscarDescripcion.CustomButton.Location = new System.Drawing.Point(299, 1);
            this.tbxBuscarDescripcion.CustomButton.Name = "";
            this.tbxBuscarDescripcion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBuscarDescripcion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBuscarDescripcion.CustomButton.TabIndex = 1;
            this.tbxBuscarDescripcion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBuscarDescripcion.CustomButton.UseSelectable = true;
            this.tbxBuscarDescripcion.CustomButton.Visible = false;
            this.tbxBuscarDescripcion.Lines = new string[0];
            this.tbxBuscarDescripcion.Location = new System.Drawing.Point(111, 386);
            this.tbxBuscarDescripcion.MaxLength = 11;
            this.tbxBuscarDescripcion.Name = "tbxBuscarDescripcion";
            this.tbxBuscarDescripcion.PasswordChar = '\0';
            this.tbxBuscarDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBuscarDescripcion.SelectedText = "";
            this.tbxBuscarDescripcion.SelectionLength = 0;
            this.tbxBuscarDescripcion.SelectionStart = 0;
            this.tbxBuscarDescripcion.Size = new System.Drawing.Size(321, 23);
            this.tbxBuscarDescripcion.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBuscarDescripcion.TabIndex = 80;
            this.tbxBuscarDescripcion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBuscarDescripcion.UseSelectable = true;
            this.tbxBuscarDescripcion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBuscarDescripcion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(865, 386);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(91, 37);
            this.btnBuscar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscar.TabIndex = 83;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscar.UseSelectable = true;
            this.btnBuscar.UseStyleColors = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // FrmArticulo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 742);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.lblBuscar2);
            this.Controls.Add(this.tbxBuscarDescripcion);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.btnUnidad);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.tbxUnidad);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnMoneda);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.tbxMoneda);
            this.Controls.Add(this.tbxCantidad);
            this.Controls.Add(this.tbxPrecio);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnBuscarFamilia);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.tbxDescripcion);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.tbxFamilia);
            this.Name = "FrmArticulo";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Articulo";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmArticulo_FormClosed);
            this.Load += new System.EventHandler(this.FrmArticulo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton btnMoneda;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox tbxMoneda;
        private MetroFramework.Controls.MetroTextBox tbxCantidad;
        private MetroFramework.Controls.MetroTextBox tbxPrecio;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton btnBuscarFamilia;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox tbxDescripcion;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox tbxFamilia;
        private MetroFramework.Controls.MetroButton btnCancelar;
        private MetroFramework.Controls.MetroButton btnEliminar;
        private MetroFramework.Controls.MetroButton btnEditar;
        private MetroFramework.Controls.MetroButton btnNuevo;
        private MetroFramework.Controls.MetroButton btnGuardar;
        private MetroFramework.Controls.MetroButton btnUnidad;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox tbxUnidad;
        private MetroFramework.Controls.MetroGrid dgvLista;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel lblBuscar2;
        private MetroFramework.Controls.MetroTextBox tbxBuscarDescripcion;
        private MetroFramework.Controls.MetroButton btnBuscar;
    }
}