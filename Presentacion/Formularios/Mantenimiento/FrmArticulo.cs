﻿using BOL;
using Entidades;
using MetroFramework;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Mantenimiento
{
    public partial class FrmArticulo : MetroForm
    {
        public Articulo ent { get; set; }

        public string IdEmpresa { get; internal set; }
        public string IdSucursal { get; internal set; }
        public string IdUsuario { get; internal set; }

        private void OcultarCampos()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["_Empresa"].Visible = false;
            dgvLista.Columns["Sucursal"].Visible = false;
            dgvLista.Columns["Usuario"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        private void Habilitar()
        {
            tbxDescripcion.Enabled = true;
            tbxCantidad.Enabled = true;
            tbxPrecio.Enabled = true;
            btnCancelar.Enabled = true;
            btnGuardar.Enabled = true;
        }

        public FrmArticulo()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ent = new Articulo();
            ent.moneda = new Moneda();
            ent.Familia = new Familia();
            ent.Unidad = new Unidad();
            ent._Empresa = new Entidades.Empresa();
            ent.Sucursal = new Entidades.Sucursal();
            ent.Usuario = new Entidades.Usuario();
            ent.Unidad = new Unidad();
            btnNuevo.Enabled = false;
            Habilitar();
            ent.Id = "";
            tbxCantidad.Text = "0";
        }

        private void btnBuscarProveedor_Click(object sender, EventArgs e)
        {
            //Mantenimiento.FrmProveedor aux = new Mantenimiento.FrmProveedor();
            //aux.ShowDialog();
            //ent.Proveedor = aux.proveedor;
            //tbxProveedor.Text = ent.Proveedor.ToString();
        }

        private void btnBuscarFamilia_Click(object sender, EventArgs e)
        {
            Mantenimiento.FrmFamilia aux = new Mantenimiento.FrmFamilia();
            aux.ShowDialog();
            ent.Familia = aux.ent;
            tbxFamilia.Text = ent.Familia.ToString();
        }

        private void btnMoneda_Click(object sender, EventArgs e)
        {
            Mantenimiento.FrmMoneda aux = new Mantenimiento.FrmMoneda();
            aux.ShowDialog();
            ent.moneda = aux.ent;
            tbxMoneda.Text = ent.moneda.ToString();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ent.Stock = int.Parse(tbxCantidad.Text);
                ent.StockInicial = int.Parse(tbxCantidad.Text);
                ent.Descripcion = tbxDescripcion.Text;
                ent.Precio = Convert.ToDecimal(tbxPrecio.Text);
                ent._Empresa.Id = IdEmpresa;
                ent.Sucursal.Id = IdSucursal;
                ent.Usuario.Id = IdUsuario;

                if (string.IsNullOrWhiteSpace(ent.Descripcion) == true)
                {
                    MessageBox.Show("No puede estar vacío.", "");
                }
                else
                {
                    var r = ArticuloBOL.Registro(ent);
                    if (r)
                    {
                        Limpiar();
                        DesHabilitar();
                        Ultimos();
                        MetroMessageBox.Show(this, $"Modifcado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }
                    else
                    {
                        Ultimos();
                        Limpiar();
                        DesHabilitar();
                        MetroMessageBox.Show(this, $"Guardado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }
                }
            }
            catch (Exception)
            {
                MetroMessageBox.Show(this, $"Error al guardar.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            }
        }

        private void Limpiar()
        {
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
            //tbxDescripcion.Clear();
            tbxDescripcion.Enabled = false;
            //tbxCantidad.Clear();
            tbxCantidad.Enabled = false;
            //tbxFamilia.Clear();
            tbxFamilia.Enabled = false;
            //tbxMoneda.Clear();
            tbxMoneda.Enabled = false;
            //tbxPrecio.Clear();
            tbxPrecio.Enabled = false;
        }

        private void DesHabilitar()
        {
            btnNuevo.Enabled = true;
            tbxDescripcion.Enabled = false;
            tbxPrecio.Enabled = false;
            tbxMoneda.Enabled = false;
            tbxFamilia.Enabled = false;
            tbxCantidad.Enabled = false;
            btnCancelar.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void Ultimos()
        {
            var lista = ArticuloBOL.BuscarUltimos(IdEmpresa, IdSucursal);
            dgvLista.DataSource = lista.ToList();
            OcultarCampos();
        }

        private void btnUnidad_Click(object sender, EventArgs e)
        {
            FrmUnidad aux = new Mantenimiento.FrmUnidad();
            aux.ShowDialog();
            ent.Unidad = aux.obj;
            tbxUnidad.Text = ent.Unidad.ToString();
        }

        private void FrmArticulo_Load(object sender, EventArgs e)
        {
            Ultimos();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLista.SelectedRows.Count > -1)
                {
                    ent = (Articulo)dgvLista.CurrentRow.DataBoundItem;
                    Habilitar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al seleccionar");
            }
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                Habilitar();
                btnGuardar.Enabled = true;
                btnNuevo.Enabled = false;
                if (dgvLista.SelectedRows.Count == 1)
                {
                    tbxDescripcion.Text = ent.Descripcion;
                    tbxCantidad.Text = ent.Stock.ToString();
                    tbxFamilia.Text = ent.Familia.ToString();
                    tbxMoneda.Text = ent.moneda.ToString();
                    tbxPrecio.Text = ent.Precio.ToString();
                    tbxUnidad.Text = ent.Unidad.ToString();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (r.Equals(DialogResult.Yes))
                {
                    ArticuloBOL.Eliminar(ent.Id);
                    Ultimos();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No se pudo Eliminar");
            }
        }

        private void dgvLista_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {

            }
        }

        private void FrmArticulo_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void dgvLista_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                dgvLista.ClearSelection();
                DesHabilitar();
            }
            catch (Exception)
            {
                MessageBox.Show("Se ha generado un error al listar");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Articulo();
            aux._Empresa = new Entidades.Empresa();
            aux.Sucursal = new Sucursal();
            aux.Usuario = new Usuario();
            aux._Empresa.Id = IdEmpresa;
            aux.Sucursal.Id = IdSucursal;
            aux.Usuario.Id = IdUsuario;
            aux.Descripcion = tbxBuscarDescripcion.Text;
            dgvLista.DataSource = ArticuloBOL.Buscar(aux);
        }
    }
}