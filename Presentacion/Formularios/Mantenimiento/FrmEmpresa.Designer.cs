﻿namespace Presentacion.Formularios.Empresa
{
    partial class FrmEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.mtbxNombre = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.mtbxTelefono = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.mtbxFax = new MetroFramework.Controls.MetroTextBox();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            this.btnGuardar = new MetroFramework.Controls.MetroButton();
            this.btnEditar = new MetroFramework.Controls.MetroButton();
            this.btnEliminar = new MetroFramework.Controls.MetroButton();
            this.btnNuevo = new MetroFramework.Controls.MetroButton();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.mtbxDireccion = new MetroFramework.Controls.MetroTextBox();
            this.btnCancelar = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.mtbxRuc = new MetroFramework.Controls.MetroTextBox();
            this.mtbxRazon = new MetroFramework.Controls.MetroTextBox();
            this.tbxBuscarRazon = new MetroFramework.Controls.MetroTextBox();
            this.tbxBuscarRUC = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.btnBuscar = new MetroFramework.Controls.MetroButton();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(47, 158);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(129, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 0;
            this.metroLabel3.Text = "Nombre abreviado: ";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            // 
            // mtbxNombre
            // 
            // 
            // 
            // 
            this.mtbxNombre.CustomButton.Image = null;
            this.mtbxNombre.CustomButton.Location = new System.Drawing.Point(322, 1);
            this.mtbxNombre.CustomButton.Name = "";
            this.mtbxNombre.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxNombre.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxNombre.CustomButton.TabIndex = 1;
            this.mtbxNombre.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxNombre.CustomButton.UseSelectable = true;
            this.mtbxNombre.CustomButton.Visible = false;
            this.mtbxNombre.Enabled = false;
            this.mtbxNombre.Lines = new string[0];
            this.mtbxNombre.Location = new System.Drawing.Point(193, 158);
            this.mtbxNombre.MaxLength = 32767;
            this.mtbxNombre.Name = "mtbxNombre";
            this.mtbxNombre.PasswordChar = '\0';
            this.mtbxNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxNombre.SelectedText = "";
            this.mtbxNombre.SelectionLength = 0;
            this.mtbxNombre.SelectionStart = 0;
            this.mtbxNombre.Size = new System.Drawing.Size(344, 23);
            this.mtbxNombre.TabIndex = 3;
            this.mtbxNombre.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxNombre.UseSelectable = true;
            this.mtbxNombre.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxNombre.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(47, 233);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(65, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Teléfono: ";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseStyleColors = true;
            // 
            // mtbxTelefono
            // 
            // 
            // 
            // 
            this.mtbxTelefono.CustomButton.Image = null;
            this.mtbxTelefono.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.mtbxTelefono.CustomButton.Name = "";
            this.mtbxTelefono.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxTelefono.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxTelefono.CustomButton.TabIndex = 1;
            this.mtbxTelefono.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxTelefono.CustomButton.UseSelectable = true;
            this.mtbxTelefono.CustomButton.Visible = false;
            this.mtbxTelefono.Enabled = false;
            this.mtbxTelefono.Lines = new string[0];
            this.mtbxTelefono.Location = new System.Drawing.Point(193, 233);
            this.mtbxTelefono.MaxLength = 32767;
            this.mtbxTelefono.Name = "mtbxTelefono";
            this.mtbxTelefono.PasswordChar = '\0';
            this.mtbxTelefono.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxTelefono.SelectedText = "";
            this.mtbxTelefono.SelectionLength = 0;
            this.mtbxTelefono.SelectionStart = 0;
            this.mtbxTelefono.Size = new System.Drawing.Size(136, 23);
            this.mtbxTelefono.TabIndex = 5;
            this.mtbxTelefono.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxTelefono.UseSelectable = true;
            this.mtbxTelefono.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxTelefono.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(364, 233);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(31, 19);
            this.metroLabel6.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel6.TabIndex = 0;
            this.metroLabel6.Text = "Fax:";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel6.UseStyleColors = true;
            // 
            // mtbxFax
            // 
            // 
            // 
            // 
            this.mtbxFax.CustomButton.Image = null;
            this.mtbxFax.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.mtbxFax.CustomButton.Name = "";
            this.mtbxFax.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxFax.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxFax.CustomButton.TabIndex = 1;
            this.mtbxFax.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxFax.CustomButton.UseSelectable = true;
            this.mtbxFax.CustomButton.Visible = false;
            this.mtbxFax.Enabled = false;
            this.mtbxFax.Lines = new string[0];
            this.mtbxFax.Location = new System.Drawing.Point(401, 233);
            this.mtbxFax.MaxLength = 32767;
            this.mtbxFax.Name = "mtbxFax";
            this.mtbxFax.PasswordChar = '\0';
            this.mtbxFax.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxFax.SelectedText = "";
            this.mtbxFax.SelectionLength = 0;
            this.mtbxFax.SelectionStart = 0;
            this.mtbxFax.Size = new System.Drawing.Size(136, 23);
            this.mtbxFax.TabIndex = 6;
            this.mtbxFax.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxFax.UseSelectable = true;
            this.mtbxFax.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxFax.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(47, 438);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(868, 157);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 3;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            this.dgvLista.SelectionChanged += new System.EventHandler(this.dgvLista_SelectionChanged);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Location = new System.Drawing.Point(824, 113);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(91, 37);
            this.btnGuardar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnGuardar.UseSelectable = true;
            this.btnGuardar.UseStyleColors = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Enabled = false;
            this.btnEditar.Location = new System.Drawing.Point(824, 170);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(91, 37);
            this.btnEditar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEditar.TabIndex = 10;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEditar.UseSelectable = true;
            this.btnEditar.UseStyleColors = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Enabled = false;
            this.btnEliminar.Location = new System.Drawing.Point(824, 226);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(91, 37);
            this.btnEliminar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEliminar.TabIndex = 11;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEliminar.UseSelectable = true;
            this.btnEliminar.UseStyleColors = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(824, 63);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(91, 37);
            this.btnNuevo.Style = MetroFramework.MetroColorStyle.Green;
            this.btnNuevo.TabIndex = 8;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnNuevo.UseSelectable = true;
            this.btnNuevo.UseStyleColors = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(47, 196);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(70, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 0;
            this.metroLabel4.Text = "Direccion: ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // mtbxDireccion
            // 
            // 
            // 
            // 
            this.mtbxDireccion.CustomButton.Image = null;
            this.mtbxDireccion.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.mtbxDireccion.CustomButton.Name = "";
            this.mtbxDireccion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxDireccion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxDireccion.CustomButton.TabIndex = 1;
            this.mtbxDireccion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxDireccion.CustomButton.UseSelectable = true;
            this.mtbxDireccion.CustomButton.Visible = false;
            this.mtbxDireccion.Enabled = false;
            this.mtbxDireccion.Lines = new string[0];
            this.mtbxDireccion.Location = new System.Drawing.Point(193, 196);
            this.mtbxDireccion.MaxLength = 32767;
            this.mtbxDireccion.Name = "mtbxDireccion";
            this.mtbxDireccion.PasswordChar = '\0';
            this.mtbxDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxDireccion.SelectedText = "";
            this.mtbxDireccion.SelectionLength = 0;
            this.mtbxDireccion.SelectionStart = 0;
            this.mtbxDireccion.Size = new System.Drawing.Size(555, 23);
            this.mtbxDireccion.TabIndex = 4;
            this.mtbxDireccion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxDireccion.UseSelectable = true;
            this.mtbxDireccion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxDireccion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Enabled = false;
            this.btnCancelar.Location = new System.Drawing.Point(824, 281);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 37);
            this.btnCancelar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancelar.UseSelectable = true;
            this.btnCancelar.UseStyleColors = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(47, 83);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(42, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "RUC: ";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(47, 119);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(92, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Razon social:  ";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // mtbxRuc
            // 
            // 
            // 
            // 
            this.mtbxRuc.CustomButton.Image = null;
            this.mtbxRuc.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.mtbxRuc.CustomButton.Name = "";
            this.mtbxRuc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxRuc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxRuc.CustomButton.TabIndex = 1;
            this.mtbxRuc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxRuc.CustomButton.UseSelectable = true;
            this.mtbxRuc.CustomButton.Visible = false;
            this.mtbxRuc.Enabled = false;
            this.mtbxRuc.Lines = new string[0];
            this.mtbxRuc.Location = new System.Drawing.Point(193, 83);
            this.mtbxRuc.MaxLength = 11;
            this.mtbxRuc.Name = "mtbxRuc";
            this.mtbxRuc.PasswordChar = '\0';
            this.mtbxRuc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxRuc.SelectedText = "";
            this.mtbxRuc.SelectionLength = 0;
            this.mtbxRuc.SelectionStart = 0;
            this.mtbxRuc.Size = new System.Drawing.Size(136, 23);
            this.mtbxRuc.Style = MetroFramework.MetroColorStyle.Green;
            this.mtbxRuc.TabIndex = 1;
            this.mtbxRuc.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxRuc.UseSelectable = true;
            this.mtbxRuc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxRuc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbxRazon
            // 
            // 
            // 
            // 
            this.mtbxRazon.CustomButton.Image = null;
            this.mtbxRazon.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.mtbxRazon.CustomButton.Name = "";
            this.mtbxRazon.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxRazon.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxRazon.CustomButton.TabIndex = 1;
            this.mtbxRazon.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxRazon.CustomButton.UseSelectable = true;
            this.mtbxRazon.CustomButton.Visible = false;
            this.mtbxRazon.Enabled = false;
            this.mtbxRazon.Lines = new string[0];
            this.mtbxRazon.Location = new System.Drawing.Point(193, 119);
            this.mtbxRazon.MaxLength = 32767;
            this.mtbxRazon.Name = "mtbxRazon";
            this.mtbxRazon.PasswordChar = '\0';
            this.mtbxRazon.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxRazon.SelectedText = "";
            this.mtbxRazon.SelectionLength = 0;
            this.mtbxRazon.SelectionStart = 0;
            this.mtbxRazon.Size = new System.Drawing.Size(555, 23);
            this.mtbxRazon.TabIndex = 2;
            this.mtbxRazon.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxRazon.UseSelectable = true;
            this.mtbxRazon.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxRazon.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbxBuscarRazon
            // 
            // 
            // 
            // 
            this.tbxBuscarRazon.CustomButton.Image = null;
            this.tbxBuscarRazon.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.tbxBuscarRazon.CustomButton.Name = "";
            this.tbxBuscarRazon.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBuscarRazon.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBuscarRazon.CustomButton.TabIndex = 1;
            this.tbxBuscarRazon.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBuscarRazon.CustomButton.UseSelectable = true;
            this.tbxBuscarRazon.CustomButton.Visible = false;
            this.tbxBuscarRazon.Lines = new string[0];
            this.tbxBuscarRazon.Location = new System.Drawing.Point(193, 395);
            this.tbxBuscarRazon.MaxLength = 32767;
            this.tbxBuscarRazon.Name = "tbxBuscarRazon";
            this.tbxBuscarRazon.PasswordChar = '\0';
            this.tbxBuscarRazon.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBuscarRazon.SelectedText = "";
            this.tbxBuscarRazon.SelectionLength = 0;
            this.tbxBuscarRazon.SelectionStart = 0;
            this.tbxBuscarRazon.Size = new System.Drawing.Size(555, 23);
            this.tbxBuscarRazon.TabIndex = 16;
            this.tbxBuscarRazon.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBuscarRazon.UseSelectable = true;
            this.tbxBuscarRazon.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBuscarRazon.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbxBuscarRUC
            // 
            // 
            // 
            // 
            this.tbxBuscarRUC.CustomButton.Image = null;
            this.tbxBuscarRUC.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.tbxBuscarRUC.CustomButton.Name = "";
            this.tbxBuscarRUC.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBuscarRUC.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBuscarRUC.CustomButton.TabIndex = 1;
            this.tbxBuscarRUC.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBuscarRUC.CustomButton.UseSelectable = true;
            this.tbxBuscarRUC.CustomButton.Visible = false;
            this.tbxBuscarRUC.Lines = new string[0];
            this.tbxBuscarRUC.Location = new System.Drawing.Point(193, 359);
            this.tbxBuscarRUC.MaxLength = 11;
            this.tbxBuscarRUC.Name = "tbxBuscarRUC";
            this.tbxBuscarRUC.PasswordChar = '\0';
            this.tbxBuscarRUC.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBuscarRUC.SelectedText = "";
            this.tbxBuscarRUC.SelectionLength = 0;
            this.tbxBuscarRUC.SelectionStart = 0;
            this.tbxBuscarRUC.Size = new System.Drawing.Size(136, 23);
            this.tbxBuscarRUC.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBuscarRUC.TabIndex = 15;
            this.tbxBuscarRUC.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBuscarRUC.UseSelectable = true;
            this.tbxBuscarRUC.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBuscarRUC.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(47, 395);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(92, 19);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel7.TabIndex = 13;
            this.metroLabel7.Text = "Razon social:  ";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel7.UseStyleColors = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(47, 359);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(42, 19);
            this.metroLabel8.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "RUC: ";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel8.UseStyleColors = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(824, 377);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(91, 37);
            this.btnBuscar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscar.TabIndex = 17;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscar.UseSelectable = true;
            this.btnBuscar.UseStyleColors = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.BackColor = System.Drawing.Color.SeaGreen;
            this.metroLabel9.Location = new System.Drawing.Point(6, 333);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(941, 10);
            this.metroLabel9.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel9.TabIndex = 18;
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel9.UseCustomBackColor = true;
            // 
            // FrmEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(950, 627);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.tbxBuscarRazon);
            this.Controls.Add(this.tbxBuscarRUC);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.mtbxFax);
            this.Controls.Add(this.mtbxTelefono);
            this.Controls.Add(this.mtbxDireccion);
            this.Controls.Add(this.mtbxNombre);
            this.Controls.Add(this.mtbxRazon);
            this.Controls.Add(this.mtbxRuc);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FrmEmpresa";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Empresa";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Empresa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox mtbxNombre;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox mtbxTelefono;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox mtbxFax;
        private MetroFramework.Controls.MetroGrid dgvLista;
        private MetroFramework.Controls.MetroButton btnGuardar;
        private MetroFramework.Controls.MetroButton btnEditar;
        private MetroFramework.Controls.MetroButton btnEliminar;
        private MetroFramework.Controls.MetroButton btnNuevo;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox mtbxDireccion;
        private MetroFramework.Controls.MetroButton btnCancelar;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox mtbxRuc;
        private MetroFramework.Controls.MetroTextBox mtbxRazon;
        private MetroFramework.Controls.MetroTextBox tbxBuscarRazon;
        private MetroFramework.Controls.MetroTextBox tbxBuscarRUC;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroButton btnBuscar;
        private MetroFramework.Controls.MetroLabel metroLabel9;
    }
}