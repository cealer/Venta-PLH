﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using BOL;
using MetroFramework;
using static BOL.EmpresaBOL;

namespace Presentacion.Formularios.Empresa
{
    public partial class FrmEmpresa : MetroForm
    {
        public Entidades.Empresa obj = new Entidades.Empresa();

        private void Limpiar()
        {
            obj = null;
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
        }

        private void Habilitar()
        {
            btnGuardar.Enabled = true;
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void HabilitaEdicion()
        {
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void DesHabilitar()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void Listar()
        {
            var lista = BuscarUltimos().Where(x => x.Estado.Equals("1")).ToList();
            dgvLista.DataSource = lista;
            OcultarCampos();
        }

        private void HabilitarControles()
        {
            mtbxDireccion.Enabled = true;
            mtbxFax.Enabled = true;
            mtbxNombre.Enabled = true;
            mtbxRazon.Enabled = true;
            mtbxRuc.Enabled = true;
            mtbxTelefono.Enabled = true;
        }

        //Ocultar columnas datagridview
        private void OcultarCampos()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        public FrmEmpresa()
        {
            InitializeComponent();
        }

        private void Empresa_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Empresa
            {
                Id = obj.Id,
                Ruc = mtbxRuc.Text,
                RazonSocial = mtbxRazon.Text,
                NombreAbreviado = mtbxNombre.Text,
                Direccion = mtbxDireccion.Text,
                Telefono = mtbxTelefono.Text,
                Fax = mtbxFax.Text
            };

            var r = Registro(aux);
            if (r == false)
            {
                MetroMessageBox.Show(this, $"Empresa registrada correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            }
            else
            {
                MetroMessageBox.Show(this, $"Empresa editada correctamente.", "Edición", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            }

            Limpiar();
            DesHabilitar();
            btnNuevo.Enabled = true;
            Listar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            obj.Id = "";
            HabilitarControles();
            Habilitar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r.Equals(DialogResult.Yes))
            {
                Eliminar(obj.Id);
                Listar();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
            DesHabilitar();
            btnNuevo.Enabled = true;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            HabilitarControles();
            btnGuardar.Enabled = true;
            btnNuevo.Enabled = false;
            if (dgvLista.SelectedRows.Count == 1)
            {
                mtbxRuc.Text = obj.Ruc;
                mtbxRazon.Text = obj.RazonSocial;
                mtbxNombre.Text = obj.NombreAbreviado;
                mtbxDireccion.Text = obj.Direccion;
                mtbxTelefono.Text = obj.Telefono;
                mtbxFax.Text = obj.Fax;
            }
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvLista.CurrentRow.Index > -1)
            {
                HabilitaEdicion();
                obj = (Entidades.Empresa)dgvLista.CurrentRow.DataBoundItem;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Empresa
            {
                RazonSocial = tbxBuscarRazon.Text,
                Ruc = tbxBuscarRUC.Text
            };
            dgvLista.DataSource = EmpresaBOL.Buscar(aux).Where(x => x.Estado.Equals("1")).ToList();
        }
    }
}