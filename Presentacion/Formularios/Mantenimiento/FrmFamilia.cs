﻿using BOL;
using Entidades;
using MetroFramework;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Mantenimiento
{
    public partial class FrmFamilia : MetroForm
    {
        public Familia ent { get; set; }

        public FrmFamilia()
        {
            InitializeComponent();
        }

        //Ocultar columnas datagridview
        private void OcultarCampos()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        private void Limpiar()
        {
            tbxDescripcion.Clear();
            tbxDescripcion.Enabled = false;
        }

        private void Habilitar()
        {
            tbxDescripcion.Enabled = true;
            btnCancelar.Enabled = true;
            btnGuardar.Enabled = true;
        }

        private void DesHabilitar()
        {
            btnNuevo.Enabled = true;
            tbxDescripcion.Enabled = false;
            btnCancelar.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void Ultimos()
        {
            var lista = FamiliaBOL.BuscarUltimos();
            dgvLista.DataSource = lista.ToList();
            OcultarCampos();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ent.Descripcion = tbxDescripcion.Text;
                if (string.IsNullOrWhiteSpace(ent.Descripcion) == true)
                {
                    MessageBox.Show("No puede estar vacío.", "");
                }
                else
                {
                    var r = FamiliaBOL.Registro(ent);
                    if (r)
                    {
                        Limpiar();
                        DesHabilitar();
                        Ultimos();
                        MetroMessageBox.Show(this, $"Modifcado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }
                    else
                    {
                        Ultimos();
                        Limpiar();
                        DesHabilitar();
                        MetroMessageBox.Show(this, $"Guardado correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                    }
                }
            }
            catch (Exception)
            {
                MetroMessageBox.Show(this, $"Error al guardar.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ent = new Familia();
            btnNuevo.Enabled = false;
            Habilitar();
            ent.Id = "";
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvLista.SelectedRows.Count == 1)
                {
                    tbxDescripcion.Text = ent.Descripcion;
                    Habilitar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ent = new Familia();
            ent.Descripcion = tbxBuscar.Text;
            var lista = FamiliaBOL.Buscar(ent);
            dgvLista.DataSource = lista.ToList();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (r.Equals(DialogResult.Yes))
                {
                    FamiliaBOL.Eliminar(ent.Id);
                    Ultimos();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No se pudo Eliminar");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
            DesHabilitar();
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLista.SelectedRows.Count == 1)
                {
                    ent = (Familia)dgvLista.CurrentRow.DataBoundItem;
                    Habilitar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al seleccionar");
            }
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmFamilia_Load(object sender, EventArgs e)
        {
            Ultimos();
        }
    }
}