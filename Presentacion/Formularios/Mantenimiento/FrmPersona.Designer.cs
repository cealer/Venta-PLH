﻿namespace Presentacion.Formularios.Mantenimiento
{
    partial class FrmPersona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtbxDireccion = new MetroFramework.Controls.MetroTextBox();
            this.mtbxNombre = new MetroFramework.Controls.MetroTextBox();
            this.mtbxNombres = new MetroFramework.Controls.MetroTextBox();
            this.mtbxRuc = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dtpFecNac = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.cboSexo = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // mtbxDireccion
            // 
            // 
            // 
            // 
            this.mtbxDireccion.CustomButton.Image = null;
            this.mtbxDireccion.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.mtbxDireccion.CustomButton.Name = "";
            this.mtbxDireccion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxDireccion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxDireccion.CustomButton.TabIndex = 1;
            this.mtbxDireccion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxDireccion.CustomButton.UseSelectable = true;
            this.mtbxDireccion.CustomButton.Visible = false;
            this.mtbxDireccion.Enabled = false;
            this.mtbxDireccion.Lines = new string[0];
            this.mtbxDireccion.Location = new System.Drawing.Point(169, 193);
            this.mtbxDireccion.MaxLength = 32767;
            this.mtbxDireccion.Name = "mtbxDireccion";
            this.mtbxDireccion.PasswordChar = '\0';
            this.mtbxDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxDireccion.SelectedText = "";
            this.mtbxDireccion.SelectionLength = 0;
            this.mtbxDireccion.SelectionStart = 0;
            this.mtbxDireccion.Size = new System.Drawing.Size(555, 23);
            this.mtbxDireccion.TabIndex = 12;
            this.mtbxDireccion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxDireccion.UseSelectable = true;
            this.mtbxDireccion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxDireccion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbxNombre
            // 
            // 
            // 
            // 
            this.mtbxNombre.CustomButton.Image = null;
            this.mtbxNombre.CustomButton.Location = new System.Drawing.Point(322, 1);
            this.mtbxNombre.CustomButton.Name = "";
            this.mtbxNombre.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxNombre.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxNombre.CustomButton.TabIndex = 1;
            this.mtbxNombre.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxNombre.CustomButton.UseSelectable = true;
            this.mtbxNombre.CustomButton.Visible = false;
            this.mtbxNombre.Enabled = false;
            this.mtbxNombre.Lines = new string[0];
            this.mtbxNombre.Location = new System.Drawing.Point(169, 155);
            this.mtbxNombre.MaxLength = 32767;
            this.mtbxNombre.Name = "mtbxNombre";
            this.mtbxNombre.PasswordChar = '\0';
            this.mtbxNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxNombre.SelectedText = "";
            this.mtbxNombre.SelectionLength = 0;
            this.mtbxNombre.SelectionStart = 0;
            this.mtbxNombre.Size = new System.Drawing.Size(344, 23);
            this.mtbxNombre.TabIndex = 11;
            this.mtbxNombre.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxNombre.UseSelectable = true;
            this.mtbxNombre.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxNombre.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbxNombres
            // 
            // 
            // 
            // 
            this.mtbxNombres.CustomButton.Image = null;
            this.mtbxNombres.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.mtbxNombres.CustomButton.Name = "";
            this.mtbxNombres.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxNombres.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxNombres.CustomButton.TabIndex = 1;
            this.mtbxNombres.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxNombres.CustomButton.UseSelectable = true;
            this.mtbxNombres.CustomButton.Visible = false;
            this.mtbxNombres.Enabled = false;
            this.mtbxNombres.Lines = new string[0];
            this.mtbxNombres.Location = new System.Drawing.Point(169, 116);
            this.mtbxNombres.MaxLength = 32767;
            this.mtbxNombres.Name = "mtbxNombres";
            this.mtbxNombres.PasswordChar = '\0';
            this.mtbxNombres.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxNombres.SelectedText = "";
            this.mtbxNombres.SelectionLength = 0;
            this.mtbxNombres.SelectionStart = 0;
            this.mtbxNombres.Size = new System.Drawing.Size(555, 23);
            this.mtbxNombres.TabIndex = 10;
            this.mtbxNombres.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxNombres.UseSelectable = true;
            this.mtbxNombres.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxNombres.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mtbxRuc
            // 
            // 
            // 
            // 
            this.mtbxRuc.CustomButton.Image = null;
            this.mtbxRuc.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.mtbxRuc.CustomButton.Name = "";
            this.mtbxRuc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxRuc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxRuc.CustomButton.TabIndex = 1;
            this.mtbxRuc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxRuc.CustomButton.UseSelectable = true;
            this.mtbxRuc.CustomButton.Visible = false;
            this.mtbxRuc.Enabled = false;
            this.mtbxRuc.Lines = new string[0];
            this.mtbxRuc.Location = new System.Drawing.Point(169, 80);
            this.mtbxRuc.MaxLength = 11;
            this.mtbxRuc.Name = "mtbxRuc";
            this.mtbxRuc.PasswordChar = '\0';
            this.mtbxRuc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxRuc.SelectedText = "";
            this.mtbxRuc.SelectionLength = 0;
            this.mtbxRuc.SelectionStart = 0;
            this.mtbxRuc.Size = new System.Drawing.Size(136, 23);
            this.mtbxRuc.Style = MetroFramework.MetroColorStyle.Green;
            this.mtbxRuc.TabIndex = 9;
            this.mtbxRuc.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxRuc.UseSelectable = true;
            this.mtbxRuc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxRuc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 155);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(107, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "Apellido Paterno";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 193);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(112, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Apellido Materno";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 116);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(71, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 7;
            this.metroLabel2.Text = "Nombres: ";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 80);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(38, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 8;
            this.metroLabel1.Text = "DNI: ";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // dtpFecNac
            // 
            this.dtpFecNac.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecNac.Location = new System.Drawing.Point(169, 233);
            this.dtpFecNac.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpFecNac.Name = "dtpFecNac";
            this.dtpFecNac.RightToLeftLayout = true;
            this.dtpFecNac.Size = new System.Drawing.Size(136, 29);
            this.dtpFecNac.TabIndex = 13;
            this.dtpFecNac.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(23, 233);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(130, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel5.TabIndex = 14;
            this.metroLabel5.Text = "Fecha de nacimiento";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseStyleColors = true;
            // 
            // cboSexo
            // 
            this.cboSexo.FormattingEnabled = true;
            this.cboSexo.ItemHeight = 23;
            this.cboSexo.Location = new System.Drawing.Point(169, 281);
            this.cboSexo.Name = "cboSexo";
            this.cboSexo.Size = new System.Drawing.Size(136, 29);
            this.cboSexo.TabIndex = 15;
            this.cboSexo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cboSexo.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(23, 281);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(44, 19);
            this.metroLabel6.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel6.TabIndex = 16;
            this.metroLabel6.Text = "Sexo: ";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel6.UseStyleColors = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(23, 336);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(35, 19);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel7.TabIndex = 17;
            this.metroLabel7.Text = "Rol: ";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel7.UseStyleColors = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(169, 336);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(45, 19);
            this.metroLabel8.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel8.TabIndex = 18;
            this.metroLabel8.Text = "------";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel8.UseStyleColors = true;
            // 
            // FrmPersona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 548);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.cboSexo);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.dtpFecNac);
            this.Controls.Add(this.mtbxDireccion);
            this.Controls.Add(this.mtbxNombre);
            this.Controls.Add(this.mtbxNombres);
            this.Controls.Add(this.mtbxRuc);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FrmPersona";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Registro";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox mtbxDireccion;
        private MetroFramework.Controls.MetroTextBox mtbxNombre;
        private MetroFramework.Controls.MetroTextBox mtbxNombres;
        private MetroFramework.Controls.MetroTextBox mtbxRuc;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroDateTime dtpFecNac;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox cboSexo;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
    }
}