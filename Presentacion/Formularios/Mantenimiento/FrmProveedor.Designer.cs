﻿namespace Presentacion.Formularios.Mantenimiento
{
    partial class FrmProveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tbxRazon = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.tbxRuc = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.btnBuscar = new MetroFramework.Controls.MetroButton();
            this.tbxBuscarRazon = new MetroFramework.Controls.MetroTextBox();
            this.tbxBuscarRUC = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.btnCancelar = new MetroFramework.Controls.MetroButton();
            this.btnEliminar = new MetroFramework.Controls.MetroButton();
            this.btnEditar = new MetroFramework.Controls.MetroButton();
            this.btnNuevo = new MetroFramework.Controls.MetroButton();
            this.btnGuardar = new MetroFramework.Controls.MetroButton();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            this.btnConsultarSunat = new MetroFramework.Controls.MetroButton();
            this.tbxDireccion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tbxTelefono = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbxFax = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxRazon
            // 
            // 
            // 
            // 
            this.tbxRazon.CustomButton.Image = null;
            this.tbxRazon.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.tbxRazon.CustomButton.Name = "";
            this.tbxRazon.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxRazon.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxRazon.CustomButton.TabIndex = 1;
            this.tbxRazon.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxRazon.CustomButton.UseSelectable = true;
            this.tbxRazon.CustomButton.Visible = false;
            this.tbxRazon.Enabled = false;
            this.tbxRazon.Lines = new string[0];
            this.tbxRazon.Location = new System.Drawing.Point(169, 123);
            this.tbxRazon.MaxLength = 32767;
            this.tbxRazon.Name = "tbxRazon";
            this.tbxRazon.PasswordChar = '\0';
            this.tbxRazon.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxRazon.SelectedText = "";
            this.tbxRazon.SelectionLength = 0;
            this.tbxRazon.SelectionStart = 0;
            this.tbxRazon.Size = new System.Drawing.Size(555, 23);
            this.tbxRazon.TabIndex = 1;
            this.tbxRazon.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxRazon.UseSelectable = true;
            this.tbxRazon.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxRazon.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 85);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(42, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 13;
            this.metroLabel4.Text = "RUC: ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 123);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(90, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 14;
            this.metroLabel3.Text = "Razon Social: ";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            // 
            // tbxRuc
            // 
            // 
            // 
            // 
            this.tbxRuc.CustomButton.Image = null;
            this.tbxRuc.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.tbxRuc.CustomButton.Name = "";
            this.tbxRuc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxRuc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxRuc.CustomButton.TabIndex = 1;
            this.tbxRuc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxRuc.CustomButton.UseSelectable = true;
            this.tbxRuc.CustomButton.Visible = false;
            this.tbxRuc.Enabled = false;
            this.tbxRuc.Lines = new string[0];
            this.tbxRuc.Location = new System.Drawing.Point(169, 81);
            this.tbxRuc.MaxLength = 11;
            this.tbxRuc.Name = "tbxRuc";
            this.tbxRuc.PasswordChar = '\0';
            this.tbxRuc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxRuc.SelectedText = "";
            this.tbxRuc.SelectionLength = 0;
            this.tbxRuc.SelectionStart = 0;
            this.tbxRuc.Size = new System.Drawing.Size(136, 23);
            this.tbxRuc.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxRuc.TabIndex = 0;
            this.tbxRuc.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxRuc.UseSelectable = true;
            this.tbxRuc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxRuc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel9
            // 
            this.metroLabel9.BackColor = System.Drawing.Color.SeaGreen;
            this.metroLabel9.Location = new System.Drawing.Point(-5, 301);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(941, 10);
            this.metroLabel9.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel9.TabIndex = 30;
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel9.UseCustomBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(813, 345);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(91, 37);
            this.btnBuscar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscar.TabIndex = 29;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscar.UseSelectable = true;
            this.btnBuscar.UseStyleColors = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // tbxBuscarRazon
            // 
            // 
            // 
            // 
            this.tbxBuscarRazon.CustomButton.Image = null;
            this.tbxBuscarRazon.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.tbxBuscarRazon.CustomButton.Name = "";
            this.tbxBuscarRazon.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBuscarRazon.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBuscarRazon.CustomButton.TabIndex = 1;
            this.tbxBuscarRazon.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBuscarRazon.CustomButton.UseSelectable = true;
            this.tbxBuscarRazon.CustomButton.Visible = false;
            this.tbxBuscarRazon.Lines = new string[0];
            this.tbxBuscarRazon.Location = new System.Drawing.Point(182, 363);
            this.tbxBuscarRazon.MaxLength = 32767;
            this.tbxBuscarRazon.Name = "tbxBuscarRazon";
            this.tbxBuscarRazon.PasswordChar = '\0';
            this.tbxBuscarRazon.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBuscarRazon.SelectedText = "";
            this.tbxBuscarRazon.SelectionLength = 0;
            this.tbxBuscarRazon.SelectionStart = 0;
            this.tbxBuscarRazon.Size = new System.Drawing.Size(555, 23);
            this.tbxBuscarRazon.TabIndex = 28;
            this.tbxBuscarRazon.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBuscarRazon.UseSelectable = true;
            this.tbxBuscarRazon.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBuscarRazon.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbxBuscarRUC
            // 
            // 
            // 
            // 
            this.tbxBuscarRUC.CustomButton.Image = null;
            this.tbxBuscarRUC.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.tbxBuscarRUC.CustomButton.Name = "";
            this.tbxBuscarRUC.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBuscarRUC.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBuscarRUC.CustomButton.TabIndex = 1;
            this.tbxBuscarRUC.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBuscarRUC.CustomButton.UseSelectable = true;
            this.tbxBuscarRUC.CustomButton.Visible = false;
            this.tbxBuscarRUC.Lines = new string[0];
            this.tbxBuscarRUC.Location = new System.Drawing.Point(182, 327);
            this.tbxBuscarRUC.MaxLength = 11;
            this.tbxBuscarRUC.Name = "tbxBuscarRUC";
            this.tbxBuscarRUC.PasswordChar = '\0';
            this.tbxBuscarRUC.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBuscarRUC.SelectedText = "";
            this.tbxBuscarRUC.SelectionLength = 0;
            this.tbxBuscarRUC.SelectionStart = 0;
            this.tbxBuscarRUC.Size = new System.Drawing.Size(136, 23);
            this.tbxBuscarRUC.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxBuscarRUC.TabIndex = 27;
            this.tbxBuscarRUC.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBuscarRUC.UseSelectable = true;
            this.tbxBuscarRUC.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBuscarRUC.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(36, 363);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(92, 19);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel7.TabIndex = 25;
            this.metroLabel7.Text = "Razon social:  ";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel7.UseStyleColors = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(36, 327);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(42, 19);
            this.metroLabel8.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel8.TabIndex = 26;
            this.metroLabel8.Text = "RUC: ";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel8.UseStyleColors = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Enabled = false;
            this.btnCancelar.Location = new System.Drawing.Point(813, 249);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 37);
            this.btnCancelar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnCancelar.TabIndex = 24;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancelar.UseSelectable = true;
            this.btnCancelar.UseStyleColors = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Enabled = false;
            this.btnEliminar.Location = new System.Drawing.Point(813, 194);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(91, 37);
            this.btnEliminar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEliminar.TabIndex = 23;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEliminar.UseSelectable = true;
            this.btnEliminar.UseStyleColors = true;
            // 
            // btnEditar
            // 
            this.btnEditar.Enabled = false;
            this.btnEditar.Location = new System.Drawing.Point(813, 138);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(91, 37);
            this.btnEditar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEditar.TabIndex = 22;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEditar.UseSelectable = true;
            this.btnEditar.UseStyleColors = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(813, 31);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(91, 37);
            this.btnNuevo.Style = MetroFramework.MetroColorStyle.Green;
            this.btnNuevo.TabIndex = 20;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnNuevo.UseSelectable = true;
            this.btnNuevo.UseStyleColors = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Location = new System.Drawing.Point(813, 81);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(91, 37);
            this.btnGuardar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnGuardar.TabIndex = 3;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnGuardar.UseSelectable = true;
            this.btnGuardar.UseStyleColors = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(36, 406);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(868, 157);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 19;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            this.dgvLista.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvLista_RowsAdded);
            this.dgvLista.SelectionChanged += new System.EventHandler(this.dgvLista_SelectionChanged);
            this.dgvLista.DoubleClick += new System.EventHandler(this.dgvLista_DoubleClick);
            // 
            // btnConsultarSunat
            // 
            this.btnConsultarSunat.Enabled = false;
            this.btnConsultarSunat.Location = new System.Drawing.Point(349, 67);
            this.btnConsultarSunat.Name = "btnConsultarSunat";
            this.btnConsultarSunat.Size = new System.Drawing.Size(91, 37);
            this.btnConsultarSunat.Style = MetroFramework.MetroColorStyle.Green;
            this.btnConsultarSunat.TabIndex = 32;
            this.btnConsultarSunat.Text = "...";
            this.btnConsultarSunat.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnConsultarSunat.UseSelectable = true;
            this.btnConsultarSunat.UseStyleColors = true;
            this.btnConsultarSunat.Click += new System.EventHandler(this.btnMedios_Click);
            // 
            // tbxDireccion
            // 
            // 
            // 
            // 
            this.tbxDireccion.CustomButton.Image = null;
            this.tbxDireccion.CustomButton.Location = new System.Drawing.Point(533, 1);
            this.tbxDireccion.CustomButton.Name = "";
            this.tbxDireccion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxDireccion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxDireccion.CustomButton.TabIndex = 1;
            this.tbxDireccion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxDireccion.CustomButton.UseSelectable = true;
            this.tbxDireccion.CustomButton.Visible = false;
            this.tbxDireccion.Enabled = false;
            this.tbxDireccion.Lines = new string[0];
            this.tbxDireccion.Location = new System.Drawing.Point(169, 156);
            this.tbxDireccion.MaxLength = 32767;
            this.tbxDireccion.Name = "tbxDireccion";
            this.tbxDireccion.PasswordChar = '\0';
            this.tbxDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxDireccion.SelectedText = "";
            this.tbxDireccion.SelectionLength = 0;
            this.tbxDireccion.SelectionStart = 0;
            this.tbxDireccion.Size = new System.Drawing.Size(555, 23);
            this.tbxDireccion.TabIndex = 2;
            this.tbxDireccion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxDireccion.UseSelectable = true;
            this.tbxDireccion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxDireccion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 156);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(66, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 33;
            this.metroLabel1.Text = "Dirección:";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // tbxTelefono
            // 
            // 
            // 
            // 
            this.tbxTelefono.CustomButton.Image = null;
            this.tbxTelefono.CustomButton.Location = new System.Drawing.Point(127, 1);
            this.tbxTelefono.CustomButton.Name = "";
            this.tbxTelefono.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxTelefono.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxTelefono.CustomButton.TabIndex = 1;
            this.tbxTelefono.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxTelefono.CustomButton.UseSelectable = true;
            this.tbxTelefono.CustomButton.Visible = false;
            this.tbxTelefono.Enabled = false;
            this.tbxTelefono.Lines = new string[0];
            this.tbxTelefono.Location = new System.Drawing.Point(169, 194);
            this.tbxTelefono.MaxLength = 32767;
            this.tbxTelefono.Name = "tbxTelefono";
            this.tbxTelefono.PasswordChar = '\0';
            this.tbxTelefono.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxTelefono.SelectedText = "";
            this.tbxTelefono.SelectionLength = 0;
            this.tbxTelefono.SelectionStart = 0;
            this.tbxTelefono.Size = new System.Drawing.Size(149, 23);
            this.tbxTelefono.TabIndex = 34;
            this.tbxTelefono.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxTelefono.UseSelectable = true;
            this.tbxTelefono.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxTelefono.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 194);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(61, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 35;
            this.metroLabel2.Text = "Teléfono:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // tbxFax
            // 
            // 
            // 
            // 
            this.tbxFax.CustomButton.Image = null;
            this.tbxFax.CustomButton.Location = new System.Drawing.Point(127, 1);
            this.tbxFax.CustomButton.Name = "";
            this.tbxFax.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxFax.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxFax.CustomButton.TabIndex = 1;
            this.tbxFax.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxFax.CustomButton.UseSelectable = true;
            this.tbxFax.CustomButton.Visible = false;
            this.tbxFax.Enabled = false;
            this.tbxFax.Lines = new string[0];
            this.tbxFax.Location = new System.Drawing.Point(169, 235);
            this.tbxFax.MaxLength = 32767;
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.PasswordChar = '\0';
            this.tbxFax.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxFax.SelectedText = "";
            this.tbxFax.SelectionLength = 0;
            this.tbxFax.SelectionStart = 0;
            this.tbxFax.Size = new System.Drawing.Size(149, 23);
            this.tbxFax.TabIndex = 36;
            this.tbxFax.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxFax.UseSelectable = true;
            this.tbxFax.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxFax.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(23, 235);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(35, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel5.TabIndex = 37;
            this.metroLabel5.Text = "Fax: ";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseStyleColors = true;
            // 
            // FrmProveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 591);
            this.Controls.Add(this.tbxFax);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.tbxTelefono);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.tbxDireccion);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnConsultarSunat);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.tbxBuscarRazon);
            this.Controls.Add(this.tbxBuscarRUC);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.tbxRuc);
            this.Controls.Add(this.tbxRazon);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Name = "FrmProveedor";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Proveedor";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmProveedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tbxRazon;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox tbxRuc;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroButton btnBuscar;
        private MetroFramework.Controls.MetroTextBox tbxBuscarRazon;
        private MetroFramework.Controls.MetroTextBox tbxBuscarRUC;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroButton btnCancelar;
        private MetroFramework.Controls.MetroButton btnEliminar;
        private MetroFramework.Controls.MetroButton btnEditar;
        private MetroFramework.Controls.MetroButton btnNuevo;
        private MetroFramework.Controls.MetroButton btnGuardar;
        private MetroFramework.Controls.MetroGrid dgvLista;
        private MetroFramework.Controls.MetroButton btnConsultarSunat;
        private MetroFramework.Controls.MetroTextBox tbxDireccion;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox tbxTelefono;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox tbxFax;
        private MetroFramework.Controls.MetroLabel metroLabel5;
    }
}