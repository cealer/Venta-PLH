﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using Entidades;
using MetroFramework;
using MetroFramework.Forms;
using Presentacion.Formularios.Mantenimiento.Detalles_Medios;
using Utilidades;

using static BOL.ProveedorBOL;

namespace Presentacion.Formularios.Mantenimiento
{
    public partial class FrmProveedor : MetroForm
    {
        public Proveedor proveedor = new Proveedor();
        public Entidades.Usuario usuario = new Entidades.Usuario();

        private void Habilitar()
        {
            btnGuardar.Enabled = true;
            btnConsultarSunat.Enabled = true;
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
            // btnMedios.Enabled = true;
        }

        private void HabilitaEdicion()
        {
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
            //    btnMedios.Enabled = true;
        }

        private void DesHabilitar()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = false;
            btnConsultarSunat.Enabled = false;
        }

        //Ocultar columnas datagridview
        private void OcultarCampos()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["Empresa"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        private void Listar()
        {
            var lista = BuscarUltimos().Where(x => x.Estado.Equals("1")).ToList();
            dgvLista.DataSource = lista;
            OcultarCampos();
        }

        private void HabilitarControles()
        {
            tbxRazon.Enabled = true;
            tbxRuc.Enabled = true;
            tbxDireccion.Enabled = true;
        }

        public FrmProveedor()
        {
            InitializeComponent();
        }

        private void btnMedios_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxRuc.Text.Length == 11)
                {
                    RucConsulta aux = new RucConsulta();
                    aux.Info(tbxRuc.Text);
                    tbxDireccion.Text = aux.Direccion;
                    tbxRazon.Text = aux.NombreCompleto;
                }
            }
            catch (Exception ex)
            {
                //    MessageBox.Show(ex.Message);
                MetroMessageBox.Show(this, $"Intente de nuevo.", "Error al Consultar", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            }

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            proveedor.Id = "";
            HabilitarControles();
            Habilitar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Proveedor();
            aux.RUC = tbxBuscarRUC.Text;
            aux.RazonSocial = tbxBuscarRazon.Text;
            dgvLista.DataSource = ProveedorBOL.Buscar(aux);
        }

        void AbrirMedioProveedor()
        {
            btnConsultarSunat.Enabled = true;
            FrmMedioProveedor form = new FrmMedioProveedor();
            form.proveedor.Id = ProveedorBOL.MaxProveedor();
            form.ShowDialog();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                var aux = new Proveedor();
                aux.Id = proveedor.Id;
                aux.Empresa = usuario.Empresa;
                aux.RUC = tbxRuc.Text;
                aux.RazonSocial = tbxRazon.Text;

                var r = ProveedorBOL.Registro(aux);

                if (r == false)
                {
                    var res = MetroMessageBox.Show(this, $"Guardardo correctamente", "Proveedor registrado correctamente", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }

                else if (r == true)
                {
                    MetroMessageBox.Show(this, $"Proveedor editado correctamente.", "Edición", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }

                Limpiar();
                DesHabilitar();
                btnNuevo.Enabled = true;
                Listar();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al guardar");
            }
        }

        private void Limpiar()
        {
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
        }

        private void FrmProveedor_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void dgvLista_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvLista.ClearSelection();
            DesHabilitar();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            tbxRazon.Text = proveedor.RazonSocial;
            tbxRuc.Text = proveedor.RUC;
            btnConsultarSunat.Enabled = true;
            HabilitarControles();
            btnGuardar.Enabled = true;
            btnConsultarSunat.Enabled = true;
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvLista.SelectedRows.Count == 1)
            {
                HabilitaEdicion();
                proveedor = (Proveedor)dgvLista.CurrentRow.DataBoundItem;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}