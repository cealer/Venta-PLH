﻿namespace Presentacion.Formularios.Mantenimiento
{
    partial class FrmSucursal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mtbxEmpresa = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.mtbxDescripcion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.mtbxDireccion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.btnCancelar = new MetroFramework.Controls.MetroButton();
            this.btnEliminar = new MetroFramework.Controls.MetroButton();
            this.btnEditar = new MetroFramework.Controls.MetroButton();
            this.btnNuevo = new MetroFramework.Controls.MetroButton();
            this.btnGuardar = new MetroFramework.Controls.MetroButton();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            this.btnBuscarEmpresa = new MetroFramework.Controls.MetroButton();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.tbxBuscarSucursal = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.btnBuscar = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.cboEmpresa = new MetroFramework.Controls.MetroComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // mtbxEmpresa
            // 
            // 
            // 
            // 
            this.mtbxEmpresa.CustomButton.Image = null;
            this.mtbxEmpresa.CustomButton.Location = new System.Drawing.Point(201, 1);
            this.mtbxEmpresa.CustomButton.Name = "";
            this.mtbxEmpresa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxEmpresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxEmpresa.CustomButton.TabIndex = 1;
            this.mtbxEmpresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxEmpresa.CustomButton.UseSelectable = true;
            this.mtbxEmpresa.CustomButton.Visible = false;
            this.mtbxEmpresa.Enabled = false;
            this.mtbxEmpresa.Lines = new string[0];
            this.mtbxEmpresa.Location = new System.Drawing.Point(127, 90);
            this.mtbxEmpresa.MaxLength = 250;
            this.mtbxEmpresa.Name = "mtbxEmpresa";
            this.mtbxEmpresa.PasswordChar = '\0';
            this.mtbxEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxEmpresa.SelectedText = "";
            this.mtbxEmpresa.SelectionLength = 0;
            this.mtbxEmpresa.SelectionStart = 0;
            this.mtbxEmpresa.Size = new System.Drawing.Size(223, 23);
            this.mtbxEmpresa.TabIndex = 1;
            this.mtbxEmpresa.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxEmpresa.UseSelectable = true;
            this.mtbxEmpresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxEmpresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(27, 90);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(63, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Empresa:";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // mtbxDescripcion
            // 
            // 
            // 
            // 
            this.mtbxDescripcion.CustomButton.Image = null;
            this.mtbxDescripcion.CustomButton.Location = new System.Drawing.Point(180, 1);
            this.mtbxDescripcion.CustomButton.Name = "";
            this.mtbxDescripcion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxDescripcion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxDescripcion.CustomButton.TabIndex = 1;
            this.mtbxDescripcion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxDescripcion.CustomButton.UseSelectable = true;
            this.mtbxDescripcion.CustomButton.Visible = false;
            this.mtbxDescripcion.Enabled = false;
            this.mtbxDescripcion.Lines = new string[0];
            this.mtbxDescripcion.Location = new System.Drawing.Point(484, 90);
            this.mtbxDescripcion.MaxLength = 250;
            this.mtbxDescripcion.Name = "mtbxDescripcion";
            this.mtbxDescripcion.PasswordChar = '\0';
            this.mtbxDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxDescripcion.SelectedText = "";
            this.mtbxDescripcion.SelectionLength = 0;
            this.mtbxDescripcion.SelectionStart = 0;
            this.mtbxDescripcion.Size = new System.Drawing.Size(202, 23);
            this.mtbxDescripcion.TabIndex = 2;
            this.mtbxDescripcion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxDescripcion.UseSelectable = true;
            this.mtbxDescripcion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxDescripcion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(419, 90);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(59, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Sucursal:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // mtbxDireccion
            // 
            // 
            // 
            // 
            this.mtbxDireccion.CustomButton.Image = null;
            this.mtbxDireccion.CustomButton.Location = new System.Drawing.Point(537, 1);
            this.mtbxDireccion.CustomButton.Name = "";
            this.mtbxDireccion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtbxDireccion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtbxDireccion.CustomButton.TabIndex = 1;
            this.mtbxDireccion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtbxDireccion.CustomButton.UseSelectable = true;
            this.mtbxDireccion.CustomButton.Visible = false;
            this.mtbxDireccion.Enabled = false;
            this.mtbxDireccion.Lines = new string[0];
            this.mtbxDireccion.Location = new System.Drawing.Point(127, 151);
            this.mtbxDireccion.MaxLength = 32767;
            this.mtbxDireccion.Name = "mtbxDireccion";
            this.mtbxDireccion.PasswordChar = '\0';
            this.mtbxDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtbxDireccion.SelectedText = "";
            this.mtbxDireccion.SelectionLength = 0;
            this.mtbxDireccion.SelectionStart = 0;
            this.mtbxDireccion.Size = new System.Drawing.Size(559, 23);
            this.mtbxDireccion.TabIndex = 3;
            this.mtbxDireccion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtbxDireccion.UseSelectable = true;
            this.mtbxDireccion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtbxDireccion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(27, 151);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(70, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 11;
            this.metroLabel4.Text = "Direccion: ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(595, 234);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 37);
            this.btnCancelar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancelar.UseSelectable = true;
            this.btnCancelar.UseStyleColors = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(456, 234);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(91, 37);
            this.btnEliminar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEliminar.TabIndex = 16;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEliminar.UseSelectable = true;
            this.btnEliminar.UseStyleColors = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(327, 234);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(91, 37);
            this.btnEditar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnEditar.TabIndex = 15;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEditar.UseSelectable = true;
            this.btnEditar.UseStyleColors = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(64, 234);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(91, 37);
            this.btnNuevo.Style = MetroFramework.MetroColorStyle.Green;
            this.btnNuevo.TabIndex = 13;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnNuevo.UseSelectable = true;
            this.btnNuevo.UseStyleColors = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(189, 234);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(91, 37);
            this.btnGuardar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnGuardar.TabIndex = 14;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnGuardar.UseSelectable = true;
            this.btnGuardar.UseStyleColors = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(39, 393);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(638, 204);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 18;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            this.dgvLista.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvLista_RowsAdded);
            this.dgvLista.SelectionChanged += new System.EventHandler(this.dgvLista_SelectionChanged);
            // 
            // btnBuscarEmpresa
            // 
            this.btnBuscarEmpresa.Enabled = false;
            this.btnBuscarEmpresa.Location = new System.Drawing.Point(366, 90);
            this.btnBuscarEmpresa.Name = "btnBuscarEmpresa";
            this.btnBuscarEmpresa.Size = new System.Drawing.Size(25, 25);
            this.btnBuscarEmpresa.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscarEmpresa.TabIndex = 19;
            this.btnBuscarEmpresa.Text = "....";
            this.btnBuscarEmpresa.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscarEmpresa.UseSelectable = true;
            this.btnBuscarEmpresa.UseStyleColors = true;
            this.btnBuscarEmpresa.Click += new System.EventHandler(this.btnBuscarEmpresa_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.BackColor = System.Drawing.Color.SeaGreen;
            this.metroLabel9.Location = new System.Drawing.Point(3, 293);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(683, 10);
            this.metroLabel9.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel9.TabIndex = 20;
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel9.UseCustomBackColor = true;
            // 
            // tbxBuscarSucursal
            // 
            // 
            // 
            // 
            this.tbxBuscarSucursal.CustomButton.Image = null;
            this.tbxBuscarSucursal.CustomButton.Location = new System.Drawing.Point(144, 1);
            this.tbxBuscarSucursal.CustomButton.Name = "";
            this.tbxBuscarSucursal.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxBuscarSucursal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxBuscarSucursal.CustomButton.TabIndex = 1;
            this.tbxBuscarSucursal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxBuscarSucursal.CustomButton.UseSelectable = true;
            this.tbxBuscarSucursal.CustomButton.Visible = false;
            this.tbxBuscarSucursal.Lines = new string[0];
            this.tbxBuscarSucursal.Location = new System.Drawing.Point(389, 340);
            this.tbxBuscarSucursal.MaxLength = 250;
            this.tbxBuscarSucursal.Name = "tbxBuscarSucursal";
            this.tbxBuscarSucursal.PasswordChar = '\0';
            this.tbxBuscarSucursal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBuscarSucursal.SelectedText = "";
            this.tbxBuscarSucursal.SelectionLength = 0;
            this.tbxBuscarSucursal.SelectionStart = 0;
            this.tbxBuscarSucursal.Size = new System.Drawing.Size(166, 23);
            this.tbxBuscarSucursal.TabIndex = 21;
            this.tbxBuscarSucursal.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxBuscarSucursal.UseSelectable = true;
            this.tbxBuscarSucursal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxBuscarSucursal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbxBuscarSucursal.Click += new System.EventHandler(this.metroTextBox1_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(312, 340);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(59, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 22;
            this.metroLabel3.Text = "Sucursal:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            this.metroLabel3.Click += new System.EventHandler(this.metroLabel3_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(595, 332);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(91, 37);
            this.btnBuscar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscar.TabIndex = 23;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscar.UseSelectable = true;
            this.btnBuscar.UseStyleColors = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(39, 340);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(63, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel5.TabIndex = 24;
            this.metroLabel5.Text = "Empresa:";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseStyleColors = true;
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.FormattingEnabled = true;
            this.cboEmpresa.ItemHeight = 23;
            this.cboEmpresa.Location = new System.Drawing.Point(127, 340);
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.Size = new System.Drawing.Size(160, 29);
            this.cboEmpresa.Style = MetroFramework.MetroColorStyle.Green;
            this.cboEmpresa.TabIndex = 29;
            this.cboEmpresa.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cboEmpresa.UseSelectable = true;
            // 
            // FrmSucursal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(701, 620);
            this.Controls.Add(this.cboEmpresa);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.tbxBuscarSucursal);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.btnBuscarEmpresa);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.mtbxDireccion);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.mtbxDescripcion);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.mtbxEmpresa);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FrmSucursal";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Sucursal";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmSucursal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox mtbxEmpresa;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox mtbxDescripcion;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox mtbxDireccion;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton btnCancelar;
        private MetroFramework.Controls.MetroButton btnEliminar;
        private MetroFramework.Controls.MetroButton btnEditar;
        private MetroFramework.Controls.MetroButton btnNuevo;
        private MetroFramework.Controls.MetroButton btnGuardar;
        private MetroFramework.Controls.MetroGrid dgvLista;
        private MetroFramework.Controls.MetroButton btnBuscarEmpresa;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox tbxBuscarSucursal;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroButton btnBuscar;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox cboEmpresa;
    }
}