﻿using BOL;
using Entidades;
using MetroFramework;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Mantenimiento
{
    public partial class FrmSucursal : MetroForm
    {

        public Sucursal obj = new Sucursal();

        private void Limpiar()
        {
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
        }

        /*
                void Habilitar()
                {
                    btnGuardar.Enabled = true;
                    btnEliminar.Enabled = true;
                    btnEditar.Enabled = true;
                    btnCancelar.Enabled = true;
                }
        */

        private void HabilitaEdicion()
        {
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
            btnBuscarEmpresa.Enabled = true;
        }

        void DesHabilitar()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = false;
        }

        void HabilitarControles()
        {
            mtbxDireccion.Enabled = true;
            mtbxDescripcion.Enabled = true;
            btnBuscarEmpresa.Enabled = true;
        }

        public FrmSucursal()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            obj.Id = "";
            HabilitarControles();
            btnGuardar.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                var aux = new Sucursal();
                aux.Id = obj.Id;
                aux.Descripcion = mtbxDescripcion.Text;
                aux.Direccion = mtbxDireccion.Text;
                aux._Empresa = new Entidades.Empresa();
                aux._Empresa.Id = obj._Empresa.Id;

                var r = SucursalBOL.Registro(aux);

                if (r == false)
                {
                    MetroMessageBox.Show(this, $"Sucursal registrada correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }
                else if (r == true)
                {
                    MetroMessageBox.Show(this, $"Sucursal editada correctamente.", "Edición", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }

                Limpiar();
                DesHabilitar();
                btnNuevo.Enabled = true;
                Listar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error al guardar");
            }
        }

        void Listar()
        {
            var lista = SucursalBOL.BuscarUltimos().Where(x => x.Estado.Equals("1")).ToList();
            dgvLista.DataSource = lista;
            FormatoDataGridView();
        }

        void CargarEmpresaCombo()
        {
            var aux = new Entidades.Empresa();
            aux.RazonSocial = "";
            aux.Ruc = "";

            cboEmpresa.DataSource = EmpresaBOL.Buscar(aux).Where(x => x.Estado.Equals("1")).ToList();
            cboEmpresa.DisplayMember = "RazonSocial";
            cboEmpresa.ValueMember = "Id";
        }

        //Ocultar columnas datagridview
        void FormatoDataGridView()
        {
            dgvLista.Columns["Id"].Visible = false;
            //dgvLista.Columns["IdEmpresa"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
            dgvLista.Columns["Descripcion"].Width = 150;
        }

        private void FrmSucursal_Load(object sender, EventArgs e)
        {
            Listar();
            CargarEmpresaCombo();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void btnBuscarEmpresa_Click(object sender, EventArgs e)
        {
            var aux = new Buscar.FrmBuscar();
            aux.Tabla = "Empresa";
            aux.ShowDialog();
            obj._Empresa.Id = aux.Resultados[0];
            mtbxEmpresa.Text = aux.Resultados[1];
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvLista.SelectedRows.Count > -1)
            {
                HabilitaEdicion();
                obj = (Sucursal)dgvLista.CurrentRow.DataBoundItem;
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r.Equals(DialogResult.Yes))
            {
                obj = (Sucursal)dgvLista.CurrentRow.DataBoundItem;
                SucursalBOL.Eliminar(obj.Id);
                Listar();
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                HabilitarControles();
                btnGuardar.Enabled = true;
                btnNuevo.Enabled = false;
                if (dgvLista.CurrentRow.Index > -1)
                {
                    mtbxEmpresa.Text = obj._Empresa.ToString();
                    mtbxDescripcion.Text = obj.Descripcion;
                    mtbxDireccion.Text = obj.Direccion;
                }
            }
            catch (Exception)
            {
                MetroMessageBox.Show(this, $"Debe seleccionar un elemento de la lista.", "Error de selección.", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            }
        }

        private void metroLabel3_Click(object sender, EventArgs e)
        {

        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Sucursal();
            aux.Descripcion = tbxBuscarSucursal.Text;
            aux._Empresa = new Entidades.Empresa();
            aux._Empresa.Id = cboEmpresa.SelectedValue.ToString();
            dgvLista.DataSource = SucursalBOL.Buscar(aux);
        }

        private void dgvLista_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvLista.ClearSelection();
            DesHabilitar();
        }
    }
}