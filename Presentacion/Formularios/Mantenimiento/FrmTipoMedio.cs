﻿using BOL;
using Entidades;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace Presentacion.Formularios.Mantenimiento
{
    public partial class FrmTipoMedio : MetroForm
    {
        public TipoMedio obj = new TipoMedio();

        //Ocultar columnas datagridview
        private void FormatoDataGridView()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
            dgvLista.Columns["Descripcion"].Width = 150;
        }

        void DesHabilitar()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void HabilitaEdicion()
        {
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
        }


        public FrmTipoMedio()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            obj.Id = "";
            tbxDescripcion.Enabled = true;
            btnGuardar.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                var aux = new TipoMedio
                {
                    Id = obj.Id,
                    Descripcion = tbxDescripcion.Text
                };

                var r = TipoMedioBOL.Registro(aux);

                if (r == false)
                {
                    MetroMessageBox.Show(this, $"{Text} registrada correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }
                else if (r == true)
                {
                    MetroMessageBox.Show(this, $"{Text} editada correctamente.", "Edición", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }

                Limpiar();
                DesHabilitar();
                btnNuevo.Enabled = true;
                Listar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error al guardar");
            }
        }

        private void Listar()
        {
            var lista = TipoMedioBOL.BuscarUltimos();
            dgvLista.DataSource = lista;
            FormatoDataGridView();
        }

        private void FrmFamilia_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (r.Equals(DialogResult.Yes))
                {
                    TipoMedioBOL.Eliminar(obj.Id);
                    Listar();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No se pudo Eliminar");
            }
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLista.SelectedRows.Count == 1)
                {
                    obj = (TipoMedio)dgvLista.CurrentRow.DataBoundItem;
                    HabilitaEdicion();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al seleccionar");
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                HabilitaEdicion();
                btnGuardar.Enabled = true;
                btnNuevo.Enabled = false;
                tbxDescripcion.Enabled = true;
                if (dgvLista.SelectedRows.Count == 1)
                {
                    tbxDescripcion.Text = obj.Descripcion;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private void Limpiar()
        {
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvLista_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                dgvLista.ClearSelection();
                DesHabilitar();

            }
            catch (Exception)
            {
                MessageBox.Show("Se ha generado un error al listar");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.TipoMedio { Descripcion = tbxBuscar.Text };
            dgvLista.DataSource = TipoMedioBOL.Buscar(aux);
        }
    }
}