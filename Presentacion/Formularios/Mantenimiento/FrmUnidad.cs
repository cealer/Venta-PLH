﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using BOL;
using MetroFramework;


namespace Presentacion.Formularios.Mantenimiento
{
    public partial class FrmUnidad : MetroForm
    {

        public Unidad obj = new Unidad();

        //Ocultar columnas datagridview
        private void OcultarCampos()
        {
            dgvLista.Columns["Id"].Visible = false;
            dgvLista.Columns["Estado"].Visible = false;
        }

        private void Limpiar()
        {
            using (var aux = new Utilidades.Controles.Controles())
            {
                aux.Limpiar(this);
            }
        }

        void DesHabilitar()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void HabilitaEdicion()
        {
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnCancelar.Enabled = true;
        }


        public FrmUnidad()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                var aux = new Unidad
                {
                    Id = obj.Id,
                    Descripcion = tbxDescripcion.Text,
                    NombreCorto = tbxNombreCorto.Text
                };

                var r = UnidadBOL.Registro(aux);

                if (r == false)
                {
                    MetroMessageBox.Show(this, $"{Text} registrada correctamente.", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }
                else if (r == true)
                {
                    MetroMessageBox.Show(this, $"{Text} editada correctamente.", "Edición", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
                }

                Limpiar();
                DesHabilitar();
                btnNuevo.Enabled = true;
                Ultimos();
            }
            catch (Exception)
            {
                MessageBox.Show("Error al guardar");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            obj.Id = "";
            tbxDescripcion.Enabled = true;
            tbxNombreCorto.Enabled = true;
        }

        private void Ultimos()
        {
            var aux = new Unidad();
            aux.Descripcion = "";
            var lista = UnidadBOL.BuscarUltimos();
            dgvLista.DataSource = lista;
            OcultarCampos();
        }

        private void FrmUnidad_Load(object sender, EventArgs e)
        {
            Ultimos();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var aux = new Entidades.Unidad() { Descripcion = tbxBuscar.Text };
            dgvLista.DataSource = UnidadBOL.Buscar(aux);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                HabilitaEdicion();
                btnGuardar.Enabled = true;
                btnNuevo.Enabled = false;
                tbxDescripcion.Enabled = true;
                tbxNombreCorto.Enabled = true;
                if (dgvLista.SelectedRows.Count == 1)
                {
                    tbxDescripcion.Text = obj.Descripcion;
                    tbxNombreCorto.Text = obj.NombreCorto;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var r = MetroMessageBox.Show(this, "¿Está seguro que desea eliminar?.", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (r.Equals(DialogResult.Yes))
                {
                    UnidadBOL.Eliminar(obj.Id);
                    Ultimos();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No se pudo Eliminar");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLista.SelectedRows.Count == 1)
                {
                    obj = (Unidad)dgvLista.CurrentRow.DataBoundItem;
                    HabilitaEdicion();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al seleccionar");
            }
        }

        private void dgvLista_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                dgvLista.ClearSelection();
                DesHabilitar();

            }
            catch (Exception)
            {
                MessageBox.Show("Se ha generado un error al listar");
            }
        }

        private void dgvLista_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}