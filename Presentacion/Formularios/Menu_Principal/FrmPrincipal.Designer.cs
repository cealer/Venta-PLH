﻿namespace Presentacion.Formularios.Menu_Principal
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.lblTipoUsuario = new MetroFramework.Controls.MetroLabel();
            this.lblUsuario = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroTile5 = new MetroFramework.Controls.MetroTile();
            this.metroTile4 = new MetroFramework.Controls.MetroTile();
            this.metroTile6 = new MetroFramework.Controls.MetroTile();
            this.metroTile3 = new MetroFramework.Controls.MetroTile();
            this.metroTile2 = new MetroFramework.Controls.MetroTile();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroTile9 = new MetroFramework.Controls.MetroTile();
            this.metroTile8 = new MetroFramework.Controls.MetroTile();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroTile7 = new MetroFramework.Controls.MetroTile();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.btnCambiarSucursal = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLink2 = new MetroFramework.Controls.MetroLink();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.cboSucursal = new MetroFramework.Controls.MetroComboBox();
            this.cboEmpresa = new MetroFramework.Controls.MetroComboBox();
            this.dgvFactura = new MetroFramework.Controls.MetroGrid();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblFecha = new MetroFramework.Controls.MetroLabel();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.mtpMantenimiento = new MetroFramework.Controls.MetroTabPage();
            this.pbxMoneda = new System.Windows.Forms.PictureBox();
            this.metroLabel39 = new MetroFramework.Controls.MetroLabel();
            this.pbxFamilia = new System.Windows.Forms.PictureBox();
            this.metroLabel38 = new MetroFramework.Controls.MetroLabel();
            this.pbxComprobantes = new System.Windows.Forms.PictureBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.pbxProveedor = new System.Windows.Forms.PictureBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.pcbxUnidad = new System.Windows.Forms.PictureBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.pcbxSucursal = new System.Windows.Forms.PictureBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.pbxManteEmpresa = new System.Windows.Forms.PictureBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pbxOrdenCompra = new System.Windows.Forms.PictureBox();
            this.pcbxArticulo = new System.Windows.Forms.PictureBox();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel37 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel36 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.pbxConsulta = new System.Windows.Forms.PictureBox();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.pbxControl = new System.Windows.Forms.PictureBox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.pbxInfoRed = new System.Windows.Forms.PictureBox();
            this.metroTabPage7 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel31 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.metroLabel30 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.metroTabPage8 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel35 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.metroLabel34 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.metroLabel33 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.metroLabel32 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.lblHora = new MetroFramework.Controls.MetroLabel();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.btnBuscarProveedor = new MetroFramework.Controls.MetroButton();
            this.metroLabel40 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox3 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox2 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.lblTipoCambio = new MetroFramework.Controls.MetroLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.pbxFb = new System.Windows.Forms.PictureBox();
            this.pbxWeb = new System.Windows.Forms.PictureBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.pbxYoutube = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pbxOpciones = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactura)).BeginInit();
            this.metroTabControl1.SuspendLayout();
            this.mtpMantenimiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFamilia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbxUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbxSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxManteEmpresa)).BeginInit();
            this.metroTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxOrdenCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbxArticulo)).BeginInit();
            this.metroTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.metroTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.metroTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxConsulta)).BeginInit();
            this.metroTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxInfoRed)).BeginInit();
            this.metroTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.metroTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.metroPanel4.SuspendLayout();
            this.metroPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxWeb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxYoutube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxOpciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTipoUsuario
            // 
            this.lblTipoUsuario.AutoSize = true;
            this.lblTipoUsuario.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblTipoUsuario.Location = new System.Drawing.Point(1061, 38);
            this.lblTipoUsuario.Name = "lblTipoUsuario";
            this.lblTipoUsuario.Size = new System.Drawing.Size(81, 19);
            this.lblTipoUsuario.TabIndex = 3;
            this.lblTipoUsuario.Text = "Tipo usuario";
            this.lblTipoUsuario.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblTipoUsuario.UseCustomForeColor = true;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblUsuario.Location = new System.Drawing.Point(1061, 19);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(53, 19);
            this.lblUsuario.TabIndex = 5;
            this.lblUsuario.Text = "Usuario";
            this.lblUsuario.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblUsuario.UseCustomForeColor = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroTile5);
            this.metroPanel1.Controls.Add(this.metroTile4);
            this.metroPanel1.Controls.Add(this.metroTile6);
            this.metroPanel1.Controls.Add(this.metroTile3);
            this.metroPanel1.Controls.Add(this.metroTile2);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroTile1);
            this.metroPanel1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(24, 320);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(477, 404);
            this.metroPanel1.TabIndex = 7;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.UseCustomForeColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroTile5
            // 
            this.metroTile5.ActiveControl = null;
            this.metroTile5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile5.Location = new System.Drawing.Point(7, 264);
            this.metroTile5.Name = "metroTile5";
            this.metroTile5.Size = new System.Drawing.Size(229, 126);
            this.metroTile5.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroTile5.TabIndex = 8;
            this.metroTile5.Text = "SUCURSAL";
            this.metroTile5.TileImage = global::Presentacion.Properties.Resources.construction_1;
            this.metroTile5.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile5.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile5.UseSelectable = true;
            this.metroTile5.UseTileImage = true;
            // 
            // metroTile4
            // 
            this.metroTile4.ActiveControl = null;
            this.metroTile4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile4.Location = new System.Drawing.Point(240, 149);
            this.metroTile4.Name = "metroTile4";
            this.metroTile4.Size = new System.Drawing.Size(229, 109);
            this.metroTile4.Style = MetroFramework.MetroColorStyle.Yellow;
            this.metroTile4.TabIndex = 7;
            this.metroTile4.Text = "MONEDA";
            this.metroTile4.TileImage = global::Presentacion.Properties.Resources.money;
            this.metroTile4.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile4.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile4.UseSelectable = true;
            this.metroTile4.UseTileImage = true;
            // 
            // metroTile6
            // 
            this.metroTile6.ActiveControl = null;
            this.metroTile6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile6.Location = new System.Drawing.Point(240, 264);
            this.metroTile6.Name = "metroTile6";
            this.metroTile6.Size = new System.Drawing.Size(229, 126);
            this.metroTile6.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTile6.TabIndex = 2;
            this.metroTile6.Text = "PROVEEDOR";
            this.metroTile6.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTile6.TileImage = global::Presentacion.Properties.Resources.people;
            this.metroTile6.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile6.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile6.UseSelectable = true;
            this.metroTile6.UseTileImage = true;
            // 
            // metroTile3
            // 
            this.metroTile3.ActiveControl = null;
            this.metroTile3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile3.Location = new System.Drawing.Point(7, 149);
            this.metroTile3.Name = "metroTile3";
            this.metroTile3.Size = new System.Drawing.Size(229, 109);
            this.metroTile3.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTile3.TabIndex = 6;
            this.metroTile3.Text = "COMPROBANTES";
            this.metroTile3.TileImage = global::Presentacion.Properties.Resources.money_1_;
            this.metroTile3.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile3.UseSelectable = true;
            this.metroTile3.UseTileImage = true;
            // 
            // metroTile2
            // 
            this.metroTile2.ActiveControl = null;
            this.metroTile2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile2.Location = new System.Drawing.Point(240, 33);
            this.metroTile2.Name = "metroTile2";
            this.metroTile2.Size = new System.Drawing.Size(229, 109);
            this.metroTile2.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroTile2.TabIndex = 5;
            this.metroTile2.Text = "ALMACEN";
            this.metroTile2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTile2.TileImage = global::Presentacion.Properties.Resources.buildings;
            this.metroTile2.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile2.UseSelectable = true;
            this.metroTile2.UseTileImage = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroLabel3.Location = new System.Drawing.Point(0, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(131, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "ACCESOS DIRECTOS";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.BackColor = System.Drawing.Color.Black;
            this.metroTile1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile1.Location = new System.Drawing.Point(7, 33);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(229, 109);
            this.metroTile1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTile1.TabIndex = 3;
            this.metroTile1.Text = "EMPRESA";
            this.metroTile1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTile1.TileImage = global::Presentacion.Properties.Resources.construction_2;
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.UseTileImage = true;
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.metroTile9);
            this.metroPanel2.Controls.Add(this.metroTile8);
            this.metroPanel2.Controls.Add(this.metroLabel4);
            this.metroPanel2.Controls.Add(this.metroTile7);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(514, 319);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(239, 404);
            this.metroPanel2.TabIndex = 8;
            this.metroPanel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroTile9
            // 
            this.metroTile9.ActiveControl = null;
            this.metroTile9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile9.Location = new System.Drawing.Point(5, 264);
            this.metroTile9.Name = "metroTile9";
            this.metroTile9.Size = new System.Drawing.Size(229, 126);
            this.metroTile9.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTile9.TabIndex = 6;
            this.metroTile9.Text = "REPORTES";
            this.metroTile9.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile9.UseSelectable = true;
            this.metroTile9.UseTileImage = true;
            // 
            // metroTile8
            // 
            this.metroTile8.ActiveControl = null;
            this.metroTile8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile8.Location = new System.Drawing.Point(5, 149);
            this.metroTile8.Name = "metroTile8";
            this.metroTile8.Size = new System.Drawing.Size(229, 109);
            this.metroTile8.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTile8.TabIndex = 5;
            this.metroTile8.Text = "COMPRAS";
            this.metroTile8.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile8.UseSelectable = true;
            this.metroTile8.UseTileImage = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroLabel4.Location = new System.Drawing.Point(0, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(103, 19);
            this.metroLabel4.TabIndex = 4;
            this.metroLabel4.Text = "OPERACIONES ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseCustomForeColor = true;
            this.metroLabel4.Visible = false;
            // 
            // metroTile7
            // 
            this.metroTile7.ActiveControl = null;
            this.metroTile7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroTile7.Location = new System.Drawing.Point(7, 33);
            this.metroTile7.Name = "metroTile7";
            this.metroTile7.Size = new System.Drawing.Size(229, 109);
            this.metroTile7.Style = MetroFramework.MetroColorStyle.Red;
            this.metroTile7.TabIndex = 3;
            this.metroTile7.Text = "VENTAS";
            this.metroTile7.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile7.UseSelectable = true;
            this.metroTile7.UseTileImage = true;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.btnCambiarSucursal);
            this.metroPanel3.Controls.Add(this.metroButton1);
            this.metroPanel3.Controls.Add(this.metroLink2);
            this.metroPanel3.Controls.Add(this.metroLink1);
            this.metroPanel3.Controls.Add(this.cboSucursal);
            this.metroPanel3.Controls.Add(this.cboEmpresa);
            this.metroPanel3.Controls.Add(this.dgvFactura);
            this.metroPanel3.Controls.Add(this.metroLabel5);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(759, 319);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(559, 404);
            this.metroPanel3.TabIndex = 9;
            this.metroPanel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // btnCambiarSucursal
            // 
            this.btnCambiarSucursal.Location = new System.Drawing.Point(234, 367);
            this.btnCambiarSucursal.Name = "btnCambiarSucursal";
            this.btnCambiarSucursal.Size = new System.Drawing.Size(128, 23);
            this.btnCambiarSucursal.Style = MetroFramework.MetroColorStyle.Green;
            this.btnCambiarSucursal.TabIndex = 10;
            this.btnCambiarSucursal.Text = "Cambiar";
            this.btnCambiarSucursal.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCambiarSucursal.UseSelectable = true;
            this.btnCambiarSucursal.UseStyleColors = true;
            this.btnCambiarSucursal.Click += new System.EventHandler(this.btnCambiarSucursal_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(422, 276);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(128, 23);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroButton1.TabIndex = 9;
            this.metroButton1.Text = "VER TODOS";
            this.metroButton1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.UseStyleColors = true;
            // 
            // metroLink2
            // 
            this.metroLink2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.metroLink2.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.metroLink2.Location = new System.Drawing.Point(287, 323);
            this.metroLink2.Name = "metroLink2";
            this.metroLink2.Size = new System.Drawing.Size(75, 23);
            this.metroLink2.TabIndex = 8;
            this.metroLink2.Text = "Sucursal";
            this.metroLink2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLink2.UseCustomForeColor = true;
            this.metroLink2.UseSelectable = true;
            // 
            // metroLink1
            // 
            this.metroLink1.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.metroLink1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.metroLink1.Location = new System.Drawing.Point(7, 317);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.Size = new System.Drawing.Size(75, 23);
            this.metroLink1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLink1.TabIndex = 8;
            this.metroLink1.Text = "Empresa";
            this.metroLink1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLink1.UseCustomForeColor = true;
            this.metroLink1.UseSelectable = true;
            // 
            // cboSucursal
            // 
            this.cboSucursal.FormattingEnabled = true;
            this.cboSucursal.ItemHeight = 23;
            this.cboSucursal.Location = new System.Drawing.Point(364, 317);
            this.cboSucursal.Name = "cboSucursal";
            this.cboSucursal.Size = new System.Drawing.Size(186, 29);
            this.cboSucursal.TabIndex = 7;
            this.cboSucursal.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cboSucursal.UseSelectable = true;
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.FormattingEnabled = true;
            this.cboEmpresa.ItemHeight = 23;
            this.cboEmpresa.Location = new System.Drawing.Point(82, 317);
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.Size = new System.Drawing.Size(187, 29);
            this.cboEmpresa.TabIndex = 7;
            this.cboEmpresa.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cboEmpresa.UseSelectable = true;
            this.cboEmpresa.SelectedIndexChanged += new System.EventHandler(this.cboEmpresa_SelectedIndexChanged);
            // 
            // dgvFactura
            // 
            this.dgvFactura.AllowUserToAddRows = false;
            this.dgvFactura.AllowUserToResizeColumns = false;
            this.dgvFactura.AllowUserToResizeRows = false;
            this.dgvFactura.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvFactura.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvFactura.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvFactura.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFactura.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFactura.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFactura.EnableHeadersVisualStyles = false;
            this.dgvFactura.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFactura.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvFactura.Location = new System.Drawing.Point(3, 33);
            this.dgvFactura.Name = "dgvFactura";
            this.dgvFactura.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFactura.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFactura.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvFactura.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvFactura.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFactura.Size = new System.Drawing.Size(552, 225);
            this.dgvFactura.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvFactura.TabIndex = 6;
            this.dgvFactura.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvFactura.UseCustomForeColor = true;
            this.dgvFactura.UseStyleColors = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nº de factura";
            this.Column1.Name = "Column1";
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Cliente";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Fecha de factura";
            this.Column3.Name = "Column3";
            this.Column3.Width = 90;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Fecha de vencimiento";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Total a pagar";
            this.Column5.Name = "Column5";
            this.Column5.Width = 90;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Forma de pago";
            this.Column6.Name = "Column6";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroLabel5.Location = new System.Drawing.Point(0, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(152, 19);
            this.metroLabel5.TabIndex = 5;
            this.metroLabel5.Text = "FACTURAS PENDIENTES";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblFecha.Location = new System.Drawing.Point(514, 19);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(50, 19);
            this.lblFecha.TabIndex = 12;
            this.lblFecha.Text = "FECHA";
            this.lblFecha.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblFecha.UseCustomForeColor = true;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.mtpMantenimiento);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Controls.Add(this.metroTabPage5);
            this.metroTabControl1.Controls.Add(this.metroTabPage4);
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage6);
            this.metroTabControl1.Controls.Add(this.metroTabPage7);
            this.metroTabControl1.Controls.Add(this.metroTabPage8);
            this.metroTabControl1.Location = new System.Drawing.Point(23, 84);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 1;
            this.metroTabControl1.Size = new System.Drawing.Size(1290, 153);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTabControl1.TabIndex = 16;
            this.metroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabControl1.UseSelectable = true;
            // 
            // mtpMantenimiento
            // 
            this.mtpMantenimiento.Controls.Add(this.pbxMoneda);
            this.mtpMantenimiento.Controls.Add(this.metroLabel39);
            this.mtpMantenimiento.Controls.Add(this.pbxFamilia);
            this.mtpMantenimiento.Controls.Add(this.metroLabel38);
            this.mtpMantenimiento.Controls.Add(this.pbxComprobantes);
            this.mtpMantenimiento.Controls.Add(this.metroLabel19);
            this.mtpMantenimiento.Controls.Add(this.pbxProveedor);
            this.mtpMantenimiento.Controls.Add(this.metroLabel11);
            this.mtpMantenimiento.Controls.Add(this.pcbxUnidad);
            this.mtpMantenimiento.Controls.Add(this.metroLabel15);
            this.mtpMantenimiento.Controls.Add(this.pcbxSucursal);
            this.mtpMantenimiento.Controls.Add(this.metroLabel12);
            this.mtpMantenimiento.Controls.Add(this.pbxManteEmpresa);
            this.mtpMantenimiento.Controls.Add(this.metroLabel6);
            this.mtpMantenimiento.HorizontalScrollbarBarColor = true;
            this.mtpMantenimiento.HorizontalScrollbarHighlightOnWheel = false;
            this.mtpMantenimiento.HorizontalScrollbarSize = 10;
            this.mtpMantenimiento.Location = new System.Drawing.Point(4, 38);
            this.mtpMantenimiento.Name = "mtpMantenimiento";
            this.mtpMantenimiento.Size = new System.Drawing.Size(1282, 111);
            this.mtpMantenimiento.Style = MetroFramework.MetroColorStyle.Green;
            this.mtpMantenimiento.TabIndex = 0;
            this.mtpMantenimiento.Text = "Mantenimiento";
            this.mtpMantenimiento.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mtpMantenimiento.VerticalScrollbarBarColor = true;
            this.mtpMantenimiento.VerticalScrollbarHighlightOnWheel = false;
            this.mtpMantenimiento.VerticalScrollbarSize = 10;
            this.mtpMantenimiento.Click += new System.EventHandler(this.mtpMantenimiento_Click);
            // 
            // pbxMoneda
            // 
            this.pbxMoneda.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxMoneda.Image = global::Presentacion.Properties.Resources.money;
            this.pbxMoneda.Location = new System.Drawing.Point(514, 35);
            this.pbxMoneda.Name = "pbxMoneda";
            this.pbxMoneda.Size = new System.Drawing.Size(79, 64);
            this.pbxMoneda.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMoneda.TabIndex = 23;
            this.pbxMoneda.TabStop = false;
            this.pbxMoneda.Click += new System.EventHandler(this.pbxMoneda_Click);
            // 
            // metroLabel39
            // 
            this.metroLabel39.AutoSize = true;
            this.metroLabel39.Location = new System.Drawing.Point(521, 11);
            this.metroLabel39.Name = "metroLabel39";
            this.metroLabel39.Size = new System.Drawing.Size(58, 19);
            this.metroLabel39.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel39.TabIndex = 22;
            this.metroLabel39.Text = "Moneda";
            this.metroLabel39.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxFamilia
            // 
            this.pbxFamilia.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxFamilia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxFamilia.Image = global::Presentacion.Properties.Resources.familia;
            this.pbxFamilia.Location = new System.Drawing.Point(429, 35);
            this.pbxFamilia.Name = "pbxFamilia";
            this.pbxFamilia.Size = new System.Drawing.Size(79, 64);
            this.pbxFamilia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxFamilia.TabIndex = 21;
            this.pbxFamilia.TabStop = false;
            this.pbxFamilia.Click += new System.EventHandler(this.pbxFamilia_Click);
            // 
            // metroLabel38
            // 
            this.metroLabel38.AutoSize = true;
            this.metroLabel38.Location = new System.Drawing.Point(436, 11);
            this.metroLabel38.Name = "metroLabel38";
            this.metroLabel38.Size = new System.Drawing.Size(50, 19);
            this.metroLabel38.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel38.TabIndex = 20;
            this.metroLabel38.Text = "Familia";
            this.metroLabel38.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxComprobantes
            // 
            this.pbxComprobantes.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxComprobantes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxComprobantes.Image = global::Presentacion.Properties.Resources.money_1_;
            this.pbxComprobantes.Location = new System.Drawing.Point(344, 35);
            this.pbxComprobantes.Name = "pbxComprobantes";
            this.pbxComprobantes.Size = new System.Drawing.Size(79, 64);
            this.pbxComprobantes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxComprobantes.TabIndex = 19;
            this.pbxComprobantes.TabStop = false;
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(335, 11);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(97, 19);
            this.metroLabel19.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel19.TabIndex = 18;
            this.metroLabel19.Text = "Comprobantes";
            this.metroLabel19.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxProveedor
            // 
            this.pbxProveedor.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxProveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxProveedor.Image = global::Presentacion.Properties.Resources.people;
            this.pbxProveedor.Location = new System.Drawing.Point(174, 35);
            this.pbxProveedor.Name = "pbxProveedor";
            this.pbxProveedor.Size = new System.Drawing.Size(79, 64);
            this.pbxProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxProveedor.TabIndex = 13;
            this.pbxProveedor.TabStop = false;
            this.pbxProveedor.Click += new System.EventHandler(this.pbxProveedor_Click);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(174, 11);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(71, 19);
            this.metroLabel11.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel11.TabIndex = 10;
            this.metroLabel11.Text = "Proveedor";
            this.metroLabel11.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pcbxUnidad
            // 
            this.pcbxUnidad.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pcbxUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbxUnidad.Image = global::Presentacion.Properties.Resources.scale;
            this.pcbxUnidad.Location = new System.Drawing.Point(259, 35);
            this.pcbxUnidad.Name = "pcbxUnidad";
            this.pcbxUnidad.Size = new System.Drawing.Size(79, 64);
            this.pcbxUnidad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbxUnidad.TabIndex = 9;
            this.pcbxUnidad.TabStop = false;
            this.pcbxUnidad.Click += new System.EventHandler(this.pcbxUnidad_Click);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(259, 11);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(51, 19);
            this.metroLabel15.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel15.TabIndex = 8;
            this.metroLabel15.Text = "Unidad";
            this.metroLabel15.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pcbxSucursal
            // 
            this.pcbxSucursal.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pcbxSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbxSucursal.Image = global::Presentacion.Properties.Resources.construction_1;
            this.pcbxSucursal.Location = new System.Drawing.Point(90, 35);
            this.pcbxSucursal.Name = "pcbxSucursal";
            this.pcbxSucursal.Size = new System.Drawing.Size(79, 64);
            this.pcbxSucursal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbxSucursal.TabIndex = 7;
            this.pcbxSucursal.TabStop = false;
            this.pcbxSucursal.Click += new System.EventHandler(this.pcbxSucursal_Click);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(90, 11);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(56, 19);
            this.metroLabel12.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel12.TabIndex = 6;
            this.metroLabel12.Text = "Sucursal";
            this.metroLabel12.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxManteEmpresa
            // 
            this.pbxManteEmpresa.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxManteEmpresa.Image = global::Presentacion.Properties.Resources.construction_2;
            this.pbxManteEmpresa.Location = new System.Drawing.Point(6, 35);
            this.pbxManteEmpresa.Name = "pbxManteEmpresa";
            this.pbxManteEmpresa.Size = new System.Drawing.Size(79, 64);
            this.pbxManteEmpresa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxManteEmpresa.TabIndex = 3;
            this.pbxManteEmpresa.TabStop = false;
            this.pbxManteEmpresa.Click += new System.EventHandler(this.pbxManteEmpresa_Click);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(5, 11);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(60, 19);
            this.metroLabel6.TabIndex = 2;
            this.metroLabel6.Text = "Empresa";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.metroLabel29);
            this.metroTabPage3.Controls.Add(this.metroLabel24);
            this.metroTabPage3.Controls.Add(this.metroLabel10);
            this.metroTabPage3.Controls.Add(this.metroLabel16);
            this.metroTabPage3.Controls.Add(this.pictureBox20);
            this.metroTabPage3.Controls.Add(this.pictureBox12);
            this.metroTabPage3.Controls.Add(this.pbxOrdenCompra);
            this.metroTabPage3.Controls.Add(this.pcbxArticulo);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Logística";
            this.metroTabPage3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.metroLabel29.Location = new System.Drawing.Point(211, 9);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(58, 19);
            this.metroLabel29.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel29.TabIndex = 16;
            this.metroLabel29.Text = "Compra";
            this.metroLabel29.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(314, 9);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(84, 19);
            this.metroLabel24.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel24.TabIndex = 14;
            this.metroLabel24.Text = "Transferencia";
            this.metroLabel24.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(91, 7);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(116, 19);
            this.metroLabel10.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel10.TabIndex = 12;
            this.metroLabel10.Text = "Orden de compra";
            this.metroLabel10.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(6, 7);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(60, 19);
            this.metroLabel16.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel16.TabIndex = 10;
            this.metroLabel16.Text = "Almacen";
            this.metroLabel16.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox20.Image = global::Presentacion.Properties.Resources.commerce_1;
            this.pictureBox20.Location = new System.Drawing.Point(211, 37);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(93, 62);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 17;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox12.Image = global::Presentacion.Properties.Resources.logistics_512;
            this.pictureBox12.Location = new System.Drawing.Point(320, 35);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(79, 64);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 15;
            this.pictureBox12.TabStop = false;
            // 
            // pbxOrdenCompra
            // 
            this.pbxOrdenCompra.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxOrdenCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxOrdenCompra.Image = global::Presentacion.Properties.Resources.commerce_1;
            this.pbxOrdenCompra.Location = new System.Drawing.Point(91, 35);
            this.pbxOrdenCompra.Name = "pbxOrdenCompra";
            this.pbxOrdenCompra.Size = new System.Drawing.Size(105, 62);
            this.pbxOrdenCompra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxOrdenCompra.TabIndex = 13;
            this.pbxOrdenCompra.TabStop = false;
            this.pbxOrdenCompra.Click += new System.EventHandler(this.pbxOrdenCompra_Click);
            // 
            // pcbxArticulo
            // 
            this.pcbxArticulo.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pcbxArticulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbxArticulo.Image = global::Presentacion.Properties.Resources.buildings;
            this.pcbxArticulo.Location = new System.Drawing.Point(6, 35);
            this.pcbxArticulo.Name = "pcbxArticulo";
            this.pcbxArticulo.Size = new System.Drawing.Size(79, 63);
            this.pcbxArticulo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbxArticulo.TabIndex = 11;
            this.pcbxArticulo.TabStop = false;
            this.pcbxArticulo.Click += new System.EventHandler(this.pcbxAlmacen_Click);
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.metroLabel17);
            this.metroTabPage5.Controls.Add(this.metroLabel26);
            this.metroTabPage5.Controls.Add(this.metroLabel13);
            this.metroTabPage5.Controls.Add(this.pictureBox5);
            this.metroTabPage5.Controls.Add(this.pictureBox14);
            this.metroTabPage5.Controls.Add(this.pictureBox4);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTabPage5.TabIndex = 7;
            this.metroTabPage5.Text = "RR.HH.";
            this.metroTabPage5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(169, 7);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(65, 19);
            this.metroLabel17.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel17.TabIndex = 23;
            this.metroLabel17.Text = "Asistencia";
            this.metroLabel17.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.Location = new System.Drawing.Point(-1, 7);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(74, 19);
            this.metroLabel26.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel26.TabIndex = 21;
            this.metroLabel26.Text = "Empleados";
            this.metroLabel26.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(84, 7);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(61, 19);
            this.metroLabel13.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel13.TabIndex = 17;
            this.metroLabel13.Text = "Permisos";
            this.metroLabel13.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Location = new System.Drawing.Point(169, 31);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(79, 64);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 24;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox14.Image = global::Presentacion.Properties.Resources.people2;
            this.pictureBox14.Location = new System.Drawing.Point(-1, 31);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(79, 64);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 22;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Location = new System.Drawing.Point(84, 31);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(79, 64);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 18;
            this.pictureBox4.TabStop = false;
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.metroLabel37);
            this.metroTabPage4.Controls.Add(this.metroLabel27);
            this.metroTabPage4.Controls.Add(this.metroLabel20);
            this.metroTabPage4.Controls.Add(this.pictureBox19);
            this.metroTabPage4.Controls.Add(this.pictureBox15);
            this.metroTabPage4.Controls.Add(this.pictureBox8);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTabPage4.TabIndex = 5;
            this.metroTabPage4.Text = "Ventas";
            this.metroTabPage4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // metroLabel37
            // 
            this.metroLabel37.AutoSize = true;
            this.metroLabel37.Location = new System.Drawing.Point(177, 7);
            this.metroLabel37.Name = "metroLabel37";
            this.metroLabel37.Size = new System.Drawing.Size(82, 19);
            this.metroLabel37.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel37.TabIndex = 24;
            this.metroLabel37.Text = "Cotizaciones";
            this.metroLabel37.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.Location = new System.Drawing.Point(91, 7);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(49, 19);
            this.metroLabel27.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel27.TabIndex = 22;
            this.metroLabel27.Text = "Cliente";
            this.metroLabel27.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(6, 7);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(50, 19);
            this.metroLabel20.TabIndex = 10;
            this.metroLabel20.Text = "Vender";
            this.metroLabel20.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox19.Image = global::Presentacion.Properties.Resources.people;
            this.pictureBox19.Location = new System.Drawing.Point(177, 31);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(79, 64);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 25;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox15.Image = global::Presentacion.Properties.Resources.people;
            this.pictureBox15.Location = new System.Drawing.Point(91, 31);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(79, 64);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 23;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox8.Location = new System.Drawing.Point(6, 31);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(79, 64);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 11;
            this.pictureBox8.TabStop = false;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroLabel36);
            this.metroTabPage1.Controls.Add(this.pictureBox18);
            this.metroTabPage1.Controls.Add(this.metroLabel25);
            this.metroTabPage1.Controls.Add(this.metroLabel22);
            this.metroTabPage1.Controls.Add(this.metroLabel23);
            this.metroTabPage1.Controls.Add(this.pictureBox13);
            this.metroTabPage1.Controls.Add(this.pictureBox10);
            this.metroTabPage1.Controls.Add(this.pictureBox11);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage1.TabIndex = 3;
            this.metroTabPage1.Text = "Reportes";
            this.metroTabPage1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroLabel36
            // 
            this.metroLabel36.AutoSize = true;
            this.metroLabel36.Location = new System.Drawing.Point(322, 10);
            this.metroLabel36.Name = "metroLabel36";
            this.metroLabel36.Size = new System.Drawing.Size(65, 19);
            this.metroLabel36.TabIndex = 16;
            this.metroLabel36.Text = "Asistencia";
            this.metroLabel36.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox18.Location = new System.Drawing.Point(322, 34);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(100, 57);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 17;
            this.pictureBox18.TabStop = false;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(216, 10);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(46, 19);
            this.metroLabel25.TabIndex = 14;
            this.metroLabel25.Text = "Ventas";
            this.metroLabel25.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(110, 10);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(40, 19);
            this.metroLabel22.TabIndex = 12;
            this.metroLabel22.Text = "Stock";
            this.metroLabel22.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(4, 10);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(49, 19);
            this.metroLabel23.TabIndex = 10;
            this.metroLabel23.Text = "Kardex";
            this.metroLabel23.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox13.Location = new System.Drawing.Point(216, 34);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(100, 57);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 15;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox10.Location = new System.Drawing.Point(110, 34);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(100, 57);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 13;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox11.Location = new System.Drawing.Point(4, 34);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 57);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 11;
            this.pictureBox11.TabStop = false;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroLabel28);
            this.metroTabPage2.Controls.Add(this.pbxConsulta);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage2.TabIndex = 4;
            this.metroTabPage2.Text = "Programming";
            this.metroTabPage2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.metroLabel28.Location = new System.Drawing.Point(6, 9);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(71, 19);
            this.metroLabel28.TabIndex = 16;
            this.metroLabel28.Text = "Consultas: ";
            this.metroLabel28.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxConsulta
            // 
            this.pbxConsulta.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxConsulta.Location = new System.Drawing.Point(6, 31);
            this.pbxConsulta.Name = "pbxConsulta";
            this.pbxConsulta.Size = new System.Drawing.Size(100, 57);
            this.pbxConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxConsulta.TabIndex = 17;
            this.pbxConsulta.TabStop = false;
            this.pbxConsulta.Click += new System.EventHandler(this.pbxConsulta_Click);
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.metroLabel21);
            this.metroTabPage6.Controls.Add(this.pbxControl);
            this.metroTabPage6.Controls.Add(this.metroLabel18);
            this.metroTabPage6.Controls.Add(this.pbxInfoRed);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage6.TabIndex = 8;
            this.metroTabPage6.Text = "Network";
            this.metroTabPage6.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(136, 11);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(53, 19);
            this.metroLabel21.TabIndex = 20;
            this.metroLabel21.Text = "Control";
            this.metroLabel21.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxControl
            // 
            this.pbxControl.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxControl.Location = new System.Drawing.Point(136, 33);
            this.pbxControl.Name = "pbxControl";
            this.pbxControl.Size = new System.Drawing.Size(127, 57);
            this.pbxControl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxControl.TabIndex = 21;
            this.pbxControl.TabStop = false;
            this.pbxControl.Click += new System.EventHandler(this.pbxControl_Click);
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(3, 11);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(127, 19);
            this.metroLabel18.TabIndex = 18;
            this.metroLabel18.Text = "Información general";
            this.metroLabel18.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pbxInfoRed
            // 
            this.pbxInfoRed.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pbxInfoRed.Location = new System.Drawing.Point(3, 33);
            this.pbxInfoRed.Name = "pbxInfoRed";
            this.pbxInfoRed.Size = new System.Drawing.Size(127, 57);
            this.pbxInfoRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxInfoRed.TabIndex = 19;
            this.pbxInfoRed.TabStop = false;
            this.pbxInfoRed.Click += new System.EventHandler(this.pbxInfoRed_Click);
            // 
            // metroTabPage7
            // 
            this.metroTabPage7.Controls.Add(this.metroLabel31);
            this.metroTabPage7.Controls.Add(this.pictureBox6);
            this.metroTabPage7.Controls.Add(this.metroLabel30);
            this.metroTabPage7.Controls.Add(this.pictureBox3);
            this.metroTabPage7.HorizontalScrollbarBarColor = true;
            this.metroTabPage7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.HorizontalScrollbarSize = 10;
            this.metroTabPage7.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage7.Name = "metroTabPage7";
            this.metroTabPage7.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage7.TabIndex = 9;
            this.metroTabPage7.Text = "Ayuda";
            this.metroTabPage7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage7.VerticalScrollbarBarColor = true;
            this.metroTabPage7.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.VerticalScrollbarSize = 10;
            // 
            // metroLabel31
            // 
            this.metroLabel31.AutoSize = true;
            this.metroLabel31.Location = new System.Drawing.Point(136, 20);
            this.metroLabel31.Name = "metroLabel31";
            this.metroLabel31.Size = new System.Drawing.Size(144, 19);
            this.metroLabel31.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel31.TabIndex = 14;
            this.metroLabel31.Text = "Informacion de licencia";
            this.metroLabel31.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = global::Presentacion.Properties.Resources.buildings;
            this.pictureBox6.Location = new System.Drawing.Point(136, 48);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(79, 63);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 15;
            this.pictureBox6.TabStop = false;
            // 
            // metroLabel30
            // 
            this.metroLabel30.AutoSize = true;
            this.metroLabel30.Location = new System.Drawing.Point(22, 15);
            this.metroLabel30.Name = "metroLabel30";
            this.metroLabel30.Size = new System.Drawing.Size(100, 19);
            this.metroLabel30.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel30.TabIndex = 12;
            this.metroLabel30.Text = "Documentacion";
            this.metroLabel30.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::Presentacion.Properties.Resources.buildings;
            this.pictureBox3.Location = new System.Drawing.Point(22, 43);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(79, 63);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // metroTabPage8
            // 
            this.metroTabPage8.Controls.Add(this.metroLabel35);
            this.metroTabPage8.Controls.Add(this.pictureBox17);
            this.metroTabPage8.Controls.Add(this.metroLabel34);
            this.metroTabPage8.Controls.Add(this.pictureBox16);
            this.metroTabPage8.Controls.Add(this.metroLabel33);
            this.metroTabPage8.Controls.Add(this.pictureBox9);
            this.metroTabPage8.Controls.Add(this.metroLabel32);
            this.metroTabPage8.Controls.Add(this.pictureBox7);
            this.metroTabPage8.HorizontalScrollbarBarColor = true;
            this.metroTabPage8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.HorizontalScrollbarSize = 10;
            this.metroTabPage8.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage8.Name = "metroTabPage8";
            this.metroTabPage8.Size = new System.Drawing.Size(1282, 111);
            this.metroTabPage8.TabIndex = 10;
            this.metroTabPage8.Text = "Plantilla de correos";
            this.metroTabPage8.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage8.VerticalScrollbarBarColor = true;
            this.metroTabPage8.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.VerticalScrollbarSize = 10;
            // 
            // metroLabel35
            // 
            this.metroLabel35.AutoSize = true;
            this.metroLabel35.Location = new System.Drawing.Point(343, 20);
            this.metroLabel35.Name = "metroLabel35";
            this.metroLabel35.Size = new System.Drawing.Size(92, 19);
            this.metroLabel35.TabIndex = 24;
            this.metroLabel35.Text = "Activar correo";
            this.metroLabel35.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox17.Location = new System.Drawing.Point(343, 42);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(100, 57);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 25;
            this.pictureBox17.TabStop = false;
            // 
            // metroLabel34
            // 
            this.metroLabel34.AutoSize = true;
            this.metroLabel34.Location = new System.Drawing.Point(237, 20);
            this.metroLabel34.Name = "metroLabel34";
            this.metroLabel34.Size = new System.Drawing.Size(86, 19);
            this.metroLabel34.TabIndex = 22;
            this.metroLabel34.Text = "Promociones";
            this.metroLabel34.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox16.Location = new System.Drawing.Point(237, 42);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(100, 57);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 23;
            this.pictureBox16.TabStop = false;
            // 
            // metroLabel33
            // 
            this.metroLabel33.AutoSize = true;
            this.metroLabel33.Location = new System.Drawing.Point(105, 20);
            this.metroLabel33.Name = "metroLabel33";
            this.metroLabel33.Size = new System.Drawing.Size(128, 19);
            this.metroLabel33.TabIndex = 20;
            this.metroLabel33.Text = "Ordenes de compra";
            this.metroLabel33.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox9.Location = new System.Drawing.Point(115, 42);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 57);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 21;
            this.pictureBox9.TabStop = false;
            // 
            // metroLabel32
            // 
            this.metroLabel32.AutoSize = true;
            this.metroLabel32.Location = new System.Drawing.Point(4, 20);
            this.metroLabel32.Name = "metroLabel32";
            this.metroLabel32.Size = new System.Drawing.Size(82, 19);
            this.metroLabel32.TabIndex = 18;
            this.metroLabel32.Text = "Cotizaciones";
            this.metroLabel32.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.pictureBox7.Location = new System.Drawing.Point(4, 42);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 57);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 19;
            this.pictureBox7.TabStop = false;
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblHora.Location = new System.Drawing.Point(359, 19);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(46, 19);
            this.lblHora.TabIndex = 17;
            this.lblHora.Text = "HORA";
            this.lblHora.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblHora.UseCustomForeColor = true;
            // 
            // metroPanel4
            // 
            this.metroPanel4.Controls.Add(this.btnBuscarProveedor);
            this.metroPanel4.Controls.Add(this.metroLabel40);
            this.metroPanel4.Controls.Add(this.metroLabel14);
            this.metroPanel4.Controls.Add(this.metroLabel9);
            this.metroPanel4.Controls.Add(this.metroLabel7);
            this.metroPanel4.Controls.Add(this.metroLabel2);
            this.metroPanel4.Controls.Add(this.metroTextBox3);
            this.metroPanel4.Controls.Add(this.metroLabel1);
            this.metroPanel4.Controls.Add(this.metroTextBox2);
            this.metroPanel4.Controls.Add(this.metroTextBox1);
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(24, 244);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(1048, 65);
            this.metroPanel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroPanel4.TabIndex = 18;
            this.metroPanel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // btnBuscarProveedor
            // 
            this.btnBuscarProveedor.Location = new System.Drawing.Point(949, 25);
            this.btnBuscarProveedor.Name = "btnBuscarProveedor";
            this.btnBuscarProveedor.Size = new System.Drawing.Size(27, 23);
            this.btnBuscarProveedor.Style = MetroFramework.MetroColorStyle.Green;
            this.btnBuscarProveedor.TabIndex = 70;
            this.btnBuscarProveedor.Text = "...";
            this.btnBuscarProveedor.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBuscarProveedor.UseSelectable = true;
            this.btnBuscarProveedor.UseStyleColors = true;
            // 
            // metroLabel40
            // 
            this.metroLabel40.AutoSize = true;
            this.metroLabel40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.metroLabel40.Location = new System.Drawing.Point(876, 25);
            this.metroLabel40.Name = "metroLabel40";
            this.metroLabel40.Size = new System.Drawing.Size(51, 19);
            this.metroLabel40.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel40.TabIndex = 19;
            this.metroLabel40.Text = "_______";
            this.metroLabel40.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel40.UseStyleColors = true;
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.metroLabel14.Location = new System.Drawing.Point(805, 25);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(65, 19);
            this.metroLabel14.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel14.TabIndex = 18;
            this.metroLabel14.Text = "Moneda: ";
            this.metroLabel14.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel14.UseStyleColors = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.metroLabel9.Location = new System.Drawing.Point(540, 25);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(65, 19);
            this.metroLabel9.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel9.TabIndex = 17;
            this.metroLabel9.Text = "Este año: ";
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel9.UseStyleColors = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.metroLabel7.Location = new System.Drawing.Point(295, 25);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(67, 19);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel7.TabIndex = 17;
            this.metroLabel7.Text = "Este mes: ";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel7.UseStyleColors = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.metroLabel2.Location = new System.Drawing.Point(80, 25);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(35, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 17;
            this.metroLabel2.Text = "Hoy:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // metroTextBox3
            // 
            // 
            // 
            // 
            this.metroTextBox3.CustomButton.Image = null;
            this.metroTextBox3.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.metroTextBox3.CustomButton.Name = "";
            this.metroTextBox3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox3.CustomButton.TabIndex = 1;
            this.metroTextBox3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox3.CustomButton.UseSelectable = true;
            this.metroTextBox3.CustomButton.Visible = false;
            this.metroTextBox3.Enabled = false;
            this.metroTextBox3.Lines = new string[] {
        "3553.83 USD"};
            this.metroTextBox3.Location = new System.Drawing.Point(646, 25);
            this.metroTextBox3.MaxLength = 32767;
            this.metroTextBox3.Name = "metroTextBox3";
            this.metroTextBox3.PasswordChar = '\0';
            this.metroTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox3.SelectedText = "";
            this.metroTextBox3.SelectionLength = 0;
            this.metroTextBox3.SelectionStart = 0;
            this.metroTextBox3.Size = new System.Drawing.Size(137, 23);
            this.metroTextBox3.Style = MetroFramework.MetroColorStyle.Red;
            this.metroTextBox3.TabIndex = 2;
            this.metroTextBox3.Text = "3553.83 USD";
            this.metroTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox3.UseSelectable = true;
            this.metroTextBox3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.metroLabel1.Location = new System.Drawing.Point(17, 25);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(57, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Ingresos";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // metroTextBox2
            // 
            // 
            // 
            // 
            this.metroTextBox2.CustomButton.Image = null;
            this.metroTextBox2.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.metroTextBox2.CustomButton.Name = "";
            this.metroTextBox2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox2.CustomButton.TabIndex = 1;
            this.metroTextBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox2.CustomButton.UseSelectable = true;
            this.metroTextBox2.CustomButton.Visible = false;
            this.metroTextBox2.Enabled = false;
            this.metroTextBox2.Lines = new string[] {
        "436.40 USD"};
            this.metroTextBox2.Location = new System.Drawing.Point(368, 25);
            this.metroTextBox2.MaxLength = 32767;
            this.metroTextBox2.Name = "metroTextBox2";
            this.metroTextBox2.PasswordChar = '\0';
            this.metroTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox2.SelectedText = "";
            this.metroTextBox2.SelectionLength = 0;
            this.metroTextBox2.SelectionStart = 0;
            this.metroTextBox2.Size = new System.Drawing.Size(137, 23);
            this.metroTextBox2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTextBox2.TabIndex = 2;
            this.metroTextBox2.Text = "436.40 USD";
            this.metroTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox2.UseSelectable = true;
            this.metroTextBox2.UseStyleColors = true;
            this.metroTextBox2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            this.metroTextBox1.CustomButton.Image = null;
            this.metroTextBox1.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.metroTextBox1.CustomButton.Name = "";
            this.metroTextBox1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox1.CustomButton.TabIndex = 1;
            this.metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox1.CustomButton.UseSelectable = true;
            this.metroTextBox1.CustomButton.Visible = false;
            this.metroTextBox1.Enabled = false;
            this.metroTextBox1.Lines = new string[] {
        "0.00 USD"};
            this.metroTextBox1.Location = new System.Drawing.Point(121, 25);
            this.metroTextBox1.MaxLength = 32767;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.SelectionLength = 0;
            this.metroTextBox1.SelectionStart = 0;
            this.metroTextBox1.Size = new System.Drawing.Size(137, 23);
            this.metroTextBox1.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroTextBox1.TabIndex = 2;
            this.metroTextBox1.Text = "0.00 USD";
            this.metroTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.UseStyleColors = true;
            this.metroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.AutoSize = true;
            this.lblTipoCambio.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblTipoCambio.Location = new System.Drawing.Point(185, 19);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(102, 19);
            this.lblTipoCambio.TabIndex = 17;
            this.lblTipoCambio.Text = "Tipo de cambio";
            this.lblTipoCambio.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblTipoCambio.UseCustomForeColor = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // metroPanel5
            // 
            this.metroPanel5.Controls.Add(this.pbxFb);
            this.metroPanel5.Controls.Add(this.pbxWeb);
            this.metroPanel5.Controls.Add(this.metroLabel8);
            this.metroPanel5.Controls.Add(this.pbxYoutube);
            this.metroPanel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(20, 730);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(1290, 57);
            this.metroPanel5.TabIndex = 19;
            this.metroPanel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // pbxFb
            // 
            this.pbxFb.BackColor = System.Drawing.Color.White;
            this.pbxFb.Image = global::Presentacion.Properties.Resources.social_media;
            this.pbxFb.Location = new System.Drawing.Point(106, 10);
            this.pbxFb.Name = "pbxFb";
            this.pbxFb.Size = new System.Drawing.Size(32, 32);
            this.pbxFb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxFb.TabIndex = 5;
            this.pbxFb.TabStop = false;
            this.pbxFb.Click += new System.EventHandler(this.pbxFb_Click);
            // 
            // pbxWeb
            // 
            this.pbxWeb.BackColor = System.Drawing.Color.Black;
            this.pbxWeb.Image = global::Presentacion.Properties.Resources.Internet_Explorer_White;
            this.pbxWeb.Location = new System.Drawing.Point(57, 10);
            this.pbxWeb.Name = "pbxWeb";
            this.pbxWeb.Size = new System.Drawing.Size(32, 32);
            this.pbxWeb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxWeb.TabIndex = 4;
            this.pbxWeb.TabStop = false;
            this.pbxWeb.Click += new System.EventHandler(this.pbxWeb_Click);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(553, 21);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(181, 19);
            this.metroLabel8.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel8.TabIndex = 3;
            this.metroLabel8.Text = "PERULATINOHOSTING S.A .C";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel8.UseStyleColors = true;
            // 
            // pbxYoutube
            // 
            this.pbxYoutube.BackColor = System.Drawing.Color.Transparent;
            this.pbxYoutube.Image = global::Presentacion.Properties.Resources.youtube;
            this.pbxYoutube.Location = new System.Drawing.Point(10, 10);
            this.pbxYoutube.Name = "pbxYoutube";
            this.pbxYoutube.Size = new System.Drawing.Size(32, 32);
            this.pbxYoutube.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxYoutube.TabIndex = 2;
            this.pbxYoutube.TabStop = false;
            this.pbxYoutube.Click += new System.EventHandler(this.pbxYoutube_Click);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pbxOpciones
            // 
            this.pbxOpciones.Image = global::Presentacion.Properties.Resources.commerce_1;
            this.pbxOpciones.Location = new System.Drawing.Point(1273, 22);
            this.pbxOpciones.Name = "pbxOpciones";
            this.pbxOpciones.Size = new System.Drawing.Size(36, 35);
            this.pbxOpciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxOpciones.TabIndex = 14;
            this.pbxOpciones.TabStop = false;
            this.pbxOpciones.Click += new System.EventHandler(this.pbxOpciones_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Presentacion.Properties.Resources.Invitado;
            this.pictureBox1.Location = new System.Drawing.Point(973, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(82, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(1330, 807);
            this.Controls.Add(this.metroPanel5);
            this.Controls.Add(this.metroPanel4);
            this.Controls.Add(this.lblTipoCambio);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.metroTabControl1);
            this.Controls.Add(this.pbxOpciones);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblTipoUsuario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmPrincipal";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "INICIO";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactura)).EndInit();
            this.metroTabControl1.ResumeLayout(false);
            this.mtpMantenimiento.ResumeLayout(false);
            this.mtpMantenimiento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFamilia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxComprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbxUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbxSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxManteEmpresa)).EndInit();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxOrdenCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbxArticulo)).EndInit();
            this.metroTabPage5.ResumeLayout(false);
            this.metroTabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxConsulta)).EndInit();
            this.metroTabPage6.ResumeLayout(false);
            this.metroTabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxInfoRed)).EndInit();
            this.metroTabPage7.ResumeLayout(false);
            this.metroTabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.metroTabPage8.ResumeLayout(false);
            this.metroTabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.metroPanel4.ResumeLayout(false);
            this.metroPanel4.PerformLayout();
            this.metroPanel5.ResumeLayout(false);
            this.metroPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxWeb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxYoutube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxOpciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel lblTipoUsuario;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel lblUsuario;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroTile metroTile4;
        private MetroFramework.Controls.MetroTile metroTile3;
        private MetroFramework.Controls.MetroTile metroTile2;
        private MetroFramework.Controls.MetroTile metroTile5;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroTile metroTile7;
        private MetroFramework.Controls.MetroTile metroTile6;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblFecha;
        private System.Windows.Forms.PictureBox pbxOpciones;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage mtpMantenimiento;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroLabel lblHora;
        private MetroFramework.Controls.MetroGrid dgvFactura;
        private MetroFramework.Controls.MetroComboBox cboSucursal;
        private MetroFramework.Controls.MetroComboBox cboEmpresa;
        private MetroFramework.Controls.MetroLink metroLink2;
        private MetroFramework.Controls.MetroLink metroLink1;
        private MetroFramework.Controls.MetroPanel metroPanel4;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroLabel lblTipoCambio;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox metroTextBox3;
        private MetroFramework.Controls.MetroTextBox metroTextBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.PictureBox pbxManteEmpresa;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.Timer timer1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroPanel metroPanel5;
        private System.Windows.Forms.PictureBox pbxYoutube;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private System.Windows.Forms.PictureBox pbxWeb;
        private System.Windows.Forms.PictureBox pbxFb;
        private System.Windows.Forms.PictureBox pcbxSucursal;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private System.Windows.Forms.PictureBox pcbxUnidad;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private System.Windows.Forms.PictureBox pbxProveedor;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private System.Windows.Forms.PictureBox pbxComprobantes;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private System.Windows.Forms.PictureBox pictureBox13;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private System.Windows.Forms.PictureBox pictureBox10;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private System.Windows.Forms.PictureBox pictureBox11;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private System.Windows.Forms.PictureBox pcbxArticulo;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private System.Windows.Forms.PictureBox pictureBox12;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private System.Windows.Forms.PictureBox pbxOrdenCompra;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private System.Windows.Forms.PictureBox pictureBox8;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroButton btnCambiarSucursal;
        private MetroFramework.Controls.MetroTile metroTile9;
        private MetroFramework.Controls.MetroTile metroTile8;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private System.Windows.Forms.PictureBox pbxConsulta;
        private System.Windows.Forms.PictureBox pictureBox15;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private System.Windows.Forms.PictureBox pbxInfoRed;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private System.Windows.Forms.PictureBox pbxControl;
        private MetroFramework.Controls.MetroTabPage metroTabPage7;
        private MetroFramework.Controls.MetroLabel metroLabel31;
        private System.Windows.Forms.PictureBox pictureBox6;
        private MetroFramework.Controls.MetroLabel metroLabel30;
        private System.Windows.Forms.PictureBox pictureBox3;
        private MetroFramework.Controls.MetroTabPage metroTabPage8;
        private MetroFramework.Controls.MetroLabel metroLabel35;
        private System.Windows.Forms.PictureBox pictureBox17;
        private MetroFramework.Controls.MetroLabel metroLabel34;
        private System.Windows.Forms.PictureBox pictureBox16;
        private MetroFramework.Controls.MetroLabel metroLabel33;
        private System.Windows.Forms.PictureBox pictureBox9;
        private MetroFramework.Controls.MetroLabel metroLabel32;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Timer timer2;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private System.Windows.Forms.PictureBox pictureBox5;
        private MetroFramework.Controls.MetroLabel metroLabel36;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private MetroFramework.Controls.MetroLabel metroLabel37;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pbxFamilia;
        private MetroFramework.Controls.MetroLabel metroLabel38;
        private System.Windows.Forms.PictureBox pbxMoneda;
        private MetroFramework.Controls.MetroLabel metroLabel39;
        private MetroFramework.Controls.MetroLabel metroLabel40;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroButton btnBuscarProveedor;
    }
}