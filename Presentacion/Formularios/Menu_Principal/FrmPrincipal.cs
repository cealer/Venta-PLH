﻿using MetroFramework;
using MetroFramework.Forms;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using Entidades;
using Presentacion.Formularios.Network;
using System.Linq;

namespace Presentacion.Formularios.Menu_Principal
{
    public partial class FrmPrincipal : MetroForm
    {
        public Entidades.Empresa empresa { get; set; }

        public Entidades.Usuario usuario = new Entidades.Usuario();
        public Entidades.PermisoSucursales permisosSucursales = new Entidades.PermisoSucursales();

        //private readonly TcpClient cliente = new TcpClient();
        //private NetworkStream mainStream;
        //private int puerto;

        public FrmPrincipal()
        {
            InitializeComponent();
        }


        void setUsuario()
        {
            lblUsuario.Text = usuario._Usuario;
            lblTipoUsuario.Text = usuario.tipoUsuario.Descripcion;
        }

        void getFecha()
        {
            lblFecha.Text = DateTime.Today.ToShortDateString();
        }

        //AYUDA-->INFORMACION DE LICENCIA 1,2(Módulo(s) registrado(s)),Soporte(Si,no),Ocultar Copyright,Addon:Cosas nuevas(Sistema Limpio),version,
        //Mantenimiento de Modulos(claves)
        //Solicitar soportes ----> Crear tickets Contactar nombre --> Enviar al correo de PLH
        //Base de conocimientos 

        List<PermisoSucursales> lista = new List<PermisoSucursales>();

        void GetPermisoSucursales()
        {
            permisosSucursales.usuario = new Entidades.Usuario();
            permisosSucursales.usuario.Id = usuario.Id;

            lista = PermisoSucursalesBOL.Buscar(permisosSucursales);

            List<Entidades.Empresa> EmpresaCombo = new List<Entidades.Empresa>();

            foreach (var item in lista)
            {
                var repetido = EmpresaCombo.Where(x => x.Id == item.empresa.Id).Count();
                if (repetido == 0)
                {
                    EmpresaCombo.Add(item.empresa);
                }
            }

            cboEmpresa.DataSource = EmpresaCombo;
            cboEmpresa.DisplayMember = "NombreAbreviado";
            cboEmpresa.ValueMember = "Id";

            List<Sucursal> SucursalCombo = new List<Sucursal>();

            foreach (var item in lista)
            {
                if (item.empresa.Id.Equals(cboEmpresa.SelectedValue.ToString()))
                {
                    SucursalCombo.Add(item.sucursal);
                }
            }

            cboSucursal.DataSource = SucursalCombo;
            cboSucursal.DisplayMember = "Descripcion";
            cboSucursal.ValueMember = "Id";
        }

        //private static Image GrabarEscritorio()
        //{
        //    Rectangle bounds = Screen.PrimaryScreen.Bounds;
        //    Bitmap screeshot = new Bitmap(bounds.Width, bounds.Height, PixelFormat.Format32bppArgb);
        //    Graphics graphics = Graphics.FromImage(screeshot);
        //    graphics.CopyFromScreen(bounds.X, bounds.Y, 0, 0, bounds.Size, CopyPixelOperation.SourceCopy);
        //    return screeshot;
        //}

        //private void SendDesktopImage()
        //{
        //    BinaryFormatter binFormatter = new BinaryFormatter();
        //    mainStream = cliente.GetStream();
        //    binFormatter.Serialize(mainStream, GrabarEscritorio());
        //}

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            //   getTipoCambio();
            LlenarData();
            setUsuario();
            GetPermisoSucursales();
            //if (usuario.tipoUsuario.Descripcion != "ADMINISTRADOR")
            //{
            //    puerto = 2015;
            //    try
            //    {
            //        cliente.Connect("192.168.1.45", puerto);
            //        MessageBox.Show("Conectado");
            //        timer2.Start();
            //    }
            //    catch (Exception)
            //    {
            //        return;
            //    }
            //}
        }

        private void getTipoCambio()
        {
            int mes, anio;
            mes = DateTime.Today.Month;
            anio = DateTime.Today.Year;
            var aux = new TipoCambio.Operaciones();
            var cambio = "Tipo de cambio: " + aux.ObtenerUltimoVenta(mes, anio).ToString();
            // 3 digitos
            if (cambio.Length <= 2)
            {
                cambio = "Tipo de cambio: " + aux.ObtenerUltimoAnteriorVenta(mes, anio).ToString();
            }

            lblTipoCambio.Text = cambio;
        }

        private void LlenarData()
        {
            dgvFactura.Rows.Add("92", "PerulatinoHosting", "20/02/2016", "05/03/2016", "$12.85 USD", "Deposito/Transferencia");
            dgvFactura.Rows.Add("92", "PerulatinoHosting", "20/02/2016", "05/03/2016", "$12.85 USD", "Deposito/Transferencia");
            dgvFactura.Rows.Add("92", "PerulatinoHosting", "20/02/2016", "05/03/2016", "$12.85 USD", "Deposito/Transferencia");
            dgvFactura.Rows.Add("92", "PerulatinoHosting", "20/02/2016", "05/03/2016", "$12.85 USD", "Deposito/Transferencia");
            dgvFactura.Rows.Add("92", "PerulatinoHosting", "20/02/2016", "05/03/2016", "$12.85 USD", "Deposito/Transferencia");
        }

        private void pbxOpciones_Click(object sender, EventArgs e)
        {
            var aux = new Login.FrmCargaUsuario();
            var r = MetroMessageBox.Show(this, "¿Está seguro que desea cerrar la sesión actual?.", "Cerrar sesión", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r.Equals(DialogResult.Yes))
            {
                this.Dispose();
                aux.Show();
            }
        }

        private void metlinkSalir_Click(object sender, EventArgs e)
        {
            MetroMessageBox.Show(this, $"Bienvenido {usuario._Usuario}", "Incio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Question, 100);
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            getFecha();
            getHora();
        }

        private void getHora()
        {
            lblHora.Text = DateTime.Now.ToShortTimeString();
        }

        private void pbxYoutube_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/user/perulatinohosting");
        }

        private void pbxWeb_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://perulatinohosting.com/");
        }

        private void pbxFb_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/perulatinohosting/");
        }

        private void pbxManteEmpresa_Click(object sender, EventArgs e)
        {
            var empresa = new Empresa.FrmEmpresa();
            empresa.ShowDialog();
        }

        private void pcbxSucursal_Click(object sender, EventArgs e)
        {
            var aux = new Mantenimiento.FrmSucursal();
            aux.ShowDialog();
        }

        private void pcbxFamilia_Click(object sender, EventArgs e)
        {
            var aux = new Mantenimiento.FrmTipoMedio();
            aux.ShowDialog();
        }

        private void pcbxUnidad_Click(object sender, EventArgs e)
        {
            var aux = new Mantenimiento.FrmUnidad();
            aux.ShowDialog();
        }

        private void pcbxAlmacen_Click(object sender, EventArgs e)
        {
            var aux = new Mantenimiento.FrmArticulo();
            aux.IdEmpresa = cboEmpresa.SelectedValue.ToString();
            aux.IdSucursal = cboSucursal.SelectedValue.ToString();
            aux.IdUsuario = usuario.Id;
            aux.ShowDialog();
        }

        private void btnCambiarSucursal_Click(object sender, EventArgs e)
        {

        }

        private void mtpMantenimiento_Click(object sender, EventArgs e)
        {

        }

        private void pbxArticulo_Click(object sender, EventArgs e)
        {
            var aux = new Logistica.FrmCotizacion();
            aux.ShowDialog();
        }

        private void pbxConsulta_Click(object sender, EventArgs e)
        {
            var aux = new Reportes.FrmReportes();
            aux.ShowDialog();
        }

        private void pbxProveedor_Click(object sender, EventArgs e)
        {
            var aux = new Mantenimiento.FrmProveedor();
            aux.usuario = usuario;
            aux.ShowDialog();
        }

        private void pbxInfoRed_Click(object sender, EventArgs e)
        {
            var aux = new Network.FrmInfoNetwork();
            aux.ShowDialog();
        }

        private void pbxControl_Click(object sender, EventArgs e)
        {
            FrmControl aux = new FrmControl();
            aux.ShowDialog();
        }

        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer2.Stop();
            Application.Exit();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    SendDesktopImage();
            //}
            //catch (Exception)
            //{
            //    return;
            //}
        }

        private void pbxOrdenCompra_Click(object sender, EventArgs e)
        {
            Logistica.FrmCotizacion aux = new Logistica.FrmCotizacion();
            aux.Empresa = cboEmpresa.SelectedValue.ToString();
            aux.Sucursal = cboSucursal.SelectedValue.ToString();
            aux.Usuario = usuario.Id;
            aux.ShowDialog();
        }

        private void pbxFamilia_Click(object sender, EventArgs e)
        {
            Mantenimiento.FrmFamilia aux = new Mantenimiento.FrmFamilia();
            aux.ShowDialog();
        }

        private void cboEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Sucursal> SucursalCombo = new List<Sucursal>();

            foreach (var item in lista)
            {
                if (item.empresa.Id.Equals(cboEmpresa.SelectedValue.ToString()))
                {
                    SucursalCombo.Add(item.sucursal);
                }
            }

            cboSucursal.DataSource = SucursalCombo;
            cboSucursal.DisplayMember = "Descripcion";
            cboSucursal.ValueMember = "Id";
        }

        private void pbxMoneda_Click(object sender, EventArgs e)
        {
            Mantenimiento.FrmMoneda aux = new Mantenimiento.FrmMoneda();
            aux.ShowDialog();
        }
    }
}