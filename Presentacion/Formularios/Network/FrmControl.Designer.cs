﻿namespace Presentacion.Formularios.Network
{
    partial class FrmControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxPuerto = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.btnConexion = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // tbxPuerto
            // 
            // 
            // 
            // 
            this.tbxPuerto.CustomButton.Image = null;
            this.tbxPuerto.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.tbxPuerto.CustomButton.Name = "";
            this.tbxPuerto.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxPuerto.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxPuerto.CustomButton.TabIndex = 1;
            this.tbxPuerto.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxPuerto.CustomButton.UseSelectable = true;
            this.tbxPuerto.CustomButton.Visible = false;
            this.tbxPuerto.Lines = new string[0];
            this.tbxPuerto.Location = new System.Drawing.Point(103, 91);
            this.tbxPuerto.MaxLength = 32767;
            this.tbxPuerto.Name = "tbxPuerto";
            this.tbxPuerto.PasswordChar = '\0';
            this.tbxPuerto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPuerto.SelectedText = "";
            this.tbxPuerto.SelectionLength = 0;
            this.tbxPuerto.SelectionStart = 0;
            this.tbxPuerto.Size = new System.Drawing.Size(139, 23);
            this.tbxPuerto.TabIndex = 48;
            this.tbxPuerto.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxPuerto.UseSelectable = true;
            this.tbxPuerto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxPuerto.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(32, 91);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(49, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 47;
            this.metroLabel2.Text = "Puerto";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // btnConexion
            // 
            this.btnConexion.Location = new System.Drawing.Point(303, 86);
            this.btnConexion.Name = "btnConexion";
            this.btnConexion.Size = new System.Drawing.Size(86, 37);
            this.btnConexion.Style = MetroFramework.MetroColorStyle.Green;
            this.btnConexion.TabIndex = 46;
            this.btnConexion.Text = "Listen";
            this.btnConexion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnConexion.UseSelectable = true;
            this.btnConexion.UseStyleColors = true;
            this.btnConexion.Click += new System.EventHandler(this.btnConexion_Click);
            // 
            // FrmControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 174);
            this.Controls.Add(this.tbxPuerto);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.btnConexion);
            this.Name = "FrmControl";
            this.Text = "FrmControl";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tbxPuerto;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroButton btnConexion;
    }
}