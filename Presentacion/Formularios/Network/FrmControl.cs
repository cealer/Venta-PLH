﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Presentacion.Formularios.Network
{
    public partial class FrmControl : MetroForm
    {

        public FrmControl()
        {
            InitializeComponent();
        }

        private void btnConexion_Click(object sender, EventArgs e)
        {
            int puerto = int.Parse(tbxPuerto.Text);
            new FrmServidor(puerto).Show();
        }
    }
}