﻿namespace Presentacion.Formularios.Network
{
    partial class FrmInfoNetwork
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvListaRedes = new MetroFramework.Controls.MetroGrid();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.dgvDetalles = new MetroFramework.Controls.MetroGrid();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnConexion = new MetroFramework.Controls.MetroButton();
            this.lblIP = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbxPuerto = new MetroFramework.Controls.MetroTextBox();
            this.btnCompartir = new MetroFramework.Controls.MetroButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.tbxIp = new MetroFramework.Controls.MetroTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaRedes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvListaRedes
            // 
            this.dgvListaRedes.AllowUserToResizeRows = false;
            this.dgvListaRedes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvListaRedes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvListaRedes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvListaRedes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListaRedes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle55;
            this.dgvListaRedes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListaRedes.DefaultCellStyle = dataGridViewCellStyle56;
            this.dgvListaRedes.EnableHeadersVisualStyles = false;
            this.dgvListaRedes.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvListaRedes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvListaRedes.Location = new System.Drawing.Point(38, 163);
            this.dgvListaRedes.Name = "dgvListaRedes";
            this.dgvListaRedes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle57.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle57.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle57.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle57.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListaRedes.RowHeadersDefaultCellStyle = dataGridViewCellStyle57;
            this.dgvListaRedes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvListaRedes.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvListaRedes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListaRedes.Size = new System.Drawing.Size(788, 139);
            this.dgvListaRedes.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvListaRedes.TabIndex = 21;
            this.dgvListaRedes.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvListaRedes.UseCustomForeColor = true;
            this.dgvListaRedes.SelectionChanged += new System.EventHandler(this.dgvListaRedes_SelectionChanged);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(38, 126);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(134, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 36;
            this.metroLabel3.Text = "Interfaces detectadas:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            // 
            // dgvDetalles
            // 
            this.dgvDetalles.AllowUserToResizeRows = false;
            this.dgvDetalles.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvDetalles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDetalles.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvDetalles.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetalles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle58;
            this.dgvDetalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle59.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle59.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle59.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle59.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetalles.DefaultCellStyle = dataGridViewCellStyle59;
            this.dgvDetalles.EnableHeadersVisualStyles = false;
            this.dgvDetalles.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvDetalles.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvDetalles.Location = new System.Drawing.Point(38, 357);
            this.dgvDetalles.Name = "dgvDetalles";
            this.dgvDetalles.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle60.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle60.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle60.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetalles.RowHeadersDefaultCellStyle = dataGridViewCellStyle60;
            this.dgvDetalles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvDetalles.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvDetalles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalles.Size = new System.Drawing.Size(788, 139);
            this.dgvDetalles.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvDetalles.TabIndex = 37;
            this.dgvDetalles.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvDetalles.UseCustomForeColor = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(38, 320);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(61, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 38;
            this.metroLabel1.Text = "Detalles: ";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // btnConexion
            // 
            this.btnConexion.Location = new System.Drawing.Point(526, 529);
            this.btnConexion.Name = "btnConexion";
            this.btnConexion.Size = new System.Drawing.Size(86, 37);
            this.btnConexion.Style = MetroFramework.MetroColorStyle.Green;
            this.btnConexion.TabIndex = 40;
            this.btnConexion.Text = "Connect";
            this.btnConexion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnConexion.UseSelectable = true;
            this.btnConexion.UseStyleColors = true;
            this.btnConexion.Click += new System.EventHandler(this.btnConexion_Click);
            // 
            // lblIP
            // 
            this.lblIP.AutoSize = true;
            this.lblIP.Location = new System.Drawing.Point(38, 78);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(59, 19);
            this.lblIP.Style = MetroFramework.MetroColorStyle.Green;
            this.lblIP.TabIndex = 41;
            this.lblIP.Text = "Mi IPv4: ";
            this.lblIP.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblIP.UseStyleColors = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(255, 534);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(49, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 42;
            this.metroLabel2.Text = "Puerto";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // tbxPuerto
            // 
            // 
            // 
            // 
            this.tbxPuerto.CustomButton.Image = null;
            this.tbxPuerto.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.tbxPuerto.CustomButton.Name = "";
            this.tbxPuerto.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxPuerto.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxPuerto.CustomButton.TabIndex = 1;
            this.tbxPuerto.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxPuerto.CustomButton.UseSelectable = true;
            this.tbxPuerto.CustomButton.Visible = false;
            this.tbxPuerto.Lines = new string[0];
            this.tbxPuerto.Location = new System.Drawing.Point(326, 534);
            this.tbxPuerto.MaxLength = 32767;
            this.tbxPuerto.Name = "tbxPuerto";
            this.tbxPuerto.PasswordChar = '\0';
            this.tbxPuerto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPuerto.SelectedText = "";
            this.tbxPuerto.SelectionLength = 0;
            this.tbxPuerto.SelectionStart = 0;
            this.tbxPuerto.Size = new System.Drawing.Size(139, 23);
            this.tbxPuerto.TabIndex = 45;
            this.tbxPuerto.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxPuerto.UseSelectable = true;
            this.tbxPuerto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxPuerto.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnCompartir
            // 
            this.btnCompartir.Location = new System.Drawing.Point(652, 529);
            this.btnCompartir.Name = "btnCompartir";
            this.btnCompartir.Size = new System.Drawing.Size(117, 37);
            this.btnCompartir.Style = MetroFramework.MetroColorStyle.Green;
            this.btnCompartir.TabIndex = 46;
            this.btnCompartir.Text = "Share";
            this.btnCompartir.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCompartir.UseSelectable = true;
            this.btnCompartir.UseStyleColors = true;
            this.btnCompartir.Click += new System.EventHandler(this.btnCompartir_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(38, 534);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(27, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 43;
            this.metroLabel4.Text = "Ip: ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // tbxIp
            // 
            // 
            // 
            // 
            this.tbxIp.CustomButton.Image = null;
            this.tbxIp.CustomButton.Location = new System.Drawing.Point(111, 1);
            this.tbxIp.CustomButton.Name = "";
            this.tbxIp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxIp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxIp.CustomButton.TabIndex = 1;
            this.tbxIp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxIp.CustomButton.UseSelectable = true;
            this.tbxIp.CustomButton.Visible = false;
            this.tbxIp.Lines = new string[0];
            this.tbxIp.Location = new System.Drawing.Point(80, 534);
            this.tbxIp.MaxLength = 32767;
            this.tbxIp.Name = "tbxIp";
            this.tbxIp.PasswordChar = '\0';
            this.tbxIp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxIp.SelectedText = "";
            this.tbxIp.SelectionLength = 0;
            this.tbxIp.SelectionStart = 0;
            this.tbxIp.Size = new System.Drawing.Size(133, 23);
            this.tbxIp.TabIndex = 44;
            this.tbxIp.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxIp.UseSelectable = true;
            this.tbxIp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxIp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // FrmInfoNetwork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 589);
            this.Controls.Add(this.btnCompartir);
            this.Controls.Add(this.tbxPuerto);
            this.Controls.Add(this.tbxIp);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.lblIP);
            this.Controls.Add(this.btnConexion);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dgvDetalles);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.dgvListaRedes);
            this.Name = "FrmInfoNetwork";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Información general de la Red";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmInfoNetwork_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaRedes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid dgvListaRedes;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroGrid dgvDetalles;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton btnConexion;
        private MetroFramework.Controls.MetroLabel lblIP;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox tbxPuerto;
        private MetroFramework.Controls.MetroButton btnCompartir;
        private System.Windows.Forms.Timer timer1;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox tbxIp;
    }
}