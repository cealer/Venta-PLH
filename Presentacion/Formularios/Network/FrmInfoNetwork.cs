﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using Entidades.Network;

namespace Presentacion.Formularios.Network
{
    public partial class FrmInfoNetwork : MetroForm
    {

        List<Red> listaRedes = new List<Red>();
        List<Computadora> listaComputadoras = new List<Computadora>();

        private readonly TcpClient cliente = new TcpClient();
        private NetworkStream mainStream;
        private int puerto;

        private static Image GrabarEscritorio()
        {
            Rectangle bounds = Screen.PrimaryScreen.Bounds;
            Bitmap screeshot = new Bitmap(bounds.Width, bounds.Height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(screeshot);
            graphics.CopyFromScreen(bounds.X, bounds.Y, 0, 0, bounds.Size, CopyPixelOperation.SourceCopy);
            return screeshot;
        }

        private void SendDesktopImage()
        {
            BinaryFormatter binFormatter = new BinaryFormatter();
            mainStream = cliente.GetStream();
            binFormatter.Serialize(mainStream, GrabarEscritorio());

        }

        public FrmInfoNetwork()
        {
            InitializeComponent();
        }

        private void MostrarInformacionDeLaRed()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                MetroMessageBox.Show(this, "No hay conexión con la red.", "Error al conectar con la Red", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            else
            {
                foreach (NetworkInterface netInterface in NetworkInterface.GetAllNetworkInterfaces())
                {
                    // Solo procesar interfaz de red tipo Ethernet 
                    //if (netInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    //{
                    Red aux = new Red();
                    aux.Nombre = netInterface.Name;
                    aux.Descripcion = netInterface.Description;
                    aux.MAC = netInterface.GetPhysicalAddress().ToString();
                    aux.Velocidad = (netInterface.Speed / 1000000).ToString() + "Mbps";
                    listaRedes.Add(aux);
                    //  MostrarDirecionesIP(netInterface);
                    //}
                }
                dgvListaRedes.DataSource = listaRedes.ToList();
            }
        }

        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(int DestIP, int SrcIP, [Out] byte[] pMacAddr, ref int PhyAddrLen);

        private void ObtenerIp(string nombre)
        {
            try
            {
                Computadora computadora;

                // usar grupo de trabajo WinNT://&&&&(Grupo de trabajo)
                DirectoryEntry DomainEntry = new DirectoryEntry("WinNT://" + nombre);
                DomainEntry.Children.SchemaFilter.Add("computer");

                // To Get all the System names And Display with the Ip Address
                foreach (DirectoryEntry machine in DomainEntry.Children)
                {
                    computadora = new Computadora();
                    computadora.Nombre = machine.Name;

                    System.Net.IPHostEntry host = null;

                    try
                    {
                        host = (IPHostEntry)Dns.GetHostByName(machine.Name);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("No se puede conectar :" + machine.Name);
                        continue;
                    }

                    IPAddress[] TempAd = host.AddressList;

                    foreach (IPAddress ip in TempAd)
                    {
                        computadora.IPv4 = ip.ToString();

                        byte[] ab = new byte[6];
                        int len = ab.Length;

                        // Obtener Direccion MAC
                        int r = SendARP((int)ip.Address, 0, ab, ref len);
                        string mac = BitConverter.ToString(ab, 0, 6);

                        computadora.MAC = mac;

                    }

                    listaComputadoras.Add(computadora);

                    dgvDetalles.DataSource = listaComputadoras.ToList();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private string MiIpv4()
        {
            string strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            foreach (IPAddress ipAddress in ipEntry.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    return $"Mi IPv4: {ipAddress.ToString()}";
                }
            }

            return "-";
        }

        private void FrmInfoNetwork_Load(object sender, EventArgs e)
        {
            lblIP.Text = MiIpv4();
            MostrarInformacionDeLaRed();

            //puerto = 2015;
            //try
            //{
            //    cliente.Connect("192.168.1.45", puerto);
            //    timer1.Start();
            //    //if (btnCompartir.Text.StartsWith("Share"))
            //    //{
            //    //    timer1.Start();
            //    //    btnCompartir.Text = "Stop";
            //    //}
            //    //else
            //    //{
            //    //    timer1.Stop();
            //    //    btnCompartir.Text = "Share";
            //    //}
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("No se pudo conectar");
            //    return;
            //}
            // ObtenerIp("WORKGROUP");
        }

        private void dgvListaRedes_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void btnConexion_Click(object sender, EventArgs e)
        {
            //puerto = int.Parse(tbxPuerto.Text);
            //try
            //{
            //    cliente.Connect(tbxIp.Text, puerto);
            //    MessageBox.Show("Conectado");
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("No se pudo conectar");
            //}
            //ObtenerIp("WORKGROUP");
        }

        private void btnCompartir_Click(object sender, EventArgs e)
        {
            //if (btnCompartir.Text.StartsWith("Share"))
            //{
            //    timer1.Start();
            //    btnCompartir.Text = "Stop";
            //}
            //else
            //{
            //    timer1.Stop();
            //    btnCompartir.Text = "Share";
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                SendDesktopImage();
            }
            catch (Exception)
            {
                MessageBox.Show("Conexion perdida");
            }

        }
    }
}