﻿namespace Presentacion.Formularios.Network
{
    partial class FrmServidor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbxDesktop = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDesktop)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxDesktop
            // 
            this.pbxDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxDesktop.Location = new System.Drawing.Point(20, 60);
            this.pbxDesktop.Name = "pbxDesktop";
            this.pbxDesktop.Size = new System.Drawing.Size(1040, 454);
            this.pbxDesktop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxDesktop.TabIndex = 0;
            this.pbxDesktop.TabStop = false;
            // 
            // FrmServidor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1080, 534);
            this.Controls.Add(this.pbxDesktop);
            this.Name = "FrmServidor";
            this.Text = "FrmServidor";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.FrmServidor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxDesktop)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxDesktop;
    }
}