﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Presentacion.Formularios.Network
{
    public partial class FrmServidor : MetroForm
    {
        private readonly int puerto;
        private TcpClient cliente;
        private TcpListener server;
        private NetworkStream mainStream;

        private readonly Thread Listening;
        private readonly Thread GetImage;

        public FrmServidor(int Puerto)
        {
            puerto = Puerto;
            cliente = new TcpClient();
            Listening = new Thread(StartListening);
            GetImage = new Thread(ReceiveImage);
            InitializeComponent();
        }

        private void StartListening()
        {
            while (!cliente.Connected)
            {
                server.Start();
                cliente = server.AcceptTcpClient();
            }
            GetImage.Start();
        }

        private void StopListening()
        {
            server.Stop();
            cliente = null;
            if (Listening.IsAlive)
            {
                Listening.Abort();
            }

            if (GetImage.IsAlive)
            {
                GetImage.Abort();
            }
        }

        private void ReceiveImage()
        {
            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                while (cliente.Connected)
                {
                    mainStream = cliente.GetStream();
                    pbxDesktop.Image = (Image)binaryFormatter.Deserialize(mainStream);

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Conexion Perdida");
            }

        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            server = new TcpListener(IPAddress.Any, puerto);
            Listening.Start();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            StopListening();
        }

        private void btnListen_Click(object sender, EventArgs e)
        {

        }

        private void FrmServidor_Load(object sender, EventArgs e)
        {

        }
    }
}