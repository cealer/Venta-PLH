﻿namespace Presentacion.Formularios.PreferenciaUsuarios
{
    partial class TileInterfaz
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TileTitulo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbxFondo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tbxLetra = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // TileTitulo
            // 
            // 
            // 
            // 
            this.TileTitulo.CustomButton.Image = null;
            this.TileTitulo.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.TileTitulo.CustomButton.Name = "";
            this.TileTitulo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.TileTitulo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TileTitulo.CustomButton.TabIndex = 1;
            this.TileTitulo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TileTitulo.CustomButton.UseSelectable = true;
            this.TileTitulo.CustomButton.Visible = false;
            this.TileTitulo.Enabled = false;
            this.TileTitulo.Lines = new string[0];
            this.TileTitulo.Location = new System.Drawing.Point(113, 10);
            this.TileTitulo.MaxLength = 11;
            this.TileTitulo.Name = "TileTitulo";
            this.TileTitulo.PasswordChar = '\0';
            this.TileTitulo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TileTitulo.SelectedText = "";
            this.TileTitulo.SelectionLength = 0;
            this.TileTitulo.SelectionStart = 0;
            this.TileTitulo.Size = new System.Drawing.Size(136, 23);
            this.TileTitulo.Style = MetroFramework.MetroColorStyle.Green;
            this.TileTitulo.TabIndex = 15;
            this.TileTitulo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TileTitulo.UseSelectable = true;
            this.TileTitulo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TileTitulo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(0, 10);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(48, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 14;
            this.metroLabel2.Text = "Titulo: ";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // tbxFondo
            // 
            // 
            // 
            // 
            this.tbxFondo.CustomButton.Image = null;
            this.tbxFondo.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.tbxFondo.CustomButton.Name = "";
            this.tbxFondo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxFondo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxFondo.CustomButton.TabIndex = 1;
            this.tbxFondo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxFondo.CustomButton.UseSelectable = true;
            this.tbxFondo.CustomButton.Visible = false;
            this.tbxFondo.Enabled = false;
            this.tbxFondo.Lines = new string[0];
            this.tbxFondo.Location = new System.Drawing.Point(113, 44);
            this.tbxFondo.MaxLength = 11;
            this.tbxFondo.Name = "tbxFondo";
            this.tbxFondo.PasswordChar = '\0';
            this.tbxFondo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxFondo.SelectedText = "";
            this.tbxFondo.SelectionLength = 0;
            this.tbxFondo.SelectionStart = 0;
            this.tbxFondo.Size = new System.Drawing.Size(136, 23);
            this.tbxFondo.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxFondo.TabIndex = 17;
            this.tbxFondo.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxFondo.UseSelectable = true;
            this.tbxFondo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxFondo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(0, 44);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(107, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 16;
            this.metroLabel1.Text = "Color de fondo: ";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseStyleColors = true;
            // 
            // tbxLetra
            // 
            // 
            // 
            // 
            this.tbxLetra.CustomButton.Image = null;
            this.tbxLetra.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.tbxLetra.CustomButton.Name = "";
            this.tbxLetra.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbxLetra.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxLetra.CustomButton.TabIndex = 1;
            this.tbxLetra.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxLetra.CustomButton.UseSelectable = true;
            this.tbxLetra.CustomButton.Visible = false;
            this.tbxLetra.Enabled = false;
            this.tbxLetra.Lines = new string[0];
            this.tbxLetra.Location = new System.Drawing.Point(113, 77);
            this.tbxLetra.MaxLength = 11;
            this.tbxLetra.Name = "tbxLetra";
            this.tbxLetra.PasswordChar = '\0';
            this.tbxLetra.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxLetra.SelectedText = "";
            this.tbxLetra.SelectionLength = 0;
            this.tbxLetra.SelectionStart = 0;
            this.tbxLetra.Size = new System.Drawing.Size(136, 23);
            this.tbxLetra.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxLetra.TabIndex = 19;
            this.tbxLetra.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxLetra.UseSelectable = true;
            this.tbxLetra.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxLetra.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(0, 77);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(98, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 18;
            this.metroLabel3.Text = "Color de letra: ";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel3.UseStyleColors = true;
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            this.metroTextBox1.CustomButton.Image = null;
            this.metroTextBox1.CustomButton.Location = new System.Drawing.Point(114, 1);
            this.metroTextBox1.CustomButton.Name = "";
            this.metroTextBox1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox1.CustomButton.TabIndex = 1;
            this.metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox1.CustomButton.UseSelectable = true;
            this.metroTextBox1.CustomButton.Visible = false;
            this.metroTextBox1.Enabled = false;
            this.metroTextBox1.Lines = new string[0];
            this.metroTextBox1.Location = new System.Drawing.Point(113, 106);
            this.metroTextBox1.MaxLength = 11;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.SelectionLength = 0;
            this.metroTextBox1.SelectionStart = 0;
            this.metroTextBox1.Size = new System.Drawing.Size(136, 23);
            this.metroTextBox1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTextBox1.TabIndex = 21;
            this.metroTextBox1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(0, 106);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(103, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 20;
            this.metroLabel4.Text = "Acceso Directo: ";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.UseStyleColors = true;
            // 
            // TileInterfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroTextBox1);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.tbxLetra);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.tbxFondo);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.TileTitulo);
            this.Controls.Add(this.metroLabel2);
            this.Name = "TileInterfaz";
            this.Size = new System.Drawing.Size(263, 137);
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        public MetroFramework.Controls.MetroTextBox TileTitulo;
        public MetroFramework.Controls.MetroTextBox tbxFondo;
        public MetroFramework.Controls.MetroTextBox tbxLetra;
        public MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroLabel metroLabel4;
    }
}
