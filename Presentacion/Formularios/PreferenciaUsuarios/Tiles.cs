﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Formularios.PreferenciaUsuarios
{
    public class Tiles
    {
        public string Texto { get; set; }
        public string Imagen { get; set; }
        public string ColorLetra { get; set; }
        public string ColorFondo { get; set; }
        public string Formulario { get; set; }
        public string Estado { get; set; }

        public Tiles()
        {

        }

        public Tiles(string texto, string imagen, string colorLetra, string colorFondo, string estado)
        {
            Texto = texto; Imagen = imagen; ColorLetra = colorLetra; ColorFondo = colorFondo; Estado = estado;
        }

    }
}
