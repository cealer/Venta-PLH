﻿namespace Presentacion.Formularios.Reportes
{
    partial class FrmReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            var dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.tbxConsulta = new MetroFramework.Controls.MetroTextBox();
            this.btnProcesar = new MetroFramework.Controls.MetroButton();
            this.btnExportarExcel = new MetroFramework.Controls.MetroButton();
            this.btnExportarPdf = new MetroFramework.Controls.MetroButton();
            this.btnImprimir = new MetroFramework.Controls.MetroButton();
            this.dgvLista = new MetroFramework.Controls.MetroGrid();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(23, 78);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(59, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel5.TabIndex = 37;
            this.metroLabel5.Text = "Consulta";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel5.UseStyleColors = true;
            // 
            // tbxConsulta
            // 
            // 
            // 
            // 
            this.tbxConsulta.CustomButton.Image = null;
            this.tbxConsulta.CustomButton.Location = new System.Drawing.Point(706, 1);
            this.tbxConsulta.CustomButton.Name = "";
            this.tbxConsulta.CustomButton.Size = new System.Drawing.Size(143, 143);
            this.tbxConsulta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbxConsulta.CustomButton.TabIndex = 1;
            this.tbxConsulta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbxConsulta.CustomButton.UseSelectable = true;
            this.tbxConsulta.CustomButton.Visible = false;
            this.tbxConsulta.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbxConsulta.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbxConsulta.Lines = new string[0];
            this.tbxConsulta.Location = new System.Drawing.Point(23, 113);
            this.tbxConsulta.MaxLength = 50000;
            this.tbxConsulta.Multiline = true;
            this.tbxConsulta.Name = "tbxConsulta";
            this.tbxConsulta.PasswordChar = '\0';
            this.tbxConsulta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxConsulta.SelectedText = "";
            this.tbxConsulta.SelectionLength = 0;
            this.tbxConsulta.SelectionStart = 0;
            this.tbxConsulta.Size = new System.Drawing.Size(850, 145);
            this.tbxConsulta.Style = MetroFramework.MetroColorStyle.Green;
            this.tbxConsulta.TabIndex = 36;
            this.tbxConsulta.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbxConsulta.UseSelectable = true;
            this.tbxConsulta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbxConsulta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnProcesar
            // 
            this.btnProcesar.Location = new System.Drawing.Point(948, 78);
            this.btnProcesar.Name = "btnProcesar";
            this.btnProcesar.Size = new System.Drawing.Size(91, 47);
            this.btnProcesar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnProcesar.TabIndex = 39;
            this.btnProcesar.Text = "Procesar";
            this.btnProcesar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnProcesar.UseSelectable = true;
            this.btnProcesar.UseStyleColors = true;
            this.btnProcesar.Click += new System.EventHandler(this.btnProcesar_Click);
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Location = new System.Drawing.Point(948, 154);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(91, 48);
            this.btnExportarExcel.Style = MetroFramework.MetroColorStyle.Green;
            this.btnExportarExcel.TabIndex = 40;
            this.btnExportarExcel.Text = "Exportar a Excel";
            this.btnExportarExcel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnExportarExcel.UseSelectable = true;
            this.btnExportarExcel.UseStyleColors = true;
            // 
            // btnExportarPdf
            // 
            this.btnExportarPdf.Location = new System.Drawing.Point(948, 230);
            this.btnExportarPdf.Name = "btnExportarPdf";
            this.btnExportarPdf.Size = new System.Drawing.Size(91, 48);
            this.btnExportarPdf.Style = MetroFramework.MetroColorStyle.Green;
            this.btnExportarPdf.TabIndex = 41;
            this.btnExportarPdf.Text = "Exportar a PDF";
            this.btnExportarPdf.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnExportarPdf.UseSelectable = true;
            this.btnExportarPdf.UseStyleColors = true;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(948, 303);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(91, 48);
            this.btnImprimir.Style = MetroFramework.MetroColorStyle.Green;
            this.btnImprimir.TabIndex = 42;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnImprimir.UseSelectable = true;
            this.btnImprimir.UseStyleColors = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToResizeRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLista.EnableHeadersVisualStyles = false;
            this.dgvLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLista.Location = new System.Drawing.Point(23, 280);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(208)))), ((int)(((byte)(104)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLista.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLista.Size = new System.Drawing.Size(850, 281);
            this.dgvLista.Style = MetroFramework.MetroColorStyle.Green;
            this.dgvLista.TabIndex = 43;
            this.dgvLista.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLista.UseCustomForeColor = true;
            // 
            // Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 584);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnExportarPdf);
            this.Controls.Add(this.btnExportarExcel);
            this.Controls.Add(this.btnProcesar);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.tbxConsulta);
            this.Name = "Reportes";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Reportes";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox tbxConsulta;
        private MetroFramework.Controls.MetroButton btnProcesar;
        private MetroFramework.Controls.MetroButton btnExportarExcel;
        private MetroFramework.Controls.MetroButton btnExportarPdf;
        private MetroFramework.Controls.MetroButton btnImprimir;
        private MetroFramework.Controls.MetroGrid dgvLista;
    }
}