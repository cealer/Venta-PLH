﻿using BOL;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Reportes
{
    public partial class FrmReportes : MetroForm
    {
        public FrmReportes()
        {
            InitializeComponent();
        }

        public static DataTable lista;

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            try
            {
                lista = ConsultasBOL.Buscar(tbxConsulta.Text);
                dgvLista.DataSource = lista;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error al procesar la consulta. Asegúrese que la consulta esta bien realizada.");
                MessageBox.Show(ex.Message);
            }
            catch (Exception er)
            {
                MessageBox.Show("Error al generar la consulta");
            }

        }



        private void btnImprimir_Click(object sender, EventArgs e)
        {
            var aux = new ImprimirReporte();
            lista.TableName = "Lista";
            //aux.lista = lista;
            aux.ShowDialog();
        }
    }
}