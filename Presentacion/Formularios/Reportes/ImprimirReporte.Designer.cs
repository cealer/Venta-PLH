﻿namespace Presentacion.Formularios.Reportes
{
    partial class ImprimirReporte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rvLista = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rvLista
            // 
            this.rvLista.Location = new System.Drawing.Point(23, 63);
            this.rvLista.Name = "rvLista";
            this.rvLista.Size = new System.Drawing.Size(933, 392);
            this.rvLista.TabIndex = 0;
            // 
            // ImprimirReporte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 468);
            this.Controls.Add(this.rvLista);
            this.Name = "ImprimirReporte";
            this.Text = "ImprimirReporte";
            this.Load += new System.EventHandler(this.ImprimirReporte_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rvLista;
    }
}