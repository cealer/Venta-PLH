﻿using MetroFramework.Forms;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Formularios.Reportes
{
    public partial class ImprimirReporte : MetroForm
    {
        public ImprimirReporte()
        {
            InitializeComponent();
        }

        public static DataTable lista { get; set; }

        private void ImprimirReporte_Load(object sender, EventArgs e)
        {
            var source = new ReportDataSource("Table", FrmReportes.lista);
            rvLista.LocalReport.ReportPath = "Reporte_Generico.rdlc";
            rvLista.LocalReport.DataSources.Clear();
            rvLista.LocalReport.DataSources.Add(source);
            rvLista.RefreshReport();

            //
            //ReportDataSource rds = new ReportDataSource();
            //rds.Name = "Table";
            //rds.Value = FrmReportes.lista;
            //rvLista.RefreshReport();
        }
    }
}
