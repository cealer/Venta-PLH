﻿namespace Utilidades.Apariencia
{
    public class Control_PictureBox
    {
        public void FormaCircular(System.Windows.Forms.PictureBox pbx)
        {
            var gp = new System.Drawing.Drawing2D.GraphicsPath();
            gp.AddEllipse(0, 0, pbx.Width - 3, pbx.Height - 3);
            var rg = new System.Drawing.Region(gp);
            pbx.Region = rg;
        }
    }
}