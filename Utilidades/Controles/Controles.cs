﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utilidades.Controles
{
    public class Controles : IDisposable
    {
        //Obtener todos los componentes por tipo de datos espeficico
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();
            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        ~Controles()
        {
            Dispose();
        }

        //Obtener todos los componentes dentro de un control

        public IEnumerable<Control> GetAll(Control control)
        {
            var controls = control.Controls.Cast<Control>();
            return controls.SelectMany(ctrl => GetAll(ctrl))
                                      .Concat(controls);
        }

        //public void CambiarColor(Control aux, Color color)
        //{
        //    aux.BackColor = color;
        //}

        public void Estado(MetroForm aux, bool estado)
        {
            var controles = GetAll(aux);
            foreach (var item in controles)
            {
                if (item.GetType().ToString() == "System.Windows.Forms.TextBox" ||
                    item.GetType().ToString().Contains("TextBox"))
                {
                    item.Enabled = estado;
                }
            }
        }

        public void Limpiar(MetroForm aux)
        {
            var controles = GetAll(aux);
            foreach (var item in controles)
            {
                if (item.GetType().ToString() == "System.Windows.Forms.TextBox" || item.GetType().ToString().Contains("TextBox"))
                {
                    item.ResetText();
                }

                else if (item.GetType().ToString() == "System.Windows.Forms.ComboBox")
                {
                    ((ComboBox)item).SelectedIndex = -1;
                }
                else if (item.GetType().ToString() == "System.Windows.Forms.DateTimePicker")
                {
                    ((DateTimePicker)item).Value = DateTime.Today;
                }
                else if (item.GetType().ToString() == "System.Windows.Forms.Label")
                {
                    if (item.Name.Contains("lbl"))
                    {
                        ((Label)item).Text = "Datos";
                    }
                }
            }
        }


        public void Dispose() => GC.SuppressFinalize(this);
    }
}
