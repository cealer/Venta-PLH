﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tesseract;

namespace Utilidades
{
    public class RucConsulta
    {
        public string RUC { get; set; }
        public string Direccion { get; set; }
        public string NombreCompleto { get; set; }
        public string NombreCorto { get; set; }

        CookieContainer cookie;


        public RucConsulta()
        {
            cookie = null;
            cookie = new CookieContainer();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

        }

        public Image DescargarImagen()
        {
            Image image = null;

            try
            {
                //ServicePointManager.ServerCertificateValidationCallback += RemoteCertificateValidationCallback()
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(@"http://www.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=image");
                webRequest.CookieContainer = cookie;
                webRequest.Proxy = null;
                webRequest.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                stream = webResponse.GetResponseStream();
                image = Image.FromStream(stream);
                //webResponse.Close();
            }
            catch (Exception ex)
            {
                return null;
            }

            return image;
        }

        public string Codigo()
        {
            string codigo;
            var img = DescargarImagen();

            Bitmap bitmapIMG = new Bitmap(img);

            const string language = "eng";

            var dataPath = @"C:\Program Files (x86)\Tesseract-OCR\tessdata";

            try
            {
                using (var tEngine = new TesseractEngine(dataPath, language, EngineMode.Default)) //creating the tesseract OCR engine with English as the language
                {
                    using (bitmapIMG) // Load of the image file from the Pix object which is a wrapper for Leptonica PIX structure
                    {
                        using (var page = tEngine.Process(bitmapIMG)) //process the specified image
                        {
                            codigo = page.GetText(); //Gets the image's content as plain text.
                            Console.WriteLine(codigo); //display the text
                            return Regex.Replace(codigo, " ", "").Trim();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: " + e.Message);
                return "";
            }

        }

        public void Info(string Ruc)
        {
            //Encoding objEncoding = Encoding.UTF8;
            string codigo = Codigo();
            string Url = $@"http://www.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?accion=consPorRuc&nroRuc={Ruc}&codigo={codigo}";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.CookieContainer = cookie;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Proxy = null;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string datos = reader.ReadToEnd();

            try
            {
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(datos);

                var table = document.DocumentNode
           .Descendants("tr")
           .Select(n => n.Elements("td").Select(e => e.InnerText).ToArray());

                foreach (var tr in table)
                {
                    if (tr[0].Contains("N&uacute;mero de RUC:"))
                    {
                        string str = Regex.Replace(tr[1], "N&uacute;mero de RUC:", "");
                        var rpt = str.Split('-');
                        //Console.WriteLine($"RUC: { rpt[0].Trim() }");
                        RUC = rpt[0].Trim();
                        NombreCompleto = rpt[1].Trim();
                        //Console.WriteLine($"Nombre Completo: { rpt[1].Trim() }");
                    }

                    if (tr[0].Contains("Nombre Comercial:"))
                    {
                        NombreCorto = tr[1].Trim();
                        //Console.WriteLine($"Nombre Corto: { tr[1].Trim() }");
                    }

                    if (tr[0].Contains("Domicilio Fiscal:"))
                    {
                        Direccion = Regex.Replace(tr[1], "  ", "");
                        //Console.WriteLine($"Direccion: {Regex.Replace(tr[1], "  ", "") }");
                    }
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("Vuelva a consultar");
            }
        }
    }
}